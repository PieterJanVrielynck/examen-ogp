package temperature;
import be.kuleuven.cs.som.annotate.*; 
/**
 * A value class of temperatures 
 * 
 * @invar Each temperature can have its cold temperature as its cold temperature
 *        | canHaveAsTemperatureCold(getTemperatureCold() ) 
 * @invar Each temperature can have its heat temperature as its heat temperature 
 *        | canHaveAsTemperatureHeat(getTemperatureHeat() ) 
 * @invar The temperature class has a valid maximum temperature 
 *        |  isValidMaxTemperature( getMaxTemperature() ) 
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 *
 */
@Value 
public class Temperature {

    /**
     * A constructor to initialize a new temperature  
     * @param cold
     *         the cold temperature 
     * @param heat
     *        the heat temperature 
     * @post  if cold is a possible cold temperature 
     *        then  if heat is a possible temperature heat and matches cold 
     *             then temperature heat is set to heat and temperature cold is set to cold 
     *             else the temperature cold is set to cold and the temperature heat is set to zero
     *        else 
     *             if heat is a possible temperature heat  
     *             then temperature cold is set to zero and temperature heat is set to heat 
     *             else both temperature cold and heat are set to zero
     *        | if ( isPossibleTemperatureCold(cold) ) 
     *        | then 
     *        |     if isPossibleTemperatureHeat(heat) && matchesTemperatureColdHeat(cold, heat)
     *        |     then new.getTemperatureHeat() == heat && new.getTemperatureCold() == cold 
     *        |     else new.getTemperatureHeat() == 0 && new.getTemperatureCold() == cold 
     *        | else 
     *        |       if ( isPossibleTemperatureHeat()  )
     *        |       then new.getTemperatureHeat() == heat && new.getTemperatureCold() == 0
     *        |       else new.getTemperatureHeat() ==  new.getTemperatureCold() == 0
     *             
     */
 	public Temperature( long cold, long heat) {
		if ( isPossibleTemperatureCold(cold)) {
			this.temperatureCold = cold ;
		}
		else {
			this.temperatureCold = 0 ; 
		}
		if (canHaveAsTemperatureHeat(heat)) {
			this.temperatureHeat = heat ; 
		}
		else {
			this.temperatureHeat = 0 ; 
		}
		
	}
	/**
	 * A constructor to initialize a new temperature 
	 * 
	 * @effect this temperature is constructed with 0 as cold temperature and 20 as heat temperature 
	 *         | this(0,20)
	 */
	public Temperature() {
		this(0,20) ; 
	}
	
	/**
	 * A basic inspector to return the cold temperature of this temperature 
	 */
	@Basic @Immutable
	public long getTemperatureCold() {
		return this.temperatureCold ; 
	}
	
	/**
	 * An inspector to check whether a given temperature cold is a possible temperature cold 
	 * @param cold
	 *        the temperature cold 
	 * @return true if the cold is greater than or equal to zero and is smaller than or equal to the maximum temperature 
	 *         false otherwise 
	 *         | result == ( cold >= 0 && cold <= getMaxTemperature() ) 
	 */
	public static boolean isPossibleTemperatureCold(long cold) {
		return cold >= 0 && cold <= getMaxTemperature() ; 
	}
	
	/**
	 * An inspector to check whether this temperature can have the cold temperature as its cold temperature 
	 * 
	 * @param cold
	 *        the given temperature cold 
	 * @return true if cold is a possible temperature cold and cold matches with the temperature heat 
	 *         false otherwise 
	 *         | result == ( isPossibleTemperatureCold(cold) && matchesTemperatureColdHeat(cold, getTemperatureHeat()) ) 
	 */
	public boolean canHaveAsTemperatureCold(long cold) {
		return isPossibleTemperatureCold(cold) && matchesTemperatureColdHeat(cold, getTemperatureHeat()) ; 
	}
	
	/**
	 * A variable referencing the temperature cold of a temperature 
	 */
	private final long temperatureCold;
	
	/**
	 * An basic inspector to return the temperature heat of a temperature 
	 */
	@Basic @Immutable
	public long getTemperatureHeat() {
		return this.temperatureHeat ; 
	}
	
	/**
	 * An inspector to check whether a given temperature heat is a possible temperature heat 
	 * @param heat
	 *        the temperature heat 
	 * @return true if  heat is greater than or equal to zero and is smaller than or equal to the maximum temperature 
	 *         false otherwise 
	 *         | result == ( heat >= 0 && heat <= getMaxTemperature() ) 
	 */
	public static boolean isPossibleTemperatureHeat(long heat) {
		return heat >= 0 && heat <= getMaxTemperature() ; 
	}
	
	
	/**
	 * An inspector to check whether this temperature can have the heat temperature as its heat temperature 
	 * 
	 * @param heat
	 *        the given temperature cold 
	 * @return true if heat is a possible temperature heat and heat matches with the temperature cold 
	 *         false otherwise 
	 *         | result == ( isPossibleTemperatureHeat(heat) && matchesTemperatureColdHeat(getTemperatureCold(), heat) ) 
	 */
	public boolean canHaveAsTemperatureHeat(long heat) {
		return isPossibleTemperatureHeat(heat) && matchesTemperatureColdHeat(getTemperatureCold(), heat) ; 
	}
	
	/**
	 * A variable referencing the temperature heat of a temperature 
	 */
	private final long temperatureHeat;
	
    /**
     * An inspector to check whether a given heat and cold can be a proper temperature
     * @param cold
     *        the given cold
     * @param heat
     *        the given heat 
     * @return true if the given cold or the given heat is zero 
     *         false otherwise
     *         | result ==  (cold == 0 || heat == 0)
     */
	public static boolean matchesTemperatureColdHeat(long cold, long heat) {
		return (cold == 0 || heat == 0) ; 
	}
	
	/**
	 * An inspector to query whether this temperature is hotter than another temperature
	 * @param other
	 *        the other temperature 
	 * @return true if this temperature is effective and this temperature converted to a long is bigger than the other temperature converted to a long
	 *         false otherwise 
	 *         | result == ( convertTemperatureToLong(this) > convertTemperatureToLong(other) ) 
	 */
	public boolean isHotterThan(Temperature other) {
		if (other == null) return false ; 
		return convertTemperatureToLong(this) > convertTemperatureToLong(other) ; 
		
	}
	
	
	/**
	 * An inspector to query whether this temperature is colder than another temperature
	 * @param other
	 *        the other temperature 
	 * @return true if this temperature is effective and this temperature converted to a long is smaller than the other temperature converted to a long
	 *         false otherwise 
	 *         | result == (convertTemperatureToLong(this) < convertTemperatureToLong(other) ) 
	 */
	public boolean isColderThan(Temperature other) {
		if (other == null) return false ; 
		return convertTemperatureToLong(this) < convertTemperatureToLong(other) ; 
	}
	
	/**
	 * An inspector to query a array representation of the temperature 
	 * @return An array with as first element the temperature cold and as second element the temperature heat 
	 *         | result == new long[] {getTemperatureCold(), getTemperatureHeat()}
	 */
	@Immutable
	public long[] getArrayTemperature() {
		return new long[] {getTemperatureCold(), getTemperatureHeat()} ; 
	}
	
	/**
	 * An inspector to query the difference between two temperatures 
	 * @param other
	 *        the other temperature 
	 * @return the differce between this temperature converted to a long and the other temperature converted to a long
	 *         | result == (convertTemperatureToLong(this) - convertTemperatureToLong(other) )
	 *         
	 * @throws IllegalArgumentException
	 *          the other temperature is not effective 
	 *          | other == null 
	 */
	public long getDifference(Temperature other) throws IllegalArgumentException {
		if (other == null) throw new IllegalArgumentException() ; 
		return convertTemperatureToLong(this) - convertTemperatureToLong(other) ; 
	}
	
	/**
	 * Method to return a temperature, converted as a single long value.
	 * @param	temperature
	 * 			The original temperature to be converted.
	 * @return	the heat temperature minus the cold temperature of the given temperature
	 *          | result == temperature.getTemperatureHeat() - temperature.getTemperatureCold();
	 * @throws IllegalArgumentException
	 *         if the given temperature is not effective 
	 *         | temperature == null
	 */
	public static long convertTemperatureToLong(Temperature temperature) throws IllegalArgumentException{
		if ( temperature == null) throw new IllegalArgumentException() ; 
		return temperature.getTemperatureHeat() - temperature.getTemperatureCold();
		
	}
	
	/**
	 * A method to convert a long to a temperature 
	 * @param temperature
	 *        the long value to convert to a temperature 
	 * @return if the temperature is smaller than zero 
	 *         then 
	 *               if - temperature is smaller or equal to the maximum temperature 
	 *               then result equals new Temperature(-1*temperature, 0)
	 *               else  result equals new Temperature(getMaxTemperature(), 0)
	 *          else 
	 *                if temperature is smaller than or equal to the maximum temperature 
	 *                then result equals new Temperature(0, temperature)
	 *                else result equals new Temperature(0, getMaxTemperature() )
	 *          | if  (temperature < 0)
	 *          | then  
	 *          |       if  (-1*temperature <= getMaxTemperature())
	 *          |       then result == new Temperature(-1*temperature, 0)
	 *          |       else result == new Temperature(getMaxTemperature(), 0)
	 *          | else 
	 *          |       if  (temperature <= getMaxTemperature()) 
	 *          |       then result == new Temperature(0, temperature)
	 *          |       else result == new Temperature(0, getMaxTemperature() )
	 *                
	 */
	public static Temperature convertLongToTemperature(long temperature) {
		if  (temperature < 0) {
			if (-1*temperature <= getMaxTemperature())return new Temperature(-1*temperature, 0);
			else return new Temperature(getMaxTemperature(), 0) ; 
		}
		else {
			if (temperature <= getMaxTemperature()) return new Temperature(0, temperature);
			else return new Temperature(0, getMaxTemperature() ) ; 
		}
	}
	
	/**
	 * An inspector to query a heated version of this temperature
	 * @param temperatureDifference
	 *        the difference between this temperature and the result temperature 
	 * @return if the temperatureDifference is  negative 
	 *         then this temperature will be returned 
	 *         else 
	 *              if either this temperature converted to a long is positive and this temperature converted to a long plus the given difference is negative 
	 *                 or this temperature converted to a long plus the given difference is greater than the maximum temperature 
	 *              then result is a new Temperature(0, getMaxTemperature() )
	 *              else the result is  this temperature converted to a long plus the given difference converted to a temperature 
	 *         | if ( temperatureDifference <= 0 ) 
	 *         | then result == this
	 *         | else 
	 *         |		if ( (convertTemperatureToLong(this) >= 0 && convertTemperatureToLong(this)+ temperatureDifference < 0) 
	 *         |              ||  convertTemperatureToLong(this)+ temperatureDifference > getMaxTemperature() )
	 *         | 		then result == new Temperature(0, getMaxTemperature() 
	 *         | 		else result == convertLongToTemperature(convertTemperatureToLong(this)+ temperatureDifference)
	 * 
	 */
	public Temperature heat(long temperatureDifference) {
		if (temperatureDifference > 0) {
			long temperature = convertTemperatureToLong(this) ;
			long newTemperature = temperature+ temperatureDifference ; 
			if ( (temperature >= 0 && newTemperature < 0) || newTemperature > getMaxTemperature() ) return new Temperature(0, getMaxTemperature() ) ; 
			return convertLongToTemperature(newTemperature) ; 
		}
		return this ; 
	}
	
	/**
	 * An inspector to query a cooled version of this temperature
	 * @param temperatureDifference
	 *        the difference between the result temperature and this temperature 
	 * @return if the temperatureDifference is  negative 
	 *         then this temperature will be returned 
	 *         else
	 *         	   if either  this temperature converted to a long is negative and the this temperature converted to a long minus the difference is strictly positive
	 *                or this temperature converted to a long minus the difference is smaller than - the maximum temperature 
	 *             then result is new Temperature( getMaxTemperature(),0 )
	 *             else result is  this temperature converted to a long minus the difference converted to a temperature 
	 *         | if ( temperatureDifference <= 0 ) 
	 *         | then result == this
	 *         | else 
	 *         |		if ( (convertTemperatureToLong(this) <= 0 && convertTemperatureToLong(this) -  temperatureDifference> 0) 
	 *         |           || -(convertTemperatureToLong(this) -  temperatureDifference) > getMaxTemperature() )
	 *         |		then result == new Temperature( getMaxTemperature(),0 )
	 *         |        else result == convertLongToTemperature(convertTemperatureToLong(this) -  temperatureDifference)
	 */
	public Temperature cool(long temperatureDifference) {
		if (temperatureDifference > 0) {
			long temperature = convertTemperatureToLong(this) ;
			long newTemperature = temperature -  temperatureDifference ; 
			if ( (temperature <= 0 && newTemperature > 0) || -newTemperature > getMaxTemperature() ) return new Temperature( getMaxTemperature(),0 ) ; 
			return convertLongToTemperature(newTemperature) ; 
		}
		return this ;
	}

    /**
     * Variable referencing the maximum value of the coldness and the heat of a temperature 
     */
    private final static long maxTemperature = 10000; 
    
    /**
     * A basic inspector to return the maximum value of the temperature 
     */
    @Basic
    public static long getMaxTemperature() {
    	return maxTemperature ; 
    }
    /**
     * An inspector to check whether a given maximum is a valid maximum temperature 
     * @param max
     *        the maximum value of the temperature
     * @return true if the maximum is not negative
     *         false otherwise 
     *         | result == (maximum >= 0  )
     */
    public static boolean isValidMaxTemperature(long maximum) {
    	return (maximum<0)? false:true ; 
    }
	
    /**
     * An inspector to check whether an given object equals this temperature
     * 
     * @param obj
     *        the object to check 
     * @return true if the object is a Temperature, effective and both the cold and the heat temperature are equal to the cold and heat temperature of this 
     *                  temperature 
     *         false otherwise 
     *         | result == obj != null && obj instanceof Temperature) 
     *         |      && getTemperatureCold() == ((Temperature) obj).getTemperatureCold() && getTemperatureHeat() == ((Temperature) obj).getTemperatureHeat()  )
     */
    @Override
    public boolean equals(Object obj) {
    	if (obj == null || !( obj instanceof Temperature)) return false ; 
    	else return (getTemperatureCold() == ((Temperature) obj).getTemperatureCold() && getTemperatureHeat() == ((Temperature) obj).getTemperatureHeat()  ); 
    }
    
    /**
     * An inspector to query the hashcode of this temperature 
     * @return the hashcode of the heat temperature 
     *         | (new Long(this.temperatureHeat) ).hashCode() 
     */
    @Override 
    public int hashCode() {
    	return (new Long(this.temperatureHeat) ).hashCode() ; 
    }

}
