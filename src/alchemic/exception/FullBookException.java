package alchemic.exception;
/**
 * An exception class for full books
 * 
 * @author @author Yoshi Vermeire, Pieter-Jan Vrielynck
 *
 */
public class FullBookException extends RuntimeException {

	/**
	 * A default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A constructor to create a new full book exception
	 */
	public FullBookException() {
		super() ; 
	}

}
