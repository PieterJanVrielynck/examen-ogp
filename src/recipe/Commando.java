package recipe;
import be.kuleuven.cs.som.annotate.*; 
/**
 * A value class of commands of a recipe. 
 * 
 * @author yoshi
 *
 */
@Value
public enum Commando {
	ADD(true), HEAT(false), COOL(false), MIX(false) ;
	
	/**
	 * Constructor
	 * @param hasIngredient
	 *        indicating whether a ingredient belongs to this instruction
	 * @post the has ingredient will be set to the given value 
	 *       |  hasIngredient() == hasIngredient   
	 */
	private Commando(boolean hasIngredient) {
		this.hasIngredient = hasIngredient ; 
	}
	
	/**
	 * A basic inspector to check whether a given instruction should have an ingredient
	 */
	@Basic
	public boolean hasIngredient() {
		return this.hasIngredient ; 
	}
	/**
	 * Variable referencing whether an ingredient belongs to this instruction
	 */
	private boolean hasIngredient;


}
