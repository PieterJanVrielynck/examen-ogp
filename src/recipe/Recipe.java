package recipe;
import be.kuleuven.cs.som.annotate.*; 
import java.util.*;

import alchemie.AlchemicIngredient; 

/**
 * A class of recipes 
 * 
 * @invar Each recipe should have a proper book
 *        | hasProperBook()
 * @author yoshi
 *
 */
public class Recipe {
	
	/**
	 * A constructor to initialize a new recipe 
	 * @param book
	 *        the recipe book
	 * @effect the recipe book of this book is set to the given book
	 *         | setRecipeBook(book)
	 */
	public Recipe(RecipeBook book) {
		setRecipeBook(book);
	}
	
    /**
     *A basic inspector to return the recipe book this recipe belongs to.  
     */
	@Basic
	public RecipeBook getRecipeBook() {
          return this.book ; 
	}
	
	/**
	 * An inspector to verify if a given book can be the recipe book for this recipe
	 * @param book
	 *        the book to check 
	 * @return true if book is effective and either contains this recipe or is not full 
	 *         false otherwise 
	 *         | result == true if book != null && ( book.containsRecipe(this) || ! book.isFull() ) 
	 *         | else result == false 
	 */
	@Raw
	public boolean canHaveAsBook(RecipeBook book) {
		if (book == null) return false ; 
		return book.containsRecipe(this) || ! book.isFull() ; 
	}
	
	/**
	 * A inspector to verify if this recipe has a proper recipe book
	 * @return true if this recipe can have its recipe book as its recipe book and its recipe book contains this recipe 
	 *         false otherwise 
	 *         | if  canHaveAsBook(getRecipeBook()) && getRecipeBook().containsRecipe(this) then result == true 
	 *         | else result == false 
	 */
	@Raw
	public boolean hasProperBook() {
		return canHaveAsBook(getRecipeBook()) && getRecipeBook().containsRecipe(this) ; 
	}
	/**
	 * Mutator to set the recipe book to a given book
	 * @param book
	 *        the target book
	 * @throws IllegalArgumentException
	 *         if the book cannot have the given recipe book as its book or the current recipe book equals the given book
	 *         | ! canHaveAsBook(book) || book == getRecipeBook()
	 * @post  the recipe book will be set to the given book
	 *        | new.getRecipeBook() == book
	 * @effect if the old recipe book is  effective this recipe will be removed from the old recipe book
	 *         | if ( this.getRecipeBook() != null) this.getRecipeBook().removeRecipe(this)
	 * @effect this recipe will be added to the new recipe book
	 *         | new.getRecipeBook().addRecipe(this) 
	 */
	@Raw
	public void setRecipeBook(RecipeBook book) throws  IllegalArgumentException{
		if (! canHaveAsBook(book) ) throw new IllegalArgumentException() ; 
		if (book == getRecipeBook()) throw new IllegalArgumentException(" same book is given") ;
		if (this.book != null ) {
			this.book.removeRecipe(this);
		}
		book.addRecipe(this);
		this.book = book ; 
	}
	
	
	
	/**
	 * A variable referencing the book that contains this recipe
	 */
	private RecipeBook book ; 
	
	/**
	 * A basic inspector to return the instructions of a recipe 
	 */
	@Basic 
	public List<Commando> getInstructions(){
		return new ArrayList<Commando>(this.instructions)  ; 
	}
	
	/**
	 * An inspector to return the number of instructions in this recipe
	 * @return the number of instructions 
	 *         | result == getInstructions().size() 
	 */
	public int getNumberOfInstructions() {
		return this.instructions.size() ; 
	}
	
	/**
	 * An inspector to check whether a instruction can be placed at the given index
	 * @param index
	 *        the index 
	 * @param instruction
	 *        the instrcution
	 * @return if the index is not a proper or the instruction is not effectve then result equals false 
	 *         else if the instruction equals ADD 
	 *                  then result equals true 
	 *         else if the instruction equals COOL or HEAT 
	 *                then 
	 *                     if the number of adds before this index is at least 1 then result equals true 
	 *                     else result equals false 
	 *         else 
	 *               if the number of ADDs before this index is at least 2 then result equals true 
	 *               else result equals false 
	 *         | if (! canHaveAsIndex(index)) || (instruction == null )
	 *         | then result == false 
	 *         | else if (instruction == Commando.ADD) 
	 *         | then result == true 
	 *         | else if (instruction == Commando.COOL || instruction == Commando.HEAT )
	 *         | then result ==  getNumberOfAddBeforeIndex(index) >= 1
	 *         | else result == getNumberOfAddBeforeIndex(index) >= 2
	 */
	public boolean canHaveAsPossibleInstructionAt(int index, Commando instruction) {
		if (! canHaveAsIndex(index)) return false ; 
		if (instruction == null ) return false ; 
		if (instruction == Commando.ADD) return true ; 
		int numberOfAdd = getNumberOfAddBeforeIndex(index) ; 
		if (instruction == Commando.COOL || instruction == Commando.HEAT ) return numberOfAdd >= 1 ; 
		return numberOfAdd >= 2 ; 
	}
	

	/**
	 * An inspector to check whether a given index is a proper index 
	 * @param index
	 *        the index to verify 
	 * @return true if the index lies between 1 and 1 plus the number of instructions 
	 *         false otherwise 
	 *         | result == ! (index < 1 || index > getNumberOfInstructions() + 1  )
	 */
	public boolean canHaveAsIndex(int index) {
		if (index < 1 || index > getNumberOfInstructions() + 1  ) return false ;
		return true ; 
	}
	
	/**
	 * A inspector to return the instruction at the given index
	 * @param index
	 *        the index to query
	 * @pre the index lies between 1 and the number of instructions 
	 *      | 1 <= index <= getNumberOfInstructions() 
	 */
	public Commando getInstructionAt(int index) {
		return this.instructions.get(index-1) ; 
	}
	
	/**
	 * A mutator to add an instruction without a ingredient 
	 * @param instruction
	 *        the instruction
	 * @param index
	 *        the index to place the instruction at 
	 * @post  the instruction is added at the given index 
	 *        | new.getInstructionAt(index) == instruction 
	 * @throws IllegalArgumentException
	 *         the given instruction is not a possible instruction at the given index or the instrcution equals ADD
	 *         | ! canHaveAsPossibleInstructionAt(index, instruction) || instruction == Commando.ADD
	 */
	public void addCommandoWithoutIngredientAt(Commando instruction, int index) throws IllegalArgumentException{
		if (! canHaveAsPossibleInstructionAt(index, instruction)) throw new IllegalArgumentException(); 
		if (instruction == Commando.ADD) throw new IllegalArgumentException() ; 
		this.instructions.add(index -1 , instruction);
	}
	
	/**
	 * An inspector to check whether this recipe has proper instructions 
	 * 
	 * @return true if every instrcution is a possible instruction at its and the number of ingredients are a proper number of ingredients 
	 *         false otherwise 
	 *         |if ( for index = 1... getNumberOfInstructions:  canHaveAsPossibleInstructionAt(index, getInstructionAt(index) )) 
	 *         |         && canHaveAsNumberOfIngredients(getNumberOfIngredients())
	 *         |then result == true 
	 *         |else result == false   
	 */
	public boolean hasProperInstrcutions() {
		int index = 1 ; 
		for( Commando instruction: this.instructions) {
			if (!canHaveAsPossibleInstructionAt(index, instruction)) return false  ; 
			index ++ ; 
		}
		return canHaveAsNumberOfIngredients(getNumberOfIngredients()) ; 
	}
	
	/**
	 * Inspector to query the number of ADD-instructions before a given index
	 * @param index
	 *        the index 
	 * @pre the index should be a proper index 
	 *      | canHaveAsIndex(index)        
	 */
	private int getNumberOfAddBeforeIndex(int index) {
		int numberOfAdd = 0 ; 
		for (int i = 0; i < index -1 ; i++) {
			if (this.instructions.get(i) == Commando.ADD) numberOfAdd ++ ; 
		}
		return numberOfAdd ; 
	}
	/**
	 * Variable referencing the instructions stored in the recipe 
	 * 
	 * @invar instructions should always be a effective list 
	 *        | instructions != null 
	 * @invar every instruction should be effective 
	 *        | for instruction in instructions: 
	 *        |       instruction != null 
	 */
	private List<Commando> instructions = new ArrayList<Commando>() ; 
	
	/**
	 * A basic inspector to return the ingredients used in the instructions of this recipe  
	 */
	@Basic 
	public List<AlchemicIngredient> getIngredients() {
		return new ArrayList<AlchemicIngredient>(this.ingredients) ; 
	}
	
	/**
	 * An inspector to check whether a given alchemic ingredient can be part of the ingredients 
	 * @param ingredient
	 *        the alchemic ingredient
	 * @return true if the ingredient is effective
	 *         false otherwise 
	 *         | result == (ingredient != null)
	 */
	public static boolean isPossibleIngredient(AlchemicIngredient ingredient) {
		return ingredient != null ; 
	}
	/**
	 * Inspector to check if a given number can be a proper number of ingredients
	 * @param number
	 *        the possible number of ingredients 
	 * @return true if the number equals the total number of ADD-instructions 
	 *         false otherwise 
	 *         | result == (getNumberOfAddBeforeIndex(getNumberOfInstructions()+1) == number) 
	 */
	public boolean canHaveAsNumberOfIngredients(int number) {
		int numberOfAdd = getNumberOfAddBeforeIndex(getNumberOfInstructions()+1) ; 
		return numberOfAdd == number ; 
	}
	/**
	 * An inspector to return the ingredient at given index
	 * @param index
	 *        the index of the desired ingredient
	 * @pre index is greater than or equal to one and smaller than or equal to the total number of ingredients
	 * @return	The ingredient at the given index.
	 * 			| getIngredients().get(index-1) 
	 */
	public AlchemicIngredient getIngredientAt(int index) {
		return getIngredients().get(index-1) ; 
	}
	
	/**
	 * An inspector to return the total number of ingredients in this recipe.
	 * @return	The number of ingredients in this recipe.
	 * 			| return getIngredients().size()
	 */
	public int getNumberOfIngredients() {
		return getIngredients().size() ; 
	}
	
	/**
	 * An inspector to check whether this recipe has only proper ingredients 
	 * @return true if this recipe can have the number of ingredients as its number of ingredients and 
	 *                  every ingredient in this recipe is a possible ingredient
	 *         false otherwise 
	 *         | if  canHaveAsNumberOfIngredients(getNumberOfIngredients()) 
	 *         |       && (for index = 1...getNumberOfIngredients(): isPossibleIngredient(getIngredientAt(index)) ) 
	 *         | then result == true 
	 *         | else result == false  
	 *                    
	 */
	public boolean hasProperIngredients() {
		if( ! canHaveAsNumberOfIngredients(getNumberOfIngredients())) return false ; 
		for (AlchemicIngredient ingredient: this.ingredients) {
			if (!isPossibleIngredient(ingredient)) return false ; 
		}
		return true ; 
	}
	
	/**
	 * Variable referencing the ingredients used in the instructions 
	 * 
	 * @invar ingredients should always be an effective list 
	 *        | ingredients != null 
	 * @invar every ingredient in ingredients should be effective 
	 *        | for ingredient in ingredients: 
	 *        |    ingredient != null 
	 */
	private List<AlchemicIngredient> ingredients = new ArrayList<AlchemicIngredient>() ; 

	/**
	 * A mutator to add an ADD-instruction at the given index
	 * @param ingredient
	 *        the ingredient this recipe should add 
	 * @param index
	 *        the  index 
	 * @post The Add-instruction is added at the given index and every instruction with an former index greater than or equal to the given index 
	 *       moves one index to the right and the number of instructions increases by one
	 *       | new.getInstructionAt(index) == Commandp.ADD  & 
	 *       | for every i = index...old.getNumberOfInstructions(): new.getInstructionAt(i+1) == old.getInstructionAt(i) &
	 *       | new.getNumberOfInstructions() == old.getNumberOfInstrcutions() + 1 
	 * @post The given ingredient is inserted just after all the ingredients belonging to an add with a new index smaller than the given index and just before all 
	 *       the ingredients belonging to an add with an new index greater than the given index. 
	 *       The indexes of the ingredients belonging to an add with an new index greater than the given index will be increased by one.
	 *       The total number of ingredients will have been increased by one 
	 *       | new.getIngredientAt(getNumberOfAddBeforeIndex(index)+1) == ingredient &
	 *       | for (i = getNumberOfAddBeforeIndex(index)+1...getNumberOfIngredients(): new.getIngredientAt(i+1) == old.getIngredientAt(i) 
	 *       | new.getNumberOfIngredients() == getNumberOfIngredients() +1  
	 *       
	 * @throws IllegalArgumentException 
	 *         this recipe cannot have the ADD-commando at the given index or the given ingredient is not a possible ingredient 
	 *         | ! canHaveAsPossibleInstructionAt(index, Commando.ADD) || ! isPossibleIngredient(ingredient)
	 */
	public void addCommandoAt(AlchemicIngredient ingredient, int index) throws IllegalArgumentException {
		if ( ! canHaveAsPossibleInstructionAt(index, Commando.ADD) || ! isPossibleIngredient(ingredient)) throw new IllegalArgumentException() ; 
		this.instructions.add(index -1 , Commando.ADD);
		this.ingredients.add(getNumberOfAddBeforeIndex(index) , ingredient);
	}
}
