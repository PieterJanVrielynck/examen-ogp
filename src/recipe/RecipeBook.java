package recipe;
import be.kuleuven.cs.som.annotate.*; 
import alchemic.exception.*; 
/**
 * A class of a recipe book
 * 
 * @invar Each book should have a valid number of pages as its number of pages 
 *       | isValidNumberOfPages(getNumberOfPages() ) 
 * @invar Each book should have proper pages 
 *        | hasProperPages() 
 * 
 * 
 * @author yoshi
 *
 */
public class RecipeBook {
	public RecipeBook(int numberOfPages) throws IllegalArgumentException{
		if (! isValidNumberOfPages(numberOfPages)) throw new IllegalArgumentException() ; 
		this.numberOfPages = numberOfPages ; 
		this.pages = new Recipe[numberOfPages] ; 
	}
	/**
	 * A basic inspector to return the number of pages this recipe book contains. 
	 *      This number equals the maximum number of recipes this book can contain. 
	 */
	@Basic
	public int getNumberOfPages() {
		return this.numberOfPages ; 
	}
	/**
	 * An inspector to verify if a given number of pages is a valid number of pages for a recipe book
	 * @param numberOfPages
	 *        the number to check 
	 * @return true if the given number is greater than zero 
	 *         false otherwise 
	 *         | result == (numberOfPages>0)
	 */
	public static boolean isValidNumberOfPages(int numberOfPages){
		return (numberOfPages>0) ; 
	}
	/**
	 * A variable referencing the number of pages of this recipe book. 
	 */
	private final int numberOfPages ;
	
    /**
     * An inspector to return the recipe at the given page number 
     * @param pageNumber
     *        the page number
     * @return  a null reference if the page at the given number is free
     *          else a affective recipe
     *          |  if ( isFreePage(pageNumber) then result == null 
     *          |  else getPageNumberxOf(result ) == pageNumber
     * @throws IndexOutOfBoundsException
     *         if the book does not contain a page with given page number 
     *         |!hasAsPage( pageNumber)
     */
	public Recipe getRecipeAt(int pageNumber) throws IndexOutOfBoundsException{
		if (! hasAsPage(pageNumber)) throw new IndexOutOfBoundsException() ; 
		return this.pages[pageNumber -1 ] ;  
	}
	/**
	 * An inspector to check whether a given page number references a free page
	 * @param pageNumber
	 *        the page number 
	 * @return true if given page exists and the recipe at the given page is not effective
	 *         false otherwise 
	 *         | if hasAsPage(pageNumber) && getRecipeAt(pageNumber) == null result == true
	 *         | else result == false  
	 * 
	 */
	public boolean isFreePage(int pageNumber) {
		if (! hasAsPage(pageNumber)) return false ; 
		else return this.pages[pageNumber-1] == null ; 
	}
	/**
	 * An inspector to check whether a page with given page number exists in this recipe book
	 * @param pageNumber
	 *        the page number
	 * @return true if the given page number is smaller than or equal to the number of pages
	 *         false otherwise 
	 *         | result == pageNumber <= getNumberOfPages()
	 */
	public boolean hasAsPage(int pageNumber) {
		return pageNumber <= getNumberOfPages() && pageNumber >=  1; 
	}
	/**
	 * An inspector to return the page number of the given recipe
	 * @param recipe
	 *        the recipe
	 * @return if recipe is not effective 
	 *         then if the book is not full the first empty page will be returned. 
	 *              else -1 will be returned 
	 *         else if there is a page number referencing the recipe this page number will be returned 
	 *              else -1 will be returned 
	 *         | if ( recipe == null) 
	 *         |       if (isFull() ) then result == -1 
	 *         |       else  isFreePage(result) 
	 *         |              && (for number = 1...(result -1) : ! isFreePage(number) ) 
	 *         | else  
	 *         |     if (for any number in 1...getNumberOfPages():  getRecipeAt(number) == recipe) then result == number ) 
	 *         |     else result == -1 
	 */
	public int getPageNumberOf(Recipe recipe) {
		int pageNumber = 1 ; 
		for (Recipe rep : this.pages) {
			if (rep == (recipe))  {
				return pageNumber ; 
			}
			pageNumber ++ ; 
		}
		return -1 ; 
	}
	
	/**
	 * A inspector to check whether this recipe book does no longer contain any free page
	 * @return true if for all recipes in this book are effective 
	 *         false otherwise 
	 *         | if ( for any number in 1... getNumberOfPages() getRecipeAt(number) == null )
	 *         | then result == false 
	 *         | else result == true  
	 */
	public boolean isFull() {
		for (Recipe recipe : this.pages) {
			if (recipe == null) {
				return false ; 
			}
		}
		return true ; 
	}
	
	/**
	 * An inspector to check whether a this book contains a given recipe
	 * @param recipe
	 *        the given recipe 
	 * @return result == (getPageNumberOf(recipe) != -1)
	 */
	public boolean containsRecipe(Recipe recipe) {
		return (getPageNumberOf(recipe) != -1) ; 
	}
	
	/**
	 * An inspector to check whether this book only contains proper pages 
	 * @return true if for every page in this book: 
	 *              the page references a non-effective recipe 
	 *              or the page references a recipe that references back to this book and 
	 *                 there is only one page that references that recipe   
	 *         false otherwise
	 *         |if ( for ( number = 1...getNumberOfPages()) :  
	 *         |        isFreePage(number) 
	 *         |        || (getRecipeAt(number).getRecipeBook() == this 
	 *         |            && for index = getPageNumberOf(page) +1...getNumberOfPages() : getRecipeAt(number) != getRecipeAt(index)   ) )
	 *         |then result == true 
	 *         |else result == false
	 */
	@Raw 
	public boolean hasProperPages() {
		for (Recipe page: this.pages) {
			if (page != null && ! (page.getRecipeBook() == this) ) {
				return false ; 
			}
			if (page != null) {
				for (int number= getPageNumberOf(page) +1; number <= getNumberOfPages() ; number ++ ) {
					if ( getRecipeAt(number) == (page)) {
						return false ; 
					}
				}
			}

		}
		return true ; 
	}
    /**
     * A mutator to add a recipe to this recipe book
     * @param recipe
     *        the recipe to add
     * @post the recipe will be added at the first free page 
     *       | new.getRecipeAt( this.getPageNumberOf(null) ) == recipe
     * @throws FullBookException
     *         if the book is full 
     *         | isFull() 
     * @throws IllegalArgumentException
     *         if the book already contains this recipe 
     *         | containsRecipe(recipe)
     */
	@Model
	void addRecipe(@Raw Recipe recipe) throws FullBookException, IllegalArgumentException{
		if ( isFull()) throw new FullBookException() ; 
		if (containsRecipe(recipe) ) throw new IllegalArgumentException() ; 
		pages[getPageNumberOf(null) -1 ] = recipe ;  
	}
	/**
	 * A mutator to remove a given recipe from a book
	 * @param recipe
	 *        the given recipe 
	 * @post the book will no longer contain this recipe and the page that used to references the given recipe will be free 
	 *       | new.isFreePage(this.getPageNumberOf(recipe) )  
	 * @throws IllegalArgumentException
	 *         if the book does not contain this recipe 
	 *         | ! containsRecipe(recipe) 
	 */
	@Model 
	void removeRecipe(Recipe recipe ) throws IllegalArgumentException {
		if (! containsRecipe(recipe)) throw new IllegalArgumentException() ; 
		pages[getPageNumberOf(recipe)-1] = null ; 
	}
	
	/**
	 * Variable referencing the pages of this recipe book. 
	 * @invar pages should always references a effective array. 
	 *        | pages != null 
	 * @invar each page should be either non-effective or reference a recipe the references back to this book
	 *        | for each recipe in pages:
	 *        |       recipe == null or recipe.getRecipeBook() == this
	 * @invar Each recipe in pages is either not effective or has only one page 
	 *        | for number in 1...getNumberOfPages() :
	 *        |   getRecipeAt(number) == null 
	 *        |    || for index in  1...getNumberOfPages(): if (index != number) then getRecipeAt(number) != getRecipeAt(index)
	 *        
	 */
	private final Recipe[] pages ; 

}
