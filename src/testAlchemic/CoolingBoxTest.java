package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alchemie.*;
import temperature.Temperature;

/**
 * Test class for the classes 'Device' (abstract), 'DeviceSingleContainer' (abstract), 
 * 'DeviceTemperatureController' (abstract) and the class 'CoolingBox' from the 'alchemie' package.
 * @author	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version	1.0
 */
class CoolingBoxTest {
	
	Laboratory laboratory;
	
	
	//notLinked: not part of a Laboratory: can't be executed and can't have any IngredientContainer's.
	CoolingBox validCoolingBox;
	CoolingBox notLinkedCoolingBox;
	
	//No invalid containers, because these wouldn't be accepted by the Laboratory.
	//All tests relative to the Laboratory can be found within the Laboratory test class from the package
	//testAlchemic.
	IngredientContainer validContainerHOT;
	IngredientContainer validContainerCOLD;
	IngredientContainer validContainerEXTRA;
	
	//No invalid ingredients, because these wouldn't be accepted by the IngredientContainer.
	//All tests relative to the IngredientContainer can be found within the IngredientContainer test class from the package
	//testAlchemic.
	AlchemicIngredient validIngredientHOT;
	AlchemicIngredient validIngredientCOLD;
	AlchemicIngredient validIngredientEXTRA;
	
		
	
	@BeforeEach
	void setUp() throws Exception {
		laboratory = new Laboratory(500);
		validCoolingBox = new CoolingBox(0, 0);
		notLinkedCoolingBox = new CoolingBox(0,0);
		validIngredientHOT = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypeone", State.LIQUID, 100, 0),5,Quantity.SPOON);
		validIngredientCOLD = new AlchemicIngredient(State.POWDER, new IngredientType("Ingredienttypetwo", State.POWDER, 0, 0), 5, Quantity.SPOON);
		validIngredientEXTRA = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypeextra", State.LIQUID, 100, 0), 5, Quantity.SPOON);
		
		//REMINDER: the containers validContainerHOT and validContainerCOLD will be terminated at the end of the SetUp() method.
		validContainerHOT = new IngredientContainer(Quantity.BARREL, validIngredientHOT);
		validContainerCOLD = new IngredientContainer(Quantity.CHEST, validIngredientCOLD);
		validContainerEXTRA = new IngredientContainer(Quantity.BARREL, validIngredientEXTRA);
		
		laboratory.addDevice(validCoolingBox);
		laboratory.addIngredient(validContainerCOLD);
		laboratory.addIngredient(validContainerHOT);
	}

	@Test
	void testExecute() {
		try {
			notLinkedCoolingBox.execute();
			fail("Execution of a non-linked CoolingBox succeeded.");
		} catch (IllegalStateException e) {};
		
		IngredientContainer containerHOT = laboratory.retrieveIngredient("Ingredienttypeone");
		IngredientContainer containerCOLD = laboratory.retrieveIngredient("Ingredienttypetwo");
		assertEquals(containerHOT.getIngredient().getTemperature(), validIngredientHOT.getTemperature());
		assertEquals(containerCOLD.getIngredient().getTemperature(), validIngredientCOLD.getTemperature());
		
		try {
			validCoolingBox.execute();
			fail("Succesfully executed a CoolingBox without any ingredients.");
		} catch (IllegalStateException e) {};
		
		validCoolingBox.addIngredientContainer(containerHOT);
		assertTrue(containerHOT.isTerminated());
		validCoolingBox.execute();
		assertTrue(validCoolingBox.isExecuted());
		assertEquals(validCoolingBox.getResult().getIngredient().getTemperature().getTemperatureCold(), 
				validCoolingBox.getTemperature().getTemperatureCold());
		assertEquals(validCoolingBox.getResult().getIngredient().getTemperature().getTemperatureHeat(), 
				validCoolingBox.getTemperature().getTemperatureHeat());
		validCoolingBox.clean();
		assertTrue(validCoolingBox.isCleaned());
		assertFalse(validCoolingBox.isExecuted());
		
		//	!!!
		validCoolingBox.setTemperature(0, 50);
		
		validCoolingBox.addIngredientContainer(containerCOLD);
		validCoolingBox.execute();
		assertTrue(validCoolingBox.isExecuted());
		assertEquals(validCoolingBox.getResult().getIngredient().getTemperature().getTemperatureCold(), 
				validIngredientCOLD.getTemperature().getTemperatureCold());
		assertEquals(validCoolingBox.getResult().getIngredient().getTemperature().getTemperatureHeat(), 
				validIngredientCOLD.getTemperature().getTemperatureHeat());
		validCoolingBox.clean();
		assertTrue(validCoolingBox.isCleaned());
		assertFalse(validCoolingBox.isExecuted());
		
		laboratory.addIngredient(validContainerEXTRA);
		try {
			validCoolingBox.addIngredientContainer(validContainerEXTRA);
			fail("Succesfully added a terminated container to a CoolingBox.");
		} catch (IllegalArgumentException e) {};
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient(validIngredientEXTRA.getSimpleName()));
		assertTrue(validContainerEXTRA.isTerminated());
		validCoolingBox.execute();
		assertTrue(validCoolingBox.isExecuted());
		assertEquals(validCoolingBox.getResult().getIngredient().getTemperature().getTemperatureCold(), 
				validCoolingBox.getTemperature().getTemperatureCold());
		assertEquals(validCoolingBox.getResult().getIngredient().getTemperature().getTemperatureHeat(),
				validCoolingBox.getTemperature().getTemperatureHeat());
		validCoolingBox.clean();
		assertTrue(validCoolingBox.isCleaned());
		assertFalse(validCoolingBox.isExecuted());
	}
	
	

	@Test
	void testCoolingBox_LegalCase() {
		CoolingBox[] devices = {validCoolingBox, notLinkedCoolingBox};
		for (CoolingBox device:devices) {
			assertEquals(device.getTemperature().getTemperatureCold(),0);
			assertEquals(device.getTemperature().getTemperatureHeat(),0);
		}
	}
	
	@Test
	void testCoolingBox_IllegalCase() {
		long[] temperatures = {Long.MAX_VALUE, Long.MIN_VALUE, 0, 500, -500};
		for (long temp1:temperatures) {
			for (long temp2:temperatures) {
				if (!((temp1 == 0 && temp2 >= 0) || (temp1 >= 0 && temp2 == 0))) {
					try {
						@SuppressWarnings("unused")
						CoolingBox test = new CoolingBox(temp1, temp2);
						fail("Succesfull initialization of a CoolingBox without proper given "
								+ "temperatures " + temp1 + " and " + temp2 +".");
					} catch (IllegalArgumentException e) {}
				}
			}
		}
	}

	@Test
	void testIsValidTemperature() {
		assertTrue(CoolingBox.isValidTemperature(validCoolingBox.getTemperature().getTemperatureCold(), validCoolingBox.getTemperature().getTemperatureHeat()));
		assertTrue(CoolingBox.isValidTemperature(notLinkedCoolingBox.getTemperature().getTemperatureCold(), validCoolingBox.getTemperature().getTemperatureHeat()));
		assertTrue(CoolingBox.isValidTemperature(Integer.MAX_VALUE,0));
		assertTrue(CoolingBox.isValidTemperature(0, Integer.MAX_VALUE));
		assertFalse(CoolingBox.isValidTemperature(500, 200));
		assertFalse(CoolingBox.isValidTemperature(-500, 200));
		assertFalse(CoolingBox.isValidTemperature(500, -200));
		assertFalse(CoolingBox.isValidTemperature(-500, -2000));
		assertFalse(CoolingBox.isValidTemperature(-500, 0));
		assertFalse(CoolingBox.isValidTemperature(0, -50));
	}

	@Test
	void testGetTemperature() {
		assertEquals(notLinkedCoolingBox.getTemperature(), new Temperature(0,0));
		validCoolingBox.setTemperature(500, 0);
		assertEquals(validCoolingBox.getTemperature(), new Temperature(500,0));
	}

	@Test
	void testSetTemperature() {
		int[] temperaturePart = {0,Integer.MAX_VALUE, Integer.MIN_VALUE, 500, -200};
		for (int part1:temperaturePart) {
			for (int part2:temperaturePart) {
				if ((part1 == 0 && part2 >= 0) || (part1 >= 0 && part2 == 0)) {
					validCoolingBox.setTemperature(part1, part2);
				}
				else {
					try {
						validCoolingBox.setTemperature(part1, part2);
						fail("Succesfully set the temperature of validCoolingBox to a temperature that's not allowed: "
								+ "part1: " + part1 + " and part 2: " + part2);
					} catch (IllegalArgumentException e) {}
				}
			}
		}
	}

	@Test
	void testAddIngredientContainer() {
		try {
			notLinkedCoolingBox.addIngredientContainer(validContainerEXTRA);
			fail("Added an Ingredientcontainer to a non-linked CoolingBox succesfully.");
		} catch (IllegalStateException e) {}
	
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient("Ingredienttypeone"));
		validCoolingBox.execute();
		assertEquals(validCoolingBox.getResult().getIngredient().getAmount(), validIngredientHOT.getAmount());
		assertEquals(validCoolingBox.getResult().getIngredient().getUnit(), validIngredientHOT.getUnit());
		assertEquals(validCoolingBox.getResult().getIngredient().getCharacteristicalVolatility(), validIngredientHOT.getCharacteristicalVolatility());
		assertEquals(validCoolingBox.getResult().getIngredient().getSimpleName(), validIngredientHOT.getSimpleName());
		assertEquals(validCoolingBox.getResult().getIngredient().getState(), validIngredientHOT.getState());
		assertEquals(validCoolingBox.getResult().getIngredient().getType(), validIngredientHOT.getType());
		assertEquals(validCoolingBox.getResult().getIngredient().getVolatility(), validIngredientHOT.getVolatility());
	}
	

	@Test
	void testClean() {
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient("Ingredienttypeone"));
		validCoolingBox.clean();
		assertTrue(validCoolingBox.isCleaned());
		try {
			validCoolingBox.execute();
			fail("The execution of an empty CoolingBox succeeded.");
		} catch (IllegalStateException e) {};
		
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient("Ingredienttypetwo"));
		validCoolingBox.execute();
		validCoolingBox.clean();
		assertTrue(validCoolingBox.isCleaned());
		assertNull(validCoolingBox.getResult());
	}

	@Test
	void testHasProperIngredientContainer() {
		assertTrue(validCoolingBox.hasProperIngredientContainer());
		try {
			validCoolingBox.addIngredientContainer(validContainerEXTRA);
			fail("Succesfully created a loophole, were ingredients can be added to a device that aren't "
					+ "part of the same Laboratory.");
		} catch (IllegalArgumentException e) {};
		
		assertTrue(validCoolingBox.hasProperIngredientContainer());
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient("Ingredienttypeone"));
		assertTrue(validCoolingBox.hasProperIngredientContainer());
	}



	@Test
	void testCanHaveAsIngredientContainer() {
		assertTrue(validCoolingBox.canHaveAsIngredientContainer(laboratory.retrieveIngredient("Ingredienttypeone")));
		
		assertFalse(validCoolingBox.canHaveAsIngredientContainer(validContainerEXTRA));
		assertFalse(validCoolingBox.canHaveAsIngredientContainer(null));
		
		validContainerEXTRA.terminate();
		assertFalse(validCoolingBox.canHaveAsIngredientContainer(validContainerEXTRA));
		
		assertFalse(validCoolingBox.canHaveAsIngredientContainer(new IngredientContainer(Quantity.SPOON, State.LIQUID)));
	}

	@Test
	void testIsExecuted() {
		assertFalse(validCoolingBox.isExecuted());
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient("Ingredienttypeone"));
		assertFalse(validCoolingBox.isExecuted());
		validCoolingBox.execute();
		assertTrue(validCoolingBox.isExecuted());
		validCoolingBox.clean();
		assertFalse(validCoolingBox.isExecuted());
	}


	@Test
	void testGetResult() {
		//Other aspects of the result of the CoolingBox validCoolingBox are tested in other tests of this test class.
		validCoolingBox.addIngredientContainer(laboratory.retrieveIngredient("Ingredienttypeone"));
		validCoolingBox.execute();
		assertNotNull(validCoolingBox.getResult());
		assertEquals(IngredientContainer.class, validCoolingBox.getResult().getClass());
		assertFalse(validCoolingBox.getResult().isEmpty());
		validCoolingBox.clean();
		assertNull(validCoolingBox.getResult());
	}

	
	@Test
	void testGetLaboratory() {
		assertNull(notLinkedCoolingBox.getLaboratory());
		assertEquals(validCoolingBox.getLaboratory(), laboratory);
	}


	@Test
	void testRemoveLaboratory() {
		assertEquals(validCoolingBox.getLaboratory(), laboratory);
		validCoolingBox.removeLaboratory();
		assertNull(validCoolingBox.getLaboratory());
		validCoolingBox.removeLaboratory();
		assertNull(validCoolingBox.getLaboratory());
	}

}
