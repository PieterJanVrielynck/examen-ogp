package testAlchemic;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alchemie.IngredientType;
import alchemie.State;
import temperature.Temperature;

class IngredientTypeTest {

	@Test
	void testIngredientTypeStringStateLongLong1() {
		IngredientType in = new IngredientType("Name", State.LIQUID, 0, 0) ; 
		assertTrue(in.getName() == "Name") ; 
		assertTrue(in.canHaveAsName(in.getName())) ; 
		assertEquals(in.getState(), State.LIQUID );
		assertTrue(in.getTemperature().equals(new Temperature(0, 0))) ; 
		assertTrue(in.getTheoreticalVolatility() == 0 ) ; 
		in = new IngredientType("Name", State.LIQUID, 0, 50) ; 
		assertTrue(in.getTheoreticalVolatility() == 0 ) ; 
		in = new IngredientType("Name", State.LIQUID, 0, 1) ; 
		assertTrue(in.getTheoreticalVolatility() == 1 ) ; 
	    try {
	    	in = new IngredientType("N", State.LIQUID, 0, 0) ; 
	    }
	    catch(IllegalArgumentException exception) {
		   assertTrue(true) ; 
	     }
	    try {
	    	in = new IngredientType("Name", null, 0, 0) ; 
	    }
	    catch(IllegalArgumentException exception) {
		   assertTrue(true) ; 
	     }
	    
	    
 
	}

	@Test
	void testIngredientType() {
		IngredientType in = new IngredientType() ; 
		assertTrue(in.getName() == "Water") ; 
		assertTrue(in.canHaveAsName(in.getName())) ; 
		assertEquals(in.getState(), State.LIQUID );
		assertTrue(in.getTemperature().equals(new Temperature(0, 20))) ; 
		assertTrue(in.getTheoreticalVolatility() == 0 ) ;
	}

	@Test
	void testIValidTheoriticalVolatility() {
		assertTrue(IngredientType.isValidTheoriticalVolatility(0) ); 
		assertTrue(IngredientType.isValidTheoriticalVolatility(1) ); 
		assertTrue(IngredientType.isValidTheoriticalVolatility(0.5) ); 
		assertTrue(!IngredientType.isValidTheoriticalVolatility(50) ); 
		assertTrue(!IngredientType.isValidTheoriticalVolatility(-0.5) ); 
		
	}
	
	@Test 
	void testCanHaveAsName() {
		IngredientType type = new IngredientType() ; 
		assertTrue(type.canHaveAsName(" Water ")) ; // one word 
		assertTrue(type.canHaveAsName("Wat")) ; 
		assertTrue(type.canHaveAsName("Water")) ;
		assertFalse(type.canHaveAsName("Wa")) ;
		assertFalse(type.canHaveAsName("wat")) ;
		assertFalse(type.canHaveAsName("WAter")) ;
		assertFalse(type.canHaveAsName("Heated")) ; 
		assertFalse(type.canHaveAsName("H@d")) ;
		assertFalse(type.canHaveAsName(null)) ;
		assertTrue(type.canHaveAsName("Nitrogin Gas")) ; // one+ word
		assertTrue(type.canHaveAsName("Ni Ga")) ; 
		assertFalse(type.canHaveAsName("Ni gas")) ;
		assertFalse(type.canHaveAsName("Ni Gas Heated")) ; 
		assertFalse(type.canHaveAsName("Ni Gas Go Hi Hy Hi Ni op")) ;
		assertFalse(type.canHaveAsName("Ni Gas Go Hi Hy Hi Ni OP")) ;
		assertTrue(type.canHaveAsName("Ni Gas Go Hi Hy Hi Ni Op")) ;
	}
	
	@Test 
	void testCanHaveAsTemperature() {
		IngredientType type = new IngredientType() ; 
		assertTrue(!type.canHaveAsTemperature(null));
		assertTrue(!type.canHaveAsTemperature(new Temperature(20,0)));
		assertTrue(type.canHaveAsTemperature(new Temperature(0,0)));
		assertTrue(type.canHaveAsTemperature(new Temperature(0,20)));
	}
	
	@Test
	void testEquals() {
		IngredientType type = new IngredientType() ; 
		IngredientType type2 =  new IngredientType() ; 
		IngredientType in = new IngredientType("Name", State.LIQUID, 0, 0) ;
		assertTrue(!type.equals(null)) ; 
		assertTrue(!type.equals(new Temperature())) ;
		assertTrue(!type.equals(in)) ; 
		assertTrue(type.equals(type)) ; 
		assertTrue(type.equals(type2)) ; 
		
		
	}

}
