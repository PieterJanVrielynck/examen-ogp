package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alchemie.AlchemicIngredient;
import alchemie.IngredientContainer;
import alchemie.IngredientType;
import alchemie.Laboratory;
import alchemie.Quantity;
import alchemie.State;
import alchemie.Transmogrifier;

/**
 * Test class for the Transmogrifier class from the 'alchemie' package.
 * @author Yoshi vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
class TransmogrifierTest {
	
	//For tests relevant to the Device (abstract) and DeviceSingleContainer (abstract), see the CoolingBoxTest test class.
	
	Laboratory laboratory;
	
	//notLinked: not part of a Laboratory: can't be executed and can't have any IngredientContainer's.
	Transmogrifier validTM;
	Transmogrifier notLinkedTM;
	
	//No invalid containers, because these wouldn't be accepted by the Laboratory.
	//All tests relative to the Laboratory can be found within the Laboratory test class from the package
	//testAlchemic.
	IngredientContainer validContainer;
	
	//No invalid ingredients, because these wouldn't be accepted by the IngredientContainer.
	//All tests relative to the IngredientContainer can be found within the IngredientContainer test class from the package
	//testAlchemic.
	AlchemicIngredient validIngredient;
	
	@BeforeEach
	void setUp() throws Exception {
		laboratory = new Laboratory(500);
		validTM = new Transmogrifier(State.LIQUID);
		notLinkedTM = new Transmogrifier(State.LIQUID);
		validIngredient = new AlchemicIngredient(State.POWDER, new IngredientType("Ingredienttypeone", State.POWDER, 100, 0),5,Quantity.SPOON); 
		validContainer = new IngredientContainer(Quantity.CHEST, validIngredient);
		
		//REMINDER: the container validContainer will be terminated at the end of the SetUp() method.
		laboratory.addDevice(validTM);
		laboratory.addIngredient(validContainer);
	}

	@Test
	void testExecute() {
		try {
			notLinkedTM.execute();
			fail("Succesfully executed an Transmogrifier that's not part of a Laboratory.");
		} catch (IllegalStateException e) {}
		
		try {
			validTM.execute();
			fail("Succesfully executed an Transmogrifier without any ingredients.");
		} catch (IllegalStateException e) {}
		
		IngredientContainer retrieveContainer = laboratory.retrieveIngredient(validIngredient.getSimpleName());
		validTM.addIngredientContainer(retrieveContainer);
		validTM.execute();
		assertTrue(validTM.isExecuted());
		AlchemicIngredient resultIngredient = validTM.getResult().getIngredient();
		validTM.clean();
		assertTrue(validTM.isCleaned());
		
		assertEquals(resultIngredient.getState(), validTM.getState());
		assertEquals(resultIngredient.getSimpleName(), validIngredient.getSimpleName());
		assertEquals(resultIngredient.getAmount(), validIngredient.getAmount() * validIngredient.getUnit().convertTo(resultIngredient.getUnit()));
		
		validTM.setState(State.POWDER);
		validTM.addIngredientContainer(new IngredientContainer(resultIngredient.getState().getMaxQuantityToRetrieveFromLaboratory(), resultIngredient));
		validTM.execute();
		assertTrue(validTM.isExecuted());
		resultIngredient = validTM.getResult().getIngredient();
		validTM.clean();
		assertTrue(validTM.isCleaned());
		
		assertEquals(resultIngredient.getState(), validTM.getState());
		assertEquals(resultIngredient.getSimpleName(), validIngredient.getSimpleName());
		assertEquals(resultIngredient.getAmount(), validIngredient.getAmount() * validIngredient.getUnit().convertTo(resultIngredient.getUnit()));
	}

	@Test
	void testTransmogrifier() {
		for (State state:State.values()) {
			validTM = new Transmogrifier(state);
			assertEquals(validTM.getState(), state);
		}
		
		try {
			validTM = new Transmogrifier(null);
			fail("Successfully set the State of the Transmogrifier to a non-effective State.");
		} catch (IllegalArgumentException e) {};
	}

	@Test
	void testCanHaveAsState() {
		assertFalse(validTM.canHaveAsState(null));
		for (State state: State.values()) {
			assertTrue(validTM.canHaveAsState(state));
		}
	}

	@Test
	void testGetState() {
		for (State state:State.values()) {
			validTM = new Transmogrifier(state);
			assertEquals(validTM.getState(), state);
		}
	}

	@Test
	void testSetState() {
		for (State state:State.values()) {
			validTM.setState(state);
			assertEquals(validTM.getState(), state);
		}
		try {
			validTM.setState(null);
			fail("Successfully set the State of the Transmogrifier to a non-effective State.");
		} catch (IllegalArgumentException e) {};
	}

}
