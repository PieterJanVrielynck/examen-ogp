package testAlchemic;
import alchemie.*;
import temperature.Temperature;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


//For tests relevant to the Device (abstract), see the test class for the class CoolingBox.
class KettleTest {
	
	Laboratory laboratory;
	
	IngredientContainer validContainerOne ; 
	IngredientContainer validContainerTwo ; 
	IngredientContainer validContainerThree ; 
	
	Kettle validKettle ; 
	Kettle notLinkedKettle ; 
	
	AlchemicIngredient ingredientOne ; 
	AlchemicIngredient ingredientTwo ; 
	AlchemicIngredient ingredientThree ; 
	@BeforeEach
	void setUp() throws Exception {
		laboratory = new Laboratory(500); 
		validKettle = new Kettle() ; 
		notLinkedKettle = new Kettle() ; 
		
		ingredientOne = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypeone", State.LIQUID, 100, 0),5,Quantity.SPOON) ; 
		ingredientTwo = new AlchemicIngredient(State.POWDER, new IngredientType("Ingredienttypetwo", State.POWDER, 0, 0), 5, Quantity.SPOON);
		ingredientThree = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypethree", State.LIQUID, 0, 0), 5, Quantity.SPOON);
		
		validContainerOne = new IngredientContainer(Quantity.BARREL, ingredientOne ) ; 
        validContainerTwo  = new IngredientContainer(Quantity.CHEST, ingredientTwo); 
        validContainerThree = new IngredientContainer(Quantity.JUG, ingredientThree) ; 
        
        laboratory.addDevice(validKettle); 
		laboratory.addIngredient(validContainerOne);
		laboratory.addIngredient(validContainerTwo);
		laboratory.addIngredient(validContainerThree);
	}

	@Test
	void testExecute() {
		try {
			notLinkedKettle.execute();
			fail("Execution of a non-linked kettle succeeded.");
		} catch (IllegalStateException e) {};
		
		try {
			validKettle.execute();
			fail("Succesfully executed an kettle without any ingredients.");
		} catch (IllegalStateException e) {};
		validContainerOne = laboratory.retrieveIngredient("Ingredienttypeone") ; 
		validContainerTwo = laboratory.retrieveIngredient("Ingredienttypetwo") ;
		validContainerThree = laboratory.retrieveIngredient("Ingredienttypethree") ;
		
		validKettle.addIngredientContainers(validContainerOne,validContainerTwo,validContainerThree  );
		validKettle.execute(); 
		assertTrue(validKettle.isExecuted());
		AlchemicMixture result = (AlchemicMixture) validKettle.getResult().getIngredient() ; 
		assertTrue(validKettle.getResult().getState() == State.LIQUID) ; 
		assertTrue(result.getType().getTemperature().equals(new Temperature(0,0))) ; 
	}

	@Test
	void testAddIngredientContainers() {
		validContainerOne = laboratory.retrieveIngredient("Ingredienttypeone") ; 
		validContainerTwo = laboratory.retrieveIngredient("Ingredienttypetwo") ;
		validContainerThree = laboratory.retrieveIngredient("Ingredienttypethree") ;
		validKettle.addIngredientContainers(validContainerOne,validContainerTwo,validContainerThree  );
		assertTrue(validKettle.getNbIngredientContainers() == 3) ; 
	}


}
