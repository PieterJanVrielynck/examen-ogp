package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import alchemie.Quantity;
import alchemie.State;

/**
 * Test class for the Quantity enumerator from the 'alchemie' package.
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
class QuantityTest {


	@Test
	void testHasAsPossibleState() {
		for (Quantity quantity:Quantity.values()) {
			ArrayList<State> states = quantity.getAllPOSSIBLE_STATES();
			for (State state: State.values()) {
				if (states.contains(state)) {
					assertTrue(quantity.hasAsPossibleState(state));
				}
			}
		}
	}

	@Test
	void testGetAllPOSSIBLE_STATES() {
		for (Quantity quantity:Quantity.values()) {
			ArrayList<State> states = new ArrayList<State>();
			for (State state:State.values()) {
				if (quantity.hasAsPossibleState(state)) {
					states.add(state);
				}
			}
			for (State state:states) {
				assertTrue(quantity.getAllPOSSIBLE_STATES().contains(state));
			}
		}
	}

	@Test
	void testConvertTo() {
		for (Quantity quantity1:Quantity.values()) {
			for (Quantity quantity2:Quantity.values()){
				assertEquals(quantity1.convertTo(quantity2), quantity1.getValue()/quantity2.getValue());
			}
		}
	}

}
