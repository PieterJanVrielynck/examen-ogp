package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import recipe.Recipe;
import recipe.RecipeBook;

class RecipeBookTest {

	@Test
	void testRecipeBook() {
		RecipeBook book = new RecipeBook(100); 
		assertTrue(book.getNumberOfPages()== 100) ; 
		for( int page = 1; page <= 100 ; page ++) {
			assertTrue(book.isFreePage(page));
		}
		try {
			book = new RecipeBook(-100); 
			assertTrue(false ) ; 
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true ) ; 
		}
	}

	@Test
	void testIsValidNumberOfPages() {
		assertTrue(RecipeBook.isValidNumberOfPages(1) ) ; 
		assertTrue(RecipeBook.isValidNumberOfPages(100) ) ;
		assertTrue(!RecipeBook.isValidNumberOfPages(0) ) ;
		assertTrue(!RecipeBook.isValidNumberOfPages(-1000) ) ;
		
	}

	@Test
	void testHasAsPage() {
		RecipeBook book = new RecipeBook(1);
		assertTrue(book.hasAsPage(1));
		assertTrue(!book.hasAsPage(2));
		assertTrue(!book.hasAsPage(-2));
	}

	@Test
	void testGetPageNumberOf() {
		RecipeBook book = new RecipeBook(1);
		RecipeBook book2 = new RecipeBook(4);
		Recipe recipe1 = new Recipe(book) ;
	    assertTrue(book.getPageNumberOf(recipe1) == 1) ; 
	    assertTrue(book2.getPageNumberOf(recipe1) == -1) ;
	    assertTrue(book2.getPageNumberOf(null) == 1) ; 
	}

	@Test
	void testIsFull() {
		RecipeBook book = new RecipeBook(1); 
		assertTrue(! book.isFull()); 
		@SuppressWarnings("unused")
		Recipe recipe1 = new Recipe(book) ; 
		assertTrue( book.isFull()); 
	}

	@Test
	void testContainsRecipe() {
		RecipeBook book = new RecipeBook(1);
		RecipeBook book2 = new RecipeBook(4);
		Recipe recipe1 = new Recipe(book) ;
		Recipe recipe2 = new Recipe(book2) ;
		assertTrue(book.containsRecipe(recipe1)) ; 
		assertTrue(!book.containsRecipe(null)) ; 
		assertTrue(!book.containsRecipe(recipe2)) ; 
		assertTrue(book2.containsRecipe(null)) ; 
	}

}
