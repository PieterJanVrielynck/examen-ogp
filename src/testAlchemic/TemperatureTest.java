package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import temperature.Temperature;

class TemperatureTest {

	@Test
	void testTemperatureLongLong() {
		Temperature temp = new Temperature(0,20); 
		assertTrue(temp.getTemperatureCold() == 0 && temp.getTemperatureHeat() == 20);
		temp = new Temperature(20, 0); 
		assertTrue(temp.getTemperatureCold() == 20 && temp.getTemperatureHeat() == 0);
		temp = new Temperature(20, 20); 
		assertTrue(temp.getTemperatureCold() == 20 && temp.getTemperatureHeat() == 0);
		temp = new Temperature(Temperature.getMaxTemperature(), 0);
		assertTrue(temp.getTemperatureCold() == Temperature.getMaxTemperature() && temp.getTemperatureHeat() == 0);
		temp = new Temperature(-20, -20); 
		assertTrue(temp.getTemperatureCold() == 0 && temp.getTemperatureHeat() == 0);
		temp = new Temperature(-20, 20); 
		assertTrue(temp.getTemperatureCold() == 0 && temp.getTemperatureHeat() == 20);
	}

	@Test
	void testTemperature() {
		Temperature temp = new Temperature() ; 
		assertTrue(temp.getTemperatureCold() == 0 && temp.getTemperatureHeat() == 20);
	}

	@Test
	void testIsPossibleTemperatureCold() {
		assertTrue(Temperature.isPossibleTemperatureCold(0)) ; 
		assertTrue(Temperature.isPossibleTemperatureCold(1000)) ;
		assertTrue(Temperature.isPossibleTemperatureCold(Temperature.getMaxTemperature())) ; 
		assertFalse(Temperature.isPossibleTemperatureCold(Temperature.getMaxTemperature()+1)) ;
		assertFalse(Temperature.isPossibleTemperatureCold(-Temperature.getMaxTemperature())) ;
		assertFalse(Temperature.isPossibleTemperatureCold(-20)) ;
	}

	@Test
	void testCanHaveAsTemperatureCold() {
		Temperature temp = new Temperature(0,0) ; 
		assertTrue(temp.canHaveAsTemperatureCold(0)) ; 
		assertTrue(temp.canHaveAsTemperatureCold(20)) ;
		assertTrue(temp.canHaveAsTemperatureCold(Temperature.getMaxTemperature())) ;
		temp = new Temperature() ; 
		assertTrue(temp.canHaveAsTemperatureCold(0)) ;
		assertTrue(!temp.canHaveAsTemperatureCold(20)) ;
		assertTrue(!temp.canHaveAsTemperatureCold(Temperature.getMaxTemperature())) ;
	}

	@Test
	void testIsPossibleTemperatureHeat() {
		assertTrue(Temperature.isPossibleTemperatureHeat(0)) ; 
		assertTrue(Temperature.isPossibleTemperatureHeat(1000)) ;
		assertTrue(Temperature.isPossibleTemperatureHeat(Temperature.getMaxTemperature())) ; 
		assertFalse(Temperature.isPossibleTemperatureHeat(Temperature.getMaxTemperature()+1)) ;
		assertFalse(Temperature.isPossibleTemperatureHeat(-Temperature.getMaxTemperature())) ;
		assertFalse(Temperature.isPossibleTemperatureHeat(-20)) ;
	}

	@Test
	void testCanHaveAsTemperatureHeat() {
		Temperature temp = new Temperature(0,0) ; 
		assertTrue(temp.canHaveAsTemperatureHeat(0)) ; 
		assertTrue(temp.canHaveAsTemperatureHeat(20)) ;
		assertTrue(temp.canHaveAsTemperatureHeat(Temperature.getMaxTemperature())) ;
		temp = new Temperature(20,0) ; 
		assertTrue(temp.canHaveAsTemperatureHeat(0)) ;
		assertTrue(!temp.canHaveAsTemperatureHeat(20)) ;
		assertTrue(!temp.canHaveAsTemperatureHeat(Temperature.getMaxTemperature())) ;
	}

	@Test
	void testMatchesTemperatureColdHeat() {
		assertTrue(Temperature.matchesTemperatureColdHeat(0, 0)) ; 
		assertTrue(Temperature.matchesTemperatureColdHeat(0, 20)) ;
		assertTrue(Temperature.matchesTemperatureColdHeat(20, 0)) ;
		assertTrue(!Temperature.matchesTemperatureColdHeat(20, 20)) ;
	}

	@Test
	void testIsHotterThan() {
		Temperature temp1 = new Temperature(0,0) ;
		Temperature temp2 = new Temperature(0,20) ;
		Temperature temp3 = new Temperature(20,0) ;
		assertTrue(temp1.isHotterThan(temp3)) ;
		assertTrue(!temp1.isHotterThan(temp2)) ;
		assertTrue(!temp1.isHotterThan(temp1)) ;
		assertTrue(!temp1.isHotterThan(null)) ;
	}

	@Test
	void testIsColderThan() {
		Temperature temp1 = new Temperature(0,0) ;
		Temperature temp2 = new Temperature(0,20) ;
		Temperature temp3 = new Temperature(20,0) ;
		assertTrue(!temp1.isColderThan(temp3)) ;
		assertTrue(temp1.isColderThan(temp2)) ;
		assertTrue(!temp1.isColderThan(temp1)) ;
		assertTrue(!temp1.isColderThan(null)) ;
	}

	@Test
	void testGetDifferenceNormal() {
		Temperature temp1 = new Temperature(0,0) ;
		Temperature temp2 = new Temperature(0,20) ;
		Temperature temp3 = new Temperature(20,0) ;
		assertTrue(temp1.getDifference(temp2)== -20 ) ; 
		assertTrue(temp1.getDifference(temp3)== 20 ) ;
		assertTrue(temp1.getDifference(temp1)== 0 ) ;
	}
	
	@Test
	void testGetDifferenceExceptionel() {
		Temperature temp1 = new Temperature(0,0) ;
		try {
			temp1.getDifference(null) ; 
		}
		catch(IllegalArgumentException exception ) {
			assertTrue(true) ; 
		}
	}

	@Test
	void testConvertTemperatureToLongNormal() {
		Temperature temp1 = new Temperature(0,0) ;
		Temperature temp2 = new Temperature(0,20) ;
		Temperature temp3 = new Temperature(20,0) ;
		assertTrue(Temperature.convertTemperatureToLong(temp3) == -20) ; 
		assertTrue(Temperature.convertTemperatureToLong(temp2) == 20) ; 
		assertTrue(Temperature.convertTemperatureToLong(temp1) == 0) ; 
	}
	@Test
	void testConvertTemperatureToLongExceptionel() {
		try {
			Temperature.convertTemperatureToLong(null) ; 
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
	}
	

	@Test
	void testConvertLongToTemperature() {
		assertTrue(Temperature.convertLongToTemperature(20).equals(new Temperature())) ; 
		assertTrue(Temperature.convertLongToTemperature(-20).equals(new Temperature(20,0))) ; 
		assertTrue(Temperature.convertLongToTemperature(-Temperature.getMaxTemperature()-1).equals(new Temperature(Temperature.getMaxTemperature(),0))) ;
		assertTrue(Temperature.convertLongToTemperature(Temperature.getMaxTemperature()+1).equals(new Temperature(0,Temperature.getMaxTemperature()))) ;
	}

	@Test
	void testHeat() {
		Temperature temp1 = new Temperature(0,0) ;
		Temperature result = temp1.heat(10); 
		assertTrue(result.equals(new Temperature(0,10))) ; 
		result = temp1.heat(Temperature.getMaxTemperature()); 
		assertTrue(result.equals(new Temperature(0,Temperature.getMaxTemperature()))) ;
		result = result.heat(Long.MAX_VALUE); 
		assertTrue(result.equals(new Temperature(0,Temperature.getMaxTemperature()))) ;
		result = temp1.heat(-Temperature.getMaxTemperature()); 
		assertTrue(result.equals(new Temperature(0,0))) ; 
	}

	@Test
	void testCool() {
		Temperature temp1 = new Temperature(0,0) ;
		Temperature result = temp1.cool(10); 
		assertTrue(result.equals(new Temperature(10,0))) ; 
		result = temp1.cool(Temperature.getMaxTemperature()); 
		assertTrue(result.equals(new Temperature(Temperature.getMaxTemperature(),0))) ;
		result = result.cool(Long.MAX_VALUE); 
		assertTrue(result.equals(new Temperature(Temperature.getMaxTemperature(),0))) ;
		result = temp1.cool(-Temperature.getMaxTemperature()); 
		assertTrue(result.equals(new Temperature(0,0))) ; 
	}

	@Test
	void testIsValidMaxTemperature() {
		assertTrue(Temperature.isValidMaxTemperature(0));
		assertTrue(Temperature.isValidMaxTemperature(100000));
		assertTrue(Temperature.isValidMaxTemperature(Long.MAX_VALUE));
		assertTrue(!Temperature.isValidMaxTemperature(Long.MIN_VALUE));
		assertTrue(!Temperature.isValidMaxTemperature(-1000));
	}

	@Test
	void testEqualsObject() {
		Temperature temp1 = new Temperature(0,0) ;
		assertTrue(!temp1.equals(null) ); 
		assertTrue(!temp1.equals(new Integer(0))) ;
		assertTrue(!temp1.equals(new Temperature()) ); 
		assertTrue(temp1.equals(new Temperature(0,0))) ; 
	}

}
