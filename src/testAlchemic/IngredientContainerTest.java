package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alchemie.AlchemicIngredient;
import alchemie.IngredientContainer;
import alchemie.IngredientType;
import alchemie.Quantity;
import alchemie.State;

/**
 * Test class for the IngredientContainer class from the 'alchemie' package.
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
class IngredientContainerTest {
	
	IngredientContainer emptyContainer;
	IngredientContainer validContainerLIQUID;
	IngredientContainer validContainerPOWDER;
	
	Quantity validCapacityLIQUID = Quantity.BOTTLE;
	Quantity validCapacityPOWDER = Quantity.CHEST;
	
	AlchemicIngredient validIngredientLIQUID = new AlchemicIngredient(State.LIQUID, new IngredientType("Water", State.LIQUID, 20, 0), 1, Quantity.SPOON);
	AlchemicIngredient exceededAmountIngredientLIQUID = new AlchemicIngredient(State.LIQUID, new IngredientType("Water", State.LIQUID, 20, 0), 2, Quantity.STOREROOM);
	AlchemicIngredient validIngredientPOWDER = new AlchemicIngredient(State.POWDER, new IngredientType("Buskruit", State.POWDER, 20,0), 1, Quantity.PINCH);
	AlchemicIngredient exceededAmountIngredientPOWDER = new AlchemicIngredient(State.POWDER, new IngredientType("Buskruit", State.POWDER, 20,0), 3, Quantity.STOREROOM);
	
	Quantity invalidLowCapacityLIQUID = State.LIQUID.getMinQuantity();
	Quantity invalidHighCapacityLIQUID = State.LIQUID.getMaxQuantity();
	Quantity invalidLowCapacityPOWDER = State.POWDER.getMinQuantity();
	Quantity invalidHighCapacityPOWDER = State.POWDER.getMaxQuantity();

	@BeforeEach
	void setUp() throws Exception {
		emptyContainer = new IngredientContainer(validCapacityLIQUID, State.LIQUID);
		validContainerLIQUID = new IngredientContainer(validCapacityLIQUID, validIngredientLIQUID);
		validContainerPOWDER = new IngredientContainer(validCapacityPOWDER, validIngredientPOWDER);
	}


	@Test
	void testIngredientContainerQuantityState_IllegalCase() {
		Quantity[] quantities = {null,invalidLowCapacityLIQUID, invalidHighCapacityLIQUID, invalidLowCapacityPOWDER, invalidHighCapacityPOWDER};
		State[] states = State.values();
		@SuppressWarnings("unused")
		IngredientContainer testContainer;
		for (Quantity quantity:quantities) {
			for (State state:states) {
				try {
					testContainer = new IngredientContainer(quantity, state);
					fail("Initialization of the testContainer with quantity " + quantity.toString() + 
							" and state " + state.toString() + " succeeded.");
				} catch (IllegalArgumentException e) {}
			}
		}
	}
	
	
	@Test
	void testIngredientContainerQuantityState_LegalCase() {
		@SuppressWarnings("unused")
		IngredientContainer testContainer = new IngredientContainer(validCapacityLIQUID, State.LIQUID);
		testContainer = new IngredientContainer(validCapacityPOWDER, State.POWDER);
	}
		
		
		


	@Test
	void testIngredientContainerQuantityAlchemicIngredient() {
		Quantity[] quantities = {null,invalidLowCapacityLIQUID, invalidHighCapacityLIQUID, invalidLowCapacityPOWDER, invalidHighCapacityPOWDER};
		AlchemicIngredient[] ingredients = {null,exceededAmountIngredientLIQUID, exceededAmountIngredientPOWDER};
		@SuppressWarnings("unused")
		IngredientContainer testContainer;
		for (Quantity quantity:quantities) {
			for (AlchemicIngredient ingredient:ingredients) {
				try{
					testContainer = new IngredientContainer(quantity, ingredient);
					fail("Initialization of the testContainer with quantity " + quantity.toString() + 
							" and ingredient " + ingredient.getSimpleName() + " succeeded.");
				} catch (IllegalArgumentException e) {}
			}
		}
		
		//No tests for the legal cases since these are already created by the method SetUp().
		
	}

	@Test
	void testCompareTo() {
		assertTrue(validContainerLIQUID.compareTo(validContainerPOWDER) > 0);
		assertTrue(validContainerPOWDER.compareTo(validContainerLIQUID) < 0);
		assertTrue(validContainerLIQUID.compareTo(validContainerLIQUID) == 0);
	}

	@Test
	void testTerminate() {
		IngredientContainer[] containers = {emptyContainer, validContainerLIQUID, validContainerPOWDER};
		for (IngredientContainer container:containers) {
			container.terminate();
			//The method isTerminated() already checks if the container is now empty.
			assertTrue(container.isTerminated());
		}
	}


	@Test
	void testGetCapacity() {
		assertEquals(emptyContainer.getCapacity(),validCapacityLIQUID);
		assertEquals(validContainerLIQUID.getCapacity(), validCapacityLIQUID);
		assertEquals(validContainerPOWDER.getCapacity(), validCapacityPOWDER);
	}

	@Test
	void testIsProperCapacity() {
		Quantity[] validCapacities = {validCapacityLIQUID, validCapacityPOWDER};
		Quantity[] invalidCapacities = {null,invalidHighCapacityLIQUID, invalidHighCapacityPOWDER, invalidLowCapacityLIQUID, invalidLowCapacityPOWDER};
		for (Quantity validCapacity: validCapacities) {
			for (State state:validCapacity.getAllPOSSIBLE_STATES()) {
				assertTrue(IngredientContainer.isProperCapacity(validCapacity, state));
			}
		}
		for (Quantity invalidCapacity: invalidCapacities) {
			for (State state:State.values()) {
				assertFalse(IngredientContainer.isProperCapacity(invalidCapacity, state));
			}
		}
	}

	@Test
	void testGetIngredient() {
		assertNull(emptyContainer.getIngredient());
		assertEquals(validContainerLIQUID.getIngredient(), validIngredientLIQUID);
		assertEquals(validContainerPOWDER.getIngredient(), validIngredientPOWDER);
	}

	@Test
	void testCanHaveAsIngredient() {
		//Method only works if the container is still empty.
		AlchemicIngredient[] invalidIngredients = {null,validIngredientPOWDER, exceededAmountIngredientLIQUID, exceededAmountIngredientPOWDER};
		for (AlchemicIngredient ingredient:invalidIngredients) {
			assertFalse(emptyContainer.canHaveAsIngredient(ingredient));
		}
		
		assertTrue(emptyContainer.canHaveAsIngredient(validIngredientLIQUID));
	}

	@Test
	void testIsEmpty() {
		assertTrue(emptyContainer.isEmpty());
		emptyContainer.setIngredient(validIngredientLIQUID);
		assertFalse(emptyContainer.isEmpty());
		assertFalse(validContainerLIQUID.isEmpty());
		assertFalse(validContainerPOWDER.isEmpty());
	}

	@Test
	void testSetIngredient() {
		emptyContainer.setIngredient(validIngredientLIQUID);
		assertEquals(emptyContainer.getIngredient(), validIngredientLIQUID);
		emptyContainer = new IngredientContainer(validCapacityLIQUID, State.LIQUID);
		
		AlchemicIngredient[] invalidIngredients = {null, validIngredientPOWDER, exceededAmountIngredientLIQUID, exceededAmountIngredientPOWDER};
		for (AlchemicIngredient ingredient:invalidIngredients) {
			try {
				emptyContainer.setIngredient(ingredient);
				fail("Ingredient " + ingredient.getSimpleName() + " succesfully added to the empty container.");
			} catch (IllegalArgumentException e) {};
		}
	}

	@Test
	void testGetState() {
		assertEquals(validContainerLIQUID.getState(), State.LIQUID);
		assertEquals(validContainerPOWDER.getState(), State.POWDER);
	}

}
