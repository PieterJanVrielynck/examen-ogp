package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alchemie.GeneralIngredient;

class GeneralIngredientTest {

	@Test
	void testIsValidLegalCharacterString() {
		assertTrue(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z()' ]+")) ; 
		assertFalse(GeneralIngredient.isValidLegalCharacterString("a-zA-Z()' ]+")) ; 
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z()' ]")) ;
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z()' +")) ;
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[A-Z()' ]")) ;
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-z()' ]+")) ;
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z)' ]+")); 
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z(' ]+")) ; 
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z() ]+")) ;
		assertFalse(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z()']+")) ; 
		assertTrue(GeneralIngredient.isValidLegalCharacterString("[a-zA-Z()'@[}9 ]+")) ;
	}
	


}
