package testAlchemic;
import alchemie.AlchemicMixture;
import alchemie.IngredientType;
import alchemie.MixtureIngredientType;
import alchemie.Quantity;
import alchemie.State;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AlchemicMixtureTest {

	@Test
	void testCanHaveAsPossibleType() {
		IngredientType typeWrong = new IngredientType() ; 
		MixtureIngredientType typeCorrect = new MixtureIngredientType("Beer mixed with Cola", State.LIQUID, 0, 0) ;
		AlchemicMixture mixture = new AlchemicMixture(State.LIQUID, typeCorrect , 0, Quantity.SPOON, 0) ; 
		assertTrue(mixture.canHaveAsPossibleType(typeCorrect) ) ; 
		assertFalse(mixture.canHaveAsPossibleType(typeWrong) ) ; 
	}

	@Test
	void testAlchemicMixture() {
		MixtureIngredientType typeCorrect = new MixtureIngredientType("Beer mixed with Cola", State.LIQUID, 0, 1) ;
		AlchemicMixture mixture = new AlchemicMixture(State.LIQUID, typeCorrect , 0, Quantity.SPOON, 1) ; 
		assertTrue(mixture.getType().equals(typeCorrect)) ; 
	}

	@Test
	void testCanHaveAsSpecialName() {
		MixtureIngredientType typeCorrect = new MixtureIngredientType("Beer mixed with Cola", State.LIQUID, 0, 1) ;
		AlchemicMixture mixture = new AlchemicMixture(State.LIQUID, typeCorrect , 0, Quantity.SPOON, 1) ; 
		assertTrue(mixture.canHaveAsSpecialName(" Water ")) ; // one word 
		assertTrue(mixture.canHaveAsSpecialName("Wat")) ; 
		assertTrue(mixture.canHaveAsSpecialName("Water")) ;
		assertFalse(mixture.canHaveAsSpecialName("Wa")) ;
		assertFalse(mixture.canHaveAsSpecialName("wat")) ;
		assertFalse(mixture.canHaveAsSpecialName("WAter")) ;
		assertFalse(mixture.canHaveAsSpecialName("Heated")) ; 
		assertFalse(mixture.canHaveAsSpecialName("H@d")) ;
		assertFalse(mixture.canHaveAsSpecialName(null)) ;
		assertTrue(mixture.canHaveAsSpecialName("Nitrogin Gas")) ; // one+ word
		assertTrue(mixture.canHaveAsSpecialName("Ni Ga")) ; 
		assertFalse(mixture.canHaveAsSpecialName("Ni gas")) ;
		assertFalse(mixture.canHaveAsSpecialName("Ni Gas Heated")) ; 
		assertFalse(mixture.canHaveAsSpecialName("Ni Gas Go Hi Hy Hi Ni op")) ;
		assertFalse(mixture.canHaveAsSpecialName("Ni Gas Go Hi Hy Hi Ni OP")) ;
		assertTrue(mixture.canHaveAsSpecialName("Ni Gas Go Hi Hy Hi Ni Op")) ;
	}



}
