package testAlchemic;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alchemie.MixtureIngredientType;
import alchemie.State;

class MixtureIngredientTypeTest {

	@Test
	void canHaveAsNameTest() {
		MixtureIngredientType type = new MixtureIngredientType("Beer mixed with Cola", State.LIQUID, 0, 0) ;
		assertTrue(type.canHaveAsName("Amora Sang mixed with Someone and Sometime")) ; 
		assertTrue(type.canHaveAsName("Amora Sang mixed with Beer, Chocalate Black, Someone and Sometime"));
		assertFalse(type.canHaveAsName("Beer mixed with Qube mixed with Ether")); 
		assertFalse(type.canHaveAsName("Water") ) ;
		assertFalse(type.canHaveAsName("Beer,  Qube, and Ether"));
		assertFalse(type.canHaveAsName("Amora Sang@ mixed with Beer, Chocalate Black, Someone and Sometime"));
		assertFalse(type.canHaveAsName("Cola mixed with Beer")) ;
		assertFalse(type.canHaveAsName("Amora Sang mixed with Beer, Chocalate Black and Someone and Sometime")) ; 
		assertFalse(type.canHaveAsName("Amora Sang mixed with Beer, Chocalate Black, Someone and ")) ;
		assertFalse(type.canHaveAsName("Amora Sang mixed with and Sometime") ) ;  
		assertFalse(type.canHaveAsName("Amora Sang MIXED WITH Beer, Chocalate Black, Someone and Sometime"));
		assertFalse(type.canHaveAsName("Amora Sang mixed with Beer, Chocalate Black, Sometime and Someone"));
		assertFalse(type.canHaveAsName("Amora Sang mixed with Sometime, Chocalate Black, Someone and Beer "));
		assertFalse(type.canHaveAsName("Amora Sang mixed with Beer, Chocalate@ Black, Someone and Sometime")) ;
		assertFalse(type.canHaveAsName("Amora Sang mixed with Beer, Chocalate Black, Someone and Sometime@")) ;
	}
	
	@Test
	void testMixtureIngredientType() {
		MixtureIngredientType type = new MixtureIngredientType("Beer mixed with Cola", State.LIQUID, 0, 0) ;
		assertTrue(type.getName().equals("Beer mixed with Cola")) ; 
		try {
			type = new MixtureIngredientType("Beer mixed with Aola", State.LIQUID, 0, 0) ;
			assertFalse(true)  ; 
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
	}

}
