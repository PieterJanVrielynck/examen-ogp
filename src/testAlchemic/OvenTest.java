package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alchemie.AlchemicIngredient;
import alchemie.IngredientContainer;
import alchemie.IngredientType;
import alchemie.Laboratory;
import alchemie.Oven;
import alchemie.Quantity;
import alchemie.State;
import temperature.Temperature;

/**
 * Test class for the Oven class from the 'alchemie' package.
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
class OvenTest {
	
	//For tests relevant to the Device (abstract), DeviceSingleContainer (abstract) and
	//DeviceTemperatureController (abstract), see the CoolingBoxTest test class.
	
	Laboratory laboratory;
	
	//notLinked: not part of a Laboratory: can't be executed and can't have any IngredientContainer's.
	Oven validOven;
	Oven notLinkedOven;
	
	//No invalid containers, because these wouldn't be accepted by the Laboratory.
	//All tests relative to the Laboratory can be found within the Laboratory test class from the package
	//testAlchemic.
	IngredientContainer validContainerHOT;
	IngredientContainer validContainerCOLD;
	IngredientContainer validContainerEXTRA;
	
	//No invalid ingredients, because these wouldn't be accepted by the IngredientContainer.
	//All tests relative to the IngredientContainer can be found within the IngredientContainer test class from the package
	//testAlchemic.
	AlchemicIngredient validIngredientHOT;
	AlchemicIngredient validIngredientCOLD;
	
	@BeforeEach
	void setUp() throws Exception {
		laboratory = new Laboratory(500);
		validOven = new Oven(0, 50);
		notLinkedOven = new Oven(0, 50);
		validIngredientHOT = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypeone", State.LIQUID, 100, 0),5,Quantity.SPOON);
		validIngredientCOLD = new AlchemicIngredient(State.POWDER, new IngredientType("Ingredienttypetwo", State.POWDER, 0, 0), 5, Quantity.SPOON);
		
		//REMINDER: the containers validContainerHOT and validContainerCOLD will be terminated at the end of the SetUp() method.
		validContainerHOT = new IngredientContainer(Quantity.BARREL, validIngredientHOT);
		validContainerCOLD = new IngredientContainer(Quantity.CHEST, validIngredientCOLD);					
		laboratory.addDevice(validOven);
		laboratory.addIngredient(validContainerCOLD);
		laboratory.addIngredient(validContainerHOT);
	}

	@Test
	void testExecute() {
		try {
			notLinkedOven.execute();
			fail("Execution of a non-linked Oven succeeded.");
		} catch (IllegalStateException e) {};

		IngredientContainer containerHOT = laboratory.retrieveIngredient("Ingredienttypeone");
		IngredientContainer containerCOLD = laboratory.retrieveIngredient("Ingredienttypetwo");
		assertEquals(containerHOT.getIngredient().getTemperature(), validIngredientHOT.getTemperature());
		assertEquals(containerCOLD.getIngredient().getTemperature(), validIngredientCOLD.getTemperature());
		
		try {
			validOven.execute();
			fail("Succesfully executed an Oven without any ingredients.");
		} catch (IllegalStateException e) {};
		
		
		validOven.addIngredientContainer(containerHOT);
		assertTrue(containerHOT.isTerminated());
		validOven.execute();
		assertTrue(validOven.isExecuted());
		assertEquals(validOven.getResult().getIngredient().getTemperature().getTemperatureCold(), 
				validIngredientHOT.getTemperature().getTemperatureCold());
		assertEquals(validOven.getResult().getIngredient().getTemperature().getTemperatureHeat(), 
				validIngredientHOT.getTemperature().getTemperatureHeat());
		validOven.clean();
		assertTrue(validOven.isCleaned());
		assertFalse(validOven.isExecuted());
		
		//	!!!
		validOven.setTemperature(0, 5000);
		
		validOven.addIngredientContainer(containerCOLD);
		validOven.execute();
		assertTrue(validOven.isExecuted());
		assertEquals(validOven.getResult().getIngredient().getTemperature().getTemperatureCold(), 
				validOven.getTemperature().getTemperatureCold());
		assertTrue(validOven.getResult().getIngredient().getTemperature().getTemperatureHeat() 
				<= validOven.getTemperature().getTemperatureHeat()*1.05 &&
				validOven.getResult().getIngredient().getTemperature().getTemperatureHeat() 
				>= validOven.getTemperature().getTemperatureHeat()*0.95);
		validOven.clean();
		assertTrue(validOven.isCleaned());
		assertFalse(validOven.isExecuted());
	}

	@Test
	void testOven_LegalCase() {
		assertEquals(validOven.getTemperature(), new Temperature(0,50));
		assertEquals(notLinkedOven.getTemperature(), new Temperature(0,50));
	}
	
	@Test
	void testOven_IllegalCase() {
		long[] temperatures = {Long.MAX_VALUE, Long.MIN_VALUE, 0, 500, -500};
		for (long temp1:temperatures) {
			for (long temp2:temperatures) {
				if (!((temp1 == 0 && temp2 >= 0) || (temp1 >= 0 && temp2 == 0))) {
					try {
						@SuppressWarnings("unused")
						Oven test = new Oven(temp1, temp2);
						fail("Succesfull initialization of a Oven without proper given "
								+ "temperatures " + temp1 + " and " + temp2 +".");
					} catch (IllegalArgumentException e) {}
				}
			}
		}
	}
}
