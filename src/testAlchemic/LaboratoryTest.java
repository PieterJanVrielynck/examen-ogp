package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alchemie.AlchemicIngredient;
import alchemie.AlchemicMixture;
import alchemie.CoolingBox;
import alchemie.IngredientContainer;
import alchemie.IngredientType;
import alchemie.Kettle;
import alchemie.Laboratory;
import alchemie.LaboratoryIngredientIterator;
import alchemie.Oven;
import alchemie.Quantity;
import alchemie.State;
import recipe.*;

/**
 * Test class for the Laboratory class from the 'alchemie' package, 
 * with the implementation of the LaboratoryIngredientIterator interface.
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
class LaboratoryTest {

	Laboratory validLaboratory;
	
	AlchemicIngredient ingredientOne;
	AlchemicIngredient ingredientTwo;
	
	IngredientContainer ingredientContainerOne;
	IngredientContainer ingredientContainerTwo;
	
	CoolingBox coolingBox;
	Oven oven;
	
	@BeforeEach
	void setUp() throws Exception {
		validLaboratory = new Laboratory(1);
		ingredientOne = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypeone", State.LIQUID, 100, 0), 10, Quantity.SPOON, 0);
		ingredientTwo = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypeone", State.LIQUID, 100, 0), 10, Quantity.JUG, 0);
		ingredientContainerOne = new IngredientContainer(Quantity.BOTTLE, ingredientOne);
		ingredientContainerTwo = new IngredientContainer(Quantity.BARREL, ingredientTwo);
		coolingBox = new CoolingBox(0, 0);
		oven = new Oven(0,0);
		
		validLaboratory.addDevice(oven);
		validLaboratory.addDevice(coolingBox);
		validLaboratory.addIngredient(ingredientContainerOne);
	}

	@Test
	void testLaboratory_LegalCase() {
		assertEquals(validLaboratory.getDevice(Oven.class), oven);
		assertEquals(validLaboratory.getDevice(CoolingBox.class), coolingBox);
		assertNull(validLaboratory.getDevice(Kettle.class));
		assertEquals(validLaboratory.getAmount(), ingredientOne.getAmount());
		assertEquals(validLaboratory.getAmountUnit(), ingredientOne.getUnit());
		assertEquals(validLaboratory.getCapacity(), 1);
		assertTrue(validLaboratory.hasProperDevices());
	}
	
	@Test
	void testLaboratory_IllegalCase() {
		int[] invalidCapacities = {0, -500, Integer.MIN_VALUE};
		for (int capacity:invalidCapacities) {
			try {
				@SuppressWarnings("unused")
				Laboratory testLaboratory = new Laboratory(capacity);
				fail("Successfully created a Laboratory without a proper capacity.");
			} catch (IllegalArgumentException e) {}
		}
	}

	@Test
	void testToString() {
		String eol = System.getProperty("line.separator");
		String expectedString = "Name ingredient: Ingredienttypeone" + eol +
				"Amount available: 10 SPOON" + eol + eol;
		assertEquals(validLaboratory.toString(),expectedString);
		
		validLaboratory.addIngredient(ingredientContainerTwo);
		expectedString = "Name ingredient: Ingredienttypeone" + eol +
				"Amount available: 1060 SPOON" + eol + eol;
		assertEquals(validLaboratory.toString(), expectedString);
	}

	@Test
	void testRetrieveIngredientString() {
		IngredientContainer retrieveContainer = validLaboratory.retrieveIngredient("Ingredienttypeone");
		AlchemicIngredient retrieveIngredient = retrieveContainer.getIngredient();
		assertEquals(retrieveIngredient.getState(), ingredientOne.getState());
		assertEquals(retrieveIngredient.getType(), ingredientOne.getType());
		assertEquals(retrieveIngredient.getAmount(), ingredientOne.getAmount());
		assertEquals(retrieveIngredient.getUnit(), ingredientOne.getUnit());
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		
		try {
			validLaboratory.retrieveIngredient("ThisJustDoesntExistYet");
			fail("Retrieved an ingredient that just doesn't exist from the Laboratory.");
		} catch (IllegalArgumentException e) {};
	}

	@Test
	void testRetrieveIngredientStringIntQuantity() {
		IngredientContainer retrieveContainer = validLaboratory.retrieveIngredient("Ingredienttypeone", 5, Quantity.SPOON);
		AlchemicIngredient retrieveIngredient = retrieveContainer.getIngredient();
		
		assertEquals(retrieveIngredient.getState(), ingredientOne.getState());
		assertEquals(retrieveIngredient.getType(), ingredientOne.getType());
		assertEquals(retrieveIngredient.getAmount(), 5);
		assertEquals(retrieveIngredient.getUnit(), Quantity.SPOON);
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		
		assertTrue(validLaboratory.hasIngredient("Ingredienttypeone"));
		IngredientContainer retrieveContainer2 = validLaboratory.retrieveIngredient("Ingredienttypeone");
		AlchemicIngredient retrieveIngredient2 = retrieveContainer2.getIngredient();
		
		assertEquals(retrieveIngredient2.getState(), ingredientOne.getState());
		assertEquals(retrieveIngredient2.getType(), ingredientOne.getType());
		assertEquals(retrieveIngredient2.getAmount() * retrieveIngredient.getUnit().convertTo(Quantity.SPOON), 
				ingredientOne.getAmount() * ingredientOne.getUnit().convertTo(Quantity.SPOON) - 
				retrieveIngredient.getAmount() * retrieveIngredient.getUnit().convertTo(Quantity.SPOON));
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		
		try {
			retrieveContainer = validLaboratory.retrieveIngredient("Ingredienttypeone");
			fail("Retrieved an already retrieved ingredient.");
		} catch (IllegalArgumentException e) {};
	}

	@Test
	void testAddIngredient() {
		validLaboratory.addIngredient(ingredientContainerTwo);
		
		IngredientContainer retrieveContainer = validLaboratory.retrieveIngredient("Ingredienttypeone");
		AlchemicIngredient retrieveIngredient = retrieveContainer.getIngredient();
		assertEquals(retrieveIngredient.getState(), ingredientOne.getState());
		assertEquals(retrieveIngredient.getType(), ingredientOne.getType());
		assertEquals(retrieveIngredient.getAmount(), ingredientOne.getAmount() * ingredientOne.getUnit().convertTo(retrieveIngredient.getUnit()) +
				ingredientTwo.getAmount() * ingredientTwo.getUnit().convertTo(retrieveIngredient.getUnit()));
		assertEquals(retrieveIngredient.getUnit(), ingredientOne.getUnit());
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		
		IngredientContainer[] noProperContainers = {null, new IngredientContainer(Quantity.SPOON, State.LIQUID)};
		for (IngredientContainer container:noProperContainers) {
			try {
				validLaboratory.addIngredient(container);
				fail("Successfully added an invalid IngredientContainer to the Laboratory.");
			} catch (IllegalArgumentException e) {}
		}
	}

	@Test
	void testHasIngredient() {
		assertFalse(validLaboratory.hasIngredient(null));
		assertFalse(validLaboratory.hasIngredient(""));
		assertTrue(validLaboratory.hasIngredient("Ingredienttypeone"));
		@SuppressWarnings("unused")
		IngredientContainer retrieveContainer = validLaboratory.retrieveIngredient("Ingredienttypeone");
		assertFalse(validLaboratory.hasIngredient("Ingredienttypeone"));
	}


	@Test
	void testhasProperIngredients() {
		assertTrue(validLaboratory.hasProperIngredients());
		ingredientTwo = new AlchemicMixture(ingredientTwo.getState(), ingredientTwo.getType(), 1, Quantity.SPOON, 0);
		ingredientContainerTwo = new IngredientContainer(Quantity.BARREL, ingredientTwo);
		validLaboratory.addIngredient(ingredientContainerTwo);
		assertTrue(validLaboratory.hasProperIngredients());
		validLaboratory.retrieveIngredient("Ingredienttypeone");
		assertTrue(validLaboratory.hasProperIngredients());
	}
	
	
	@Test
	void testCanHaveAsIngredient() {
		assertFalse(validLaboratory.canHaveAsIngredient(null));
		assertFalse(validLaboratory.canHaveAsIngredient(new IngredientContainer(Quantity.SPOON, State.LIQUID)));
		assertFalse(validLaboratory.canHaveAsIngredient(ingredientContainerOne));
		
		while (validLaboratory.canHaveAsIngredient(ingredientContainerTwo)) {
			//Exactly the same code as the creation of ingredientContainerTwo and ingredientTwo, everytime again.
			validLaboratory.addIngredient(new IngredientContainer(Quantity.BARREL,
					new AlchemicIngredient(ingredientTwo.getState(), ingredientTwo.getType(), 1, Quantity.SPOON, 0)));
		}
		
		assertTrue(validLaboratory.getCapacity() * Quantity.STOREROOM.convertTo(validLaboratory.getAmountUnit()) <
				validLaboratory.getAmount() + ingredientTwo.getAmount() * ingredientTwo.getUnit().convertTo(validLaboratory.getAmountUnit()));
	}
	
	@Test
	void testCanHaveAsAmount() {
		Quantity unit = validLaboratory.getAmountUnit();
		long maxPossibleAmount = (long) (validLaboratory.getCapacity() * Quantity.STOREROOM.convertTo(unit));
		assertTrue(validLaboratory.canHaveAsAmount(0));
		assertTrue(validLaboratory.canHaveAsAmount(maxPossibleAmount));
		assertTrue(validLaboratory.canHaveAsAmount(0.5*maxPossibleAmount));
		assertFalse(validLaboratory.canHaveAsAmount(-maxPossibleAmount));
		assertFalse(validLaboratory.canHaveAsAmount(Long.MIN_VALUE));
	}

	@Test
	void testGetAmount() {
		Quantity originalUnit = validLaboratory.getAmountUnit();
		long originalAmount =validLaboratory.getAmount();
		
		validLaboratory.addIngredient(ingredientContainerTwo);
		Quantity newUnit = validLaboratory.getAmountUnit();
		long newAmount =validLaboratory.getAmount();
		assertEquals(newAmount, originalAmount * originalUnit.convertTo(newUnit) +
				ingredientTwo.getAmount() * ingredientTwo.getUnit().convertTo(newUnit));
		
		validLaboratory.retrieveIngredient("Ingredienttypeone", 5, Quantity.SPOON);
		Quantity CompletelyNewUnit = validLaboratory.getAmountUnit();
		long completelyNewAmount =validLaboratory.getAmount();
		assertEquals(completelyNewAmount * CompletelyNewUnit.convertTo(Quantity.SPOON),
				newAmount * newUnit.convertTo(Quantity.SPOON) - 5 * Quantity.SPOON.getValue());
	}

	@Test
	void testGetAmountUnit() {
		validLaboratory = new Laboratory(1);
		assertEquals(validLaboratory.getAmountUnit(), Quantity.STOREROOM);
		
		AlchemicIngredient testIngredient = new AlchemicIngredient(
				State.LIQUID,new IngredientType("Testtype",State.LIQUID, 0,0),10,Quantity.BOTTLE);
		validLaboratory.addIngredient(new IngredientContainer(Quantity.BARREL, testIngredient));
		assertEquals(validLaboratory.getAmountUnit(), testIngredient.getUnit());
		
		validLaboratory.addIngredient(ingredientContainerTwo);
		//Since the value of the Quantity unit of testIngredient is lower than these of ingredientTwo.
		assertEquals(validLaboratory.getAmountUnit(), testIngredient.getUnit());
		
		
	}

	@Test
	void testCanHaveAsCapacity() {
		assertTrue(validLaboratory.canHaveAsCapacity(100));		
		assertFalse(validLaboratory.canHaveAsCapacity(0));
		assertFalse(validLaboratory.canHaveAsCapacity(-100));

	}

	@Test
	void testGetCapacity() {
		assertEquals(validLaboratory.getCapacity(), 1);
		validLaboratory = new Laboratory(500);
		assertEquals(validLaboratory.getCapacity(), 500);
	}


	@Test
	void testGetDevice() {
		assertNull(validLaboratory.getDevice(null));
		assertNull(validLaboratory.getDevice(Kettle.class));
		assertEquals(validLaboratory.getDevice(Oven.class), oven);
		assertEquals(validLaboratory.getDevice(CoolingBox.class), coolingBox);
		
		validLaboratory.removeDevice(oven);
		assertNull(validLaboratory.getDevice(Oven.class));
	}

	@Test
	void testAddDevice() {
		assertEquals(validLaboratory.getDevice(Oven.class), oven);
		validLaboratory.removeDevice(oven);
		assertNull(validLaboratory.getDevice(Oven.class));
		assertTrue(validLaboratory.hasProperDevices());
		validLaboratory.addDevice(oven);
		assertEquals(validLaboratory.getDevice(Oven.class), oven);
		try {
			validLaboratory.addDevice(oven);
			fail("Succesfully added the same device twice to the same Laboratory.");
		} catch (IllegalStateException e) {};
		
	}

	@Test
	void testRemoveDevice() {
		assertEquals(validLaboratory.getDevice(CoolingBox.class), coolingBox);
		validLaboratory.removeDevice(coolingBox);
		assertNull(validLaboratory.getDevice(CoolingBox.class));
		assertTrue(validLaboratory.hasProperDevices());
		try {
			validLaboratory.removeDevice(coolingBox);
			fail("Succesfully removed the same device twice from the same Laboratory.");
		} catch (IllegalArgumentException e) {}
	}

	@Test
	void testExecute() {
		assertTrue(validLaboratory.hasIngredient(ingredientOne.getSimpleName()));
		
		AlchemicIngredient ingredient2 = new AlchemicIngredient(State.LIQUID, new IngredientType("Ingredienttypetwo", State.LIQUID, 100, 100), 10, Quantity.SPOON, 0);
		
		RecipeBook book = new RecipeBook(5);
		Recipe recipe = new Recipe(book);
		//ADD-command
		recipe.addCommandoAt(ingredientOne, 1);
		//COOL-command
		recipe.addCommandoWithoutIngredientAt(Commando.COOL, 2);
		//ADD-command
		recipe.addCommandoAt(ingredient2, 3);
		//HEAT-command
		recipe.addCommandoWithoutIngredientAt(Commando.HEAT, 4);
		recipe.addCommandoWithoutIngredientAt(Commando.HEAT, 5);
		recipe.addCommandoWithoutIngredientAt(Commando.HEAT, 6);
		//MIX-command
		recipe.addCommandoWithoutIngredientAt(Commando.MIX, 7);
		
		//TEST 1: RECIPE SHOULD FAIL BECAUSE ingredient2 IS NOT A PART OF THE LABORATORY.
		validLaboratory.execute(recipe, 1);
		assertTrue(validLaboratory.hasIngredient(ingredientOne.getSimpleName()));
		AlchemicIngredient retrieveIngredient = validLaboratory.retrieveIngredient(ingredientOne.getSimpleName()).getIngredient();
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		assertEquals(retrieveIngredient.getAmount(), ingredientOne.getAmount());
		assertEquals(retrieveIngredient.getUnit(), ingredientOne.getUnit());
		
		validLaboratory.addIngredient(new IngredientContainer(ingredientOne.getState().getMaxQuantityToRetrieveFromLaboratory(), ingredientOne));
		validLaboratory.addIngredient(new IngredientContainer(ingredient2.getState().getMaxQuantityToRetrieveFromLaboratory(), ingredient2));
		assertTrue(validLaboratory.hasIngredient(ingredientOne.getSimpleName()));
		assertTrue(validLaboratory.hasIngredient(ingredient2.getSimpleName()));
		
		//TEST 2: ADDING ingredient2 BEFORE THE EXECUTION, BUT MIXING FAILED BECAUSE THE LABORATORY DOESN'T HAVE A KETTLE TO MIX WITH.
		validLaboratory.execute(recipe, 1);
		assertTrue(validLaboratory.hasIngredient(ingredientOne.getSimpleName()));
		assertTrue(validLaboratory.hasIngredient(ingredient2.getSimpleName()));
		retrieveIngredient = validLaboratory.retrieveIngredient(ingredientOne.getSimpleName()).getIngredient();
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		assertEquals(retrieveIngredient.getAmount(), ingredientOne.getAmount());
		assertEquals(retrieveIngredient.getUnit(), ingredientOne.getUnit());
		retrieveIngredient = validLaboratory.retrieveIngredient(ingredient2.getSimpleName()).getIngredient();
		assertEquals(retrieveIngredient.getTemperature(), ingredientOne.getTemperature());
		assertEquals(retrieveIngredient.getAmount(), ingredientOne.getAmount());
		assertEquals(retrieveIngredient.getUnit(), ingredientOne.getUnit());
		
		
		validLaboratory.addIngredient(new IngredientContainer(ingredientOne.getState().getMaxQuantityToRetrieveFromLaboratory(), ingredientOne));
		validLaboratory.addIngredient(new IngredientContainer(ingredient2.getState().getMaxQuantityToRetrieveFromLaboratory(), ingredient2));
		assertTrue(validLaboratory.hasIngredient(ingredientOne.getSimpleName()));
		assertTrue(validLaboratory.hasIngredient(ingredient2.getSimpleName()));
		
		Kettle kettle = new Kettle();
		validLaboratory.addDevice(kettle);
		
		//TEST 3: TEST 2 BUT NOW THE LABORATORY CONTAINS A KETTLE
		validLaboratory.execute(recipe, 1);
		assertFalse(validLaboratory.hasIngredient(ingredientOne.getSimpleName()));
		assertFalse(validLaboratory.hasIngredient(ingredient2.getSimpleName()));
		assertTrue(validLaboratory.hasIngredient("Ingredienttypeone mixed with Ingredienttypetwo"));
		assertFalse(validLaboratory.hasIngredient("Water"));
		retrieveIngredient = validLaboratory.retrieveIngredient("Ingredienttypeone mixed with Ingredienttypetwo").getIngredient();
		//Indicates that the mix succeeded; mix relative tests are part of the test classes of AlchemicMixture and Kettle.
		assertEquals(retrieveIngredient.getSimpleName(), "Ingredienttypeone mixed with Ingredienttypetwo");
	}

	@Test
	void testIterator() {
		AlchemicIngredient newIngredient = new AlchemicIngredient(State.LIQUID, new IngredientType(
				"Ingredienttypetwo", State.LIQUID, 50, 0), 5, Quantity.VIAL);
		IngredientContainer newContainer = new IngredientContainer(Quantity.BARREL, State.LIQUID);
		newContainer.setIngredient(newIngredient);
		validLaboratory.addIngredient(newContainer);
		
		LaboratoryIngredientIterator iterator = validLaboratory.iterator();
		assertEquals(iterator.getCurrent(), ingredientOne);
		assertEquals(iterator.getNbElements(), 1);
		iterator.advance();
		assertEquals(iterator.getCurrent(), newIngredient);
		assertEquals(iterator.getNbElements(), 0);
		iterator.advance();
		
		try {
			iterator.getCurrent();
			fail("This shouldn't happen. Not enough ingredients in Laboratory.");
		} catch (IndexOutOfBoundsException e) {}
		
		iterator.reset();
		assertEquals(iterator.getNbElements(), 1);
		assertEquals(iterator.getCurrent(), ingredientOne);
	}

}
