package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alchemie.AlchemicIngredient;
import alchemie.IngredientType;
import alchemie.MixtureIngredientType;
import alchemie.Quantity;
import alchemie.State;
import temperature.Temperature;

class AlchemicIngredientTest {

	@Test
	void testCanHaveAsState() {
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ; 
		assertTrue(ingredient.canHaveAsState(State.POWDER)) ; 
		assertTrue(ingredient.canHaveAsState(State.LIQUID)) ; 
		ingredient = new AlchemicIngredient(10, Quantity.DROP) ; 
		assertTrue(!ingredient.canHaveAsState(State.POWDER)) ; 
		assertTrue(ingredient.canHaveAsState(State.LIQUID)) ;
	}

	@Test
	void testCanHaveAsTemperature() {
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ; 
		assertTrue(ingredient.canHaveAsTemperature(new Temperature())) ;
		assertTrue(!ingredient.canHaveAsTemperature(null)) ;
	}

	@Test
	void testAlchemicIngredientStateIngredientTypeIntQuantity() {
		IngredientType type = new IngredientType() ; 
		AlchemicIngredient ingredient = new AlchemicIngredient(State.LIQUID, type, 10, Quantity.BOTTLE) ; 
		assertTrue(ingredient.getState() == State.LIQUID ) ; 
		assertTrue(ingredient.getType().equals(type)) ; 
		assertTrue(ingredient.getAmount() == 10 ) ; 
		assertTrue(ingredient.getUnit() == Quantity.BOTTLE) ; 
		assertTrue(type.getTemperature().equals(ingredient.getTemperature())) ; 
		ingredient = new AlchemicIngredient(State.LIQUID, null, 10, Quantity.BOTTLE) ;
		assertTrue(ingredient.getState() == State.LIQUID ) ; 
		assertTrue(ingredient.getType().equals(type)) ; 
		assertTrue(ingredient.getAmount() == 10 ) ; 
		assertTrue(ingredient.getUnit() == Quantity.BOTTLE) ; 
		assertTrue(type.getTemperature().equals(ingredient.getTemperature())) ; 
		try {
			ingredient = new AlchemicIngredient(State.LIQUID, null, 10, Quantity.CHEST) ;
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
	}

	@Test
	void testAlchemicIngredientStateIngredientTypeIntQuantityDouble() {
		IngredientType type = new IngredientType() ; 
		AlchemicIngredient ingredient = new AlchemicIngredient(State.LIQUID, type, 10, Quantity.SPOON, 0) ; 
		assertTrue(ingredient.getState() == State.LIQUID ) ; 
		assertTrue(ingredient.getType().equals(type)) ; 
		assertTrue(ingredient.getAmount() == 10 ) ; 
		assertTrue(ingredient.getUnit() ==  Quantity.SPOON) ; 
		assertTrue(type.getTemperature().equals(ingredient.getTemperature())) ; 
		assertTrue(ingredient.getCharacteristicalVolatility() == 0 ) ; 
		try {
			ingredient = new AlchemicIngredient(State.LIQUID, null, 10, Quantity.CHEST,0) ;
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
		
		try {
			ingredient = new AlchemicIngredient(State.LIQUID, null, 10, Quantity.CHEST,1) ;
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
	}

	@Test
	void testAlchemicIngredientIntQuantity() {
		IngredientType type = new IngredientType() ; 
		AlchemicIngredient ingredient = new AlchemicIngredient(10,Quantity.SPOON) ; 
		assertTrue(ingredient.getState() == State.LIQUID ) ; 
		assertTrue(ingredient.getType().equals(type)) ; 
		assertTrue(ingredient.getAmount() == 10 ) ; 
		assertTrue(ingredient.getUnit() ==  Quantity.SPOON) ; 
		assertTrue(type.getTemperature().equals(ingredient.getTemperature())) ; 
		try {
			ingredient = new AlchemicIngredient(10,Quantity.CHEST) ; 
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
	}

	@Test
	void testCompareTo() {
		IngredientType type1 = new IngredientType() ;
		IngredientType type2 = new IngredientType("Beer", State.LIQUID, 0, 0) ;
		AlchemicIngredient ingredient1 = new AlchemicIngredient(State.LIQUID, type1, 10, Quantity.BOTTLE) ; 
		AlchemicIngredient ingredient2 = new AlchemicIngredient(State.LIQUID, type2, 10, Quantity.BOTTLE) ; 
		assertTrue(ingredient1.compareTo(ingredient2) > 0) ; 
		assertTrue(ingredient1.compareTo(ingredient1) == 0) ;
		assertTrue(ingredient2.compareTo(ingredient1) < 0) ;
	}

	@Test
	void testGetFullName() {
		AlchemicIngredient ingredient1 = new AlchemicIngredient(10,Quantity.SPOON ) ;
		assertTrue(ingredient1.getFullName().equals(ingredient1.getSimpleName()));
	}

	@Test
	void testIsValidAmount() {
		assertTrue(AlchemicIngredient.isValidAmount(0) ) ; 
		assertTrue(AlchemicIngredient.isValidAmount(100000) ) ; 
		assertTrue(!AlchemicIngredient.isValidAmount(-1000) ) ; 
	}

	@Test
	void testIsPossibleUnit() {
		assertTrue(!AlchemicIngredient.isPossibleUnit(null) ) ;
		assertTrue(AlchemicIngredient.isPossibleUnit(Quantity.BARREL) ) ;
	}

	@Test
	void testMatchesUnitState() {
		assertTrue(!AlchemicIngredient.matchesUnitState(null, State.LIQUID) ); 
		assertTrue(!AlchemicIngredient.matchesUnitState(Quantity.BARREL, null)) ; 
		assertTrue(!AlchemicIngredient.matchesUnitState(Quantity.CHEST, State.LIQUID)) ; 
		assertTrue(AlchemicIngredient.matchesUnitState(Quantity.DROP, State.LIQUID)) ; 
	}

	@Test
	void testCanHaveAsUnit() {
		AlchemicIngredient ingredient1 = new AlchemicIngredient(10,Quantity.SPOON ) ;
		assertTrue(ingredient1.canHaveAsUnit(Quantity.JUG) ); 
		assertTrue(!ingredient1.canHaveAsUnit(null) ); 
		assertTrue(!ingredient1.canHaveAsUnit(Quantity.CHEST) ); 
	}

	@Test
	void testCanHaveAsPossibleType() {
		AlchemicIngredient ingredient1 = new AlchemicIngredient(10,Quantity.SPOON ) ;
		IngredientType type1 = new IngredientType() ;
		MixtureIngredientType type2 = new MixtureIngredientType("Beer mixed with Cola", State.LIQUID, 0, 0) ; 
		assertTrue(ingredient1.canHaveAsPossibleType(type1)); 
		assertTrue(!ingredient1.canHaveAsPossibleType(null));
		assertTrue(!ingredient1.canHaveAsPossibleType(type2));
	}

	@Test
	void testMatchesTypeCharacteristicalVolatility() {
		IngredientType type1 = new IngredientType() ;
		assertTrue(AlchemicIngredient.matchesTypeCharacteristicalVolatility(type1, 0)) ; 
		assertTrue(!AlchemicIngredient.matchesTypeCharacteristicalVolatility(type1, 1)) ; 
		type1 = new IngredientType("Water",  State.LIQUID, 0, 1) ; 
		assertTrue(!AlchemicIngredient.matchesTypeCharacteristicalVolatility(type1, 0)) ; 
		assertTrue(AlchemicIngredient.matchesTypeCharacteristicalVolatility(type1, 1)) ;
		assertTrue(AlchemicIngredient.matchesTypeCharacteristicalVolatility(type1, 0.9)) ;
		assertTrue(AlchemicIngredient.matchesTypeCharacteristicalVolatility(type1, 1.1)) ;
		
	}

	@Test
	void testCanHaveAsType() {
		AlchemicIngredient ingredient1 = new AlchemicIngredient(10,Quantity.SPOON ) ;
		assertTrue(ingredient1.canHaveAsType(new IngredientType()) ); 
		assertTrue(!ingredient1.canHaveAsType(null) ); 
		assertTrue(!ingredient1.canHaveAsType(new IngredientType("Water",  State.LIQUID, 0, 1)) ); 
	}


	@Test
	void testCanHaveAsCharacteristicalVolatility() {
		IngredientType type1 = new IngredientType() ;
		IngredientType type2 = new IngredientType("Water",  State.LIQUID, 0, 1) ; 
		AlchemicIngredient ingredient1 = new AlchemicIngredient(State.LIQUID, type1,10,Quantity.SPOON ) ; 
		AlchemicIngredient ingredient2 = new AlchemicIngredient(State.LIQUID, type2,10,Quantity.SPOON ) ; 
		assertTrue(ingredient1.canHaveAsCharacteristicalVolatility(0) ) ; 
		assertTrue(!ingredient1.canHaveAsCharacteristicalVolatility(1) ) ;
		assertTrue(!ingredient2.canHaveAsCharacteristicalVolatility(0) ) ; 
		assertTrue(ingredient2.canHaveAsCharacteristicalVolatility(1) ) ;
		assertTrue(!ingredient2.canHaveAsCharacteristicalVolatility(1.1) ) ; 
		assertTrue(ingredient2.canHaveAsCharacteristicalVolatility(0.99) ) ; 
		assertTrue(!ingredient2.canHaveAsCharacteristicalVolatility(-1.1) ) ; 
	}

	@Test
	void testIsHotterThan() {
		AlchemicIngredient ingredient1 = new  AlchemicIngredient(10,Quantity.SPOON) ; 
		assertTrue(!ingredient1.isHotterThan(null)) ; 
		assertTrue(ingredient1.isHotterThan(new Temperature(0,0))) ; 
		assertTrue(!ingredient1.isHotterThan(new Temperature(0,20))) ; 
		assertTrue(!ingredient1.isHotterThan(new Temperature(0,40))) ; 
	}

	@Test
	void testIsColderThan() {
		AlchemicIngredient ingredient1 = new  AlchemicIngredient(10,Quantity.SPOON) ; 
		assertTrue(!ingredient1.isColderThan(null)) ; 
		assertTrue(!ingredient1.isColderThan(new Temperature(0,0))) ; 
		assertTrue(!ingredient1.isColderThan(new Temperature(0,20))) ; 
		assertTrue(ingredient1.isColderThan(new Temperature(0,40))) ;
	}

}
