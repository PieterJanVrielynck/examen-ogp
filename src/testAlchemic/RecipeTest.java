package testAlchemic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import alchemie.AlchemicIngredient;
import alchemie.Quantity;
import recipe.Commando;
import recipe.Recipe;
import recipe.RecipeBook;

class RecipeTest {

	@Test
	void testRecipe() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		assertTrue(book.containsRecipe(recipe1)) ; 
		assertTrue(recipe1.getRecipeBook() == book) ; 
	}

	@Test
	void testCanHaveAsBook() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		RecipeBook book2 = new RecipeBook(1);
		@SuppressWarnings("unused")
		Recipe recipe2 = new Recipe(book2) ;
		assertTrue(recipe1.canHaveAsBook(book)) ;
		assertTrue(!recipe1.canHaveAsBook(null)) ; 
		assertTrue(!recipe1.canHaveAsBook(book2)) ;
	}

	@Test
	void testSetRecipeBook() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		RecipeBook book2 = new RecipeBook(1);
		@SuppressWarnings("unused")
		Recipe recipe2 = new Recipe(book2) ;
		RecipeBook book3 = new RecipeBook(1);
		try {
			recipe1.setRecipeBook(book);
			assertTrue(false); 
		}
		catch(IllegalArgumentException exception ) {
			assertTrue(true); 
		}
		try {
			recipe1.setRecipeBook(book2);
			assertTrue(false); 
		}
		catch(IllegalArgumentException exception ) {
			assertTrue(true); 
		}
		recipe1.setRecipeBook(book3);
		assertTrue(recipe1.getRecipeBook() == book3) ; 
		assertTrue(book3.containsRecipe(recipe1)) ;
		assertTrue(!book.containsRecipe(recipe1)) ;
	}

	@Test
	void testCanHaveAsPossibleInstructionAt() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ; 
		assertTrue(recipe1.canHaveAsPossibleInstructionAt(1,Commando.ADD)) ; 
		assertTrue(!recipe1.canHaveAsPossibleInstructionAt(2,Commando.ADD)) ;
		assertTrue(!recipe1.canHaveAsPossibleInstructionAt(1,null)) ;
		assertTrue(!recipe1.canHaveAsPossibleInstructionAt(1,Commando.MIX)) ;
		assertTrue(!recipe1.canHaveAsPossibleInstructionAt(1,Commando.COOL)) ;
		recipe1.addCommandoAt(ingredient, 1);
		assertTrue(!recipe1.canHaveAsPossibleInstructionAt(2,Commando.MIX)) ;
		assertTrue(recipe1.canHaveAsPossibleInstructionAt(2,Commando.COOL)) ;
		recipe1.addCommandoWithoutIngredientAt(Commando.COOL, 2);
		assertTrue(!recipe1.canHaveAsPossibleInstructionAt(3,Commando.MIX)) ;
		recipe1.addCommandoAt(ingredient, 3);
		assertTrue(recipe1.canHaveAsPossibleInstructionAt(4,Commando.MIX)) ;
	}

	@Test
	void testCanHaveAsIndex() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ;
		assertTrue(recipe1.canHaveAsIndex(1)) ; 
		assertTrue(!recipe1.canHaveAsIndex(-1)) ;
		assertTrue(!recipe1.canHaveAsIndex(2)) ;
		recipe1.addCommandoAt(ingredient, 1);
		assertTrue(!recipe1.canHaveAsIndex(3)) ;
		assertTrue(recipe1.canHaveAsIndex(2)) ;
	}

	@Test
	void testAddCommandoWithoutIngredientAt() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ;
		recipe1.addCommandoAt(ingredient, 1);
		recipe1.addCommandoAt(ingredient, 2);
		recipe1.addCommandoWithoutIngredientAt(Commando.COOL, 2);
		assertTrue(recipe1.getInstructionAt(2) == Commando.COOL ) ; 
		assertTrue(recipe1.getNumberOfInstructions() == 3) ; 
		try {
			recipe1.addCommandoWithoutIngredientAt(Commando.COOL, 1);
			assertTrue(false) ;
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ; 
		}
	}

	@Test
	void testIsPossibleIngredient() {
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ;
		assertTrue(!Recipe.isPossibleIngredient(null)) ; 
		assertTrue(Recipe.isPossibleIngredient(ingredient)) ; 
	}

	@Test
	void testCanHaveAsNumberOfIngredients() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ;
		recipe1.addCommandoAt(ingredient, 1);
		recipe1.addCommandoAt(ingredient, 2);
		assertTrue(recipe1.canHaveAsNumberOfIngredients(2)) ; 
		assertTrue(!recipe1.canHaveAsNumberOfIngredients(3)) ;
		assertTrue(!recipe1.canHaveAsNumberOfIngredients(-3)) ;
	}

	@Test
	void testAddCommandoAt() {
		RecipeBook book = new RecipeBook(1);
		Recipe recipe1 = new Recipe(book) ;
		AlchemicIngredient ingredient = new AlchemicIngredient(10, Quantity.SPOON) ;
		AlchemicIngredient ingredient2 = new AlchemicIngredient(10, Quantity.SPOON) ;
		recipe1.addCommandoAt(ingredient, 1);
		assertTrue(recipe1.getInstructionAt(1) == Commando.ADD) ; 
		assertTrue(recipe1.getIngredientAt(1) == ingredient ) ; 
		recipe1.addCommandoAt(ingredient2, 1);
		assertTrue(recipe1.getInstructionAt(1) == Commando.ADD) ; 
		assertTrue(recipe1.getIngredientAt(1) == ingredient2 ) ; 
		assertTrue(recipe1.getNumberOfIngredients() == 2) ; 
		assertTrue(recipe1.getNumberOfInstructions() == 2) ;
		try {
			recipe1.addCommandoAt(ingredient2, -400);
			assertTrue(false) ; 
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ;
		}
		try {
			recipe1.addCommandoAt(null, 3);
			assertTrue(false) ; 
		}
		catch(IllegalArgumentException exception) {
			assertTrue(true) ;
		}
	}

}
