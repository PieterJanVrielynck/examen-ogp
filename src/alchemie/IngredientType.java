package alchemie;
import be.kuleuven.cs.som.annotate.*;
import temperature.Temperature;

/**
 * A value class of ingredient types 
 * @invar Each ingredient type should have a valid theoretical  volatility
 *        | isValidTheoriticalVolatility(getTheoreticalVolatility())
 * @invar each ingredient type should have a properly spelled  name
 *        | canHaveAsName(getName() )        
 *        
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 *
 */
@Value 
public class IngredientType extends GeneralIngredient {
	
	/**********************************************************
	 * Constructors 
	 **********************************************************/
	/**
	 * A constructor to initialize a new  ingredient type 
	 * @param name
	 *        the name of the type
	 * @param state
	 *        the state of the ingredient type
	 * @param cold
	 *        the cold temperature of the ingredient type 
	 * @param heat
	 *        the heat temperature of the ingredient type 
	 * @param theoreticalVolatility
	 *        the theoretical volatility 
	 * @throws IllegalArgumentException
	 *         if the name is not valid 
	 *         | ! isVqlidName(name) 
	 * @post the name of the ingredient type is set to the given name 
	 *       | new.getName() == name 
	 * @post if the given theoretical volatility is a valid theoretical volatility 
	 *       then the theoretical volatility will be set to the given number
	 *       else the theoretical volatility will be set to zero
	 *       | if isValidTheoriticalVolatility(theoreticalVolatility) 
	 *       | then new.getTheoreticalVolatility() ==  theoreticalVolatility
	 *       | else new.getTheoreticalVolatility() ==  0 
	 * @effect the values of the temperature and state are initialized with a auxiliary method
	 *         | super.auxiliaryGeneralIngredient(state, cold, heat)
	 */
	public IngredientType(String name, State state, long heat, double theoreticalVolatility ) throws IllegalArgumentException {
		if (! canHaveAsName(name)) throw new IllegalArgumentException("not a valid name") ; 
		super.auxiliaryGeneralIngredient(state, heat);
		this.name = name ;  
		if (isValidTheoriticalVolatility(theoreticalVolatility)) {
			this.theoreticalVolatility = theoreticalVolatility ; 
		}
		else {
			this.theoreticalVolatility = 0 ; 
		}
		
	}
	/**
	 * A constructor to initialize a new  ingredient type 
	 * @effect the ingredient type is initialized with "water" as name, a liquid state,
	 *         zero as its cold temperature and 20 as its heat temperature and zero as its theoretical volatility
	 *         | this("Water", State.LIQUID, 20, 0) 
	 */
	public IngredientType() {
		this("Water", State.LIQUID, 20, 0) ; 
	}

	
	/**********************************************************
	 * Name
	 **********************************************************/
	
	/**
	 * Variable referencing the name of an ingredient type
	 */
	private final String name  ; 
	
	/**
	 * A basic inspector to return the name of the ingredient type
	 */
	@Basic @Immutable 
	public String getName() {
		return this.name ; 
	}
	
	/**
	 * An inspector to check whether a given possibleName is a proper name 
	 * 
	 * @return false if the possbleName is not effective 
	 *         | result == false if possibleName == null 
	 */
	@Override 
	public boolean canHaveAsName(String possibleName) {
		return super.canHaveAsName(possibleName) ; 
	}

  
    
    
	/**********************************************************
	 * temperature
	 **********************************************************/
	
	/**
	 * An inspector to check whether a given temperature is a proper temperature for this ingredient type
	 * 
	 * @return true if the given temperature is effective and the cold temperature equals zero 
	 *         false otherwise 
	 *         | result == ( temperature != null && (temperature.getTemperatureCold() == 0) ) 
	 */
	@Override
	public boolean canHaveAsTemperature(Temperature temperature) {
		return super.canHaveAsTemperature(temperature) && (temperature.getTemperatureCold() == 0) ; 
	}

	
	/**********************************************************
	 * theoretical volatility
	 **********************************************************/
	
	/**
	 * A variable referencing the theoretical Volatility  of an ingredientType
	 */
	private final double theoreticalVolatility ; 
	
	/**
	 * A basic inspector to return the theoretical volatility of an ingredient type
	 */
	@Basic @Immutable
	public double getTheoreticalVolatility() {
		return this.theoreticalVolatility ; 
	}
	
	/**
	 * A checker to verify if a given volatility is  a valid volatility for a ingredient type
	 * @param volatility
	 *        the volatility to check
	 * @return true if the volatility is between zero and one
	 *         false otherwise
	 *         | result == (0 <= volatility && volatility <= 1 )
	 */
	public static boolean isValidTheoriticalVolatility(double volatility) {
		return (0 <= volatility && volatility <= 1 ) ; 
	}
	

	
	/**********************************************************
	 * Value class methods
	 **********************************************************/
	
	/**
	 * A inspector to check whether a given object equals this ingredient type 
	 * 
	 * @return true if the type is effective,  an instance of IngredientType and has the same name, temperature and theoretical temperature
	 *         false otherwise 
	 *         | if ( type != null && (type instanceof IngredientType) && Type.getName().trim().equals(this.getName().trim()) 
	 *         |      && Type.getTemperature().equals(getTemperature())) && Type.getTheoreticalVolatility() == this.getTheoreticalVolatility()) ) 
	 *         | then result == true 
	 *         | else result == false 
	 */
    @Override
	public boolean equals(Object type) {
    	if (type == null || !(type instanceof IngredientType)) return false ; 
		boolean result ; 
		IngredientType inType = (IngredientType) type ; 
		result = inType.getName().trim().equals(this.getName().trim()) ;
		result = result && (inType.getTemperature().equals(getTemperature())) ; // type: temperature cold always equals zero
		result = result && (inType.getTheoreticalVolatility() == this.getTheoreticalVolatility()) ; 
		return result ; 
	}
	
    /**
     * An inspector to query the hashcode 
     * 
     * @return the hashcode of the trimmed name
     *         | result == getName().trim().hashCode()
     */
    @Override 
	public int hashCode() {
		return getName().trim().hashCode() ; 
	}
	
	
	/**********************************************************
	 * State
	 **********************************************************/
	
    /**
     * An inspector to check whether a given state is a proper state for this ingredient type 
     * 
     * @return true if the state is effective 
     *         false otherwise 
     *         | result == (state != null ) 
     */
	@Override 
	public boolean canHaveAsState(State state) {
		return super.canHaveAsState(state) ; 
	}
	
}
