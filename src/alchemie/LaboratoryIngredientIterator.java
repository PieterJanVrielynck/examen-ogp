package alchemie;

import be.kuleuven.cs.som.annotate.Basic;

/**
 * Interface for an iterator to iterate the ingredients in a laboratory.
 * @author	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
public interface LaboratoryIngredientIterator{
	
	/**
	 * Method to return the current AlchemicIngredient of
	 * the LaboratoryIngredientIterator iterator.
	 * @return	The current element.
	 * @throws	IndexOutOfBoundsException if there is
	 * 			no element left to return.
	 * 			| getNbElements() == 0
	 */
	public AlchemicIngredient getCurrent() throws IndexOutOfBoundsException;
	
	/**
	 * Method to return the amount of elements left for this iterator.
	 * @return Integer indicating the elements left for the iterator.
	 */
	@Basic
	public int getNbElements();
	
	
	/**
	 * Method to advance the iterator.
	 * @post	The iterator has been advanced.
	 * 			| if (old.getNbElements() != 0)
	 * 			| 	then old.getNbElements() == new.getNbElements() + 1
	 * 			| else new.getNbElements() == 0
	 */
	public void advance();
	
	
	/**
	 * Method to reset the pointer of the iterator.
	 * @post	The iterator has been reset.
	 * 			| old.getNbElements() <= new.getNbElements()
	 */
	public void reset();
}
