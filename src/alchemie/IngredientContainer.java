package alchemie;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class to represent an ingredient container.
 * @invar	The capacity of the IngredientContainer
 * 			needs to be a valid capacity.
 * 			| isProperCapacity(getCapacity(), getState())
 * @invar	The IngredientContainer is empty or has a valid ingredient.
 * 			| isEmpty() || canHaveAsIngredient(getIngredient())
 * @invar	The IngredientContainer must have an effective State.
 * 			| hasProperState()
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version	1.0
 */
public class IngredientContainer implements Comparable<IngredientContainer>{

	
	/**
	 * Constructor for the IngredientContainer with a given capacity and state. 
	 * @param	capacity
	 * 			The capacity of the container.
	 * @param	The state of the proper ingredients for the
	 * 			IngredientContainer.
	 * @post	The capacity of the new IngredientContainer has been
	 * 			set to the given capacity.
	 * 			| new.getCapacity() == capacity
	 * @post	The State of the new IngredientContainer has been set
	 * 			to the given State.
	 * 			| new.getState() == state
	 * @throws	IllegalArgumentException if the given combination of capacity
	 * 			and state is not allowed.
	 * 			| !isProperCapacity(capacity, state)
	 */
	@Raw
	public IngredientContainer(Quantity capacity, State state) throws IllegalArgumentException{
		if (!isProperCapacity(capacity, state)) {
			throw new IllegalArgumentException("The given capacity is not a proper capacity"
					+ " for a IngredientContainer.");
		} else {
			this.state = state;
			this.CAPACITY = capacity;
		}
	}
	
	
	
	
	/**
	 * Constructor for the IngredientContainer with a given capacity,
	 * ingredient and an amount.
	 * @param	capacity
	 * 			The capacity of the container.
	 * @param 	ingredient
	 * 			The ingredient in the container.
	 * @post	The IngredientContainer with the given capacity and state has been initialized.
	 * 			| new.getState() == ingredient.getState() && new.getCapacity() == capacity
	 * @effect	The ingredient of the IngredientContainer has been set to the given ingredient.
	 * 			| setIngredient(ingredient)
	 * @throws	IllegalArgumentException if the given ingredient is not effective or if
	 * 			the given capacity is not a proper capacity in combination with the state of the ingredient.
	 * 			| ingredient == null || !isProperCapacity(capacity, ingredient.getState())
	 */
	@Raw
	public IngredientContainer (Quantity capacity, AlchemicIngredient ingredient) {
		if (ingredient == null) {
			throw new IllegalArgumentException("The given ingredient is not effective.");
		} else if (!isProperCapacity(capacity, ingredient.getState())) {
			throw new IllegalArgumentException("The given capacity is not a proper capacity "
					+ "for a IngredientContainer in combination with the state of the given ingredient.");
		} else {
			this.state = ingredient.getState();
			this.CAPACITY = capacity;
			setIngredient(ingredient);
		}
		
	}
	
	
	
	@Override
	public int compareTo(IngredientContainer ingredientContainer) {
		//Used for the creation of the name of the result of a Kettle.
		return getIngredient().getSimpleName().trim().compareTo(ingredientContainer.getIngredient().getSimpleName());
	}
	
	
	
	/**
	 * Method to terminate an IngredientContainer.
	 * @post	The IngredientContainer is terminated.
	 * 			| new.getIngredient() == null && new.isTerminated() == true
	 */
	public void terminate() {
		this.ingredient = null;
		this.terminated = true;
	}
	
	
	/**
	 * Inspector to check if a IngredientContainer is terminated.
	 * @return	True if the IngredientContainer is terminated and has no ingredients; false otherwise.
	 * 			| if (this.terminated == true && getIngredient() == null)
	 * 			|	then result == true
	 * 			|else result == false
	 */
	public boolean isTerminated() {
		return this.terminated == true && getIngredient() == null;
	}
	
	
	
	/**
	 * Boolean indicating if an IngredientContainer is terminated.
	 */
	private boolean terminated = false;
	
	
	
	
	
	/**
	 * Basic inspector to return the capacity of an IngredientContainer, 
	 * measured in the SPOON quantity.
	 * @return	The capacity.
	 */
	@Basic @Immutable
	public Quantity getCapacity() {
		return this.CAPACITY;
	}
	
	/**
	 * An inspector to check if a IngredientContainer can have a given
	 * capacity.
	 * @param 	capacity
	 * 			The given capacity.
	 * @param 	state
	 * 			The state of the (possible) ingredient.
	 * @return	True if and only if the capacity is effective, is compatible with the given state
	 * 			 and doesn't represent one unit
	 * 			of the smallest or biggest quantity for the given state.
	 * 			| if ((capacity == null || state == null) || (!capacity.hasAsPossibleState(state)) ||
	 * 			|		(state.getMaxQuantity().equals(capacity) || state.getMinQuantity().equals(capacity))
	 * 			|	then result == false
	 * 			| else result == true
	 */
	@Raw
	public static boolean isProperCapacity(Quantity capacity, State state) {
		if ((capacity == null || state == null) || 
				(!capacity.hasAsPossibleState(state)) ||
				(state.getMaxQuantity() == capacity || state.getMinQuantity() == capacity)){
			return false;
		} else {
			return true;
		}
	}
	

	
	
	/**
	 * Final double indicating the capacity of a ingredient container, measured in the SPOON quantity unit.
	 * @invar	The IngredientContainer must have a proper Quantity capacity.
	 * 			| IsProperCapacity(getCapacity(), getState())
	 */
	final private Quantity CAPACITY;
	

	
	
	/**
	 * An inspector to return the ingredient in an IngredientContainer.
	 * @return	The AlchemicIngredient in the IngredientContainer, or 'null' if the 
	 * IngredientContainer doesn't already have an ingredient.
	 */
	@Basic
	public AlchemicIngredient getIngredient() {
		return this.ingredient;
	}
	
	
	/**
	 * Inspector to check if an IngredientContainer can have a given ingredient as a proper ingredient.
	 * @param	ingredient
	 * 			The ingredient to check.
	 * @return	True if the IngredientContainer is not terminated, the IngredientContainer doesn't already
	 * 			contain an AlchemicIngredient, the given AlchemicIngredient is effective, the State of the
	 * 			given AlchemicIngredient matches the State of the IngredientContainer and if
	 * 			the amount of the AlchemicIngredient is a proper amount for the IngredientContainer.
	 * 			| if (!isTerminated()) && (isEmpty()) && (ingredient != null)
	 * 			|		&& (ingredient.getState().equals(getState())) &&  (canHaveAsAmount(ingredient))
	 * 			| 	then result == true
	 * 			| else result == false
	 */
	@Raw
	public boolean canHaveAsIngredient(AlchemicIngredient ingredient) {
		return !isTerminated() && isEmpty() && ingredient != null &&
				ingredient.getState().equals(getState()) && canHaveAsAmount(ingredient);
	}
	
	
	/**
	 * Method to check if an IngredientContainer is empty.
	 * @return	True if the ingredientContainer doesn't contain an AlchemicIngredient;
	 * 			false otherwise.
	 * 			| if (getIngredient() == null)
	 * 			|	result == true
	 * 			| else result == false
	 */
	public boolean isEmpty() {
		return getIngredient() == null;
	}
	
	/**
	 * Mutator to set the ingredient of an IngredientContainer.
	 * @param	ingredient
	 * 			The ingredient for the IngredientContainer.
	 * @effect	The ingredient of the IngredientContainer has been set to the given ingredient parameter.
	 * 			| setIngredient(ingredient)
	 * @throws	IllegalArgumentException if the ingredient is not a proper ingredient
	 * 			for the IngredientContainer.
	 * 			| !canHaveAsIngredient(ingredient)
	 */
	@Raw
	public void setIngredient(AlchemicIngredient ingredient) {
		if (!canHaveAsIngredient(ingredient)) {
			throw new IllegalArgumentException("The given AlchemicIngredient is not a "
					+ "proper AlchemicIngredient for the IngredientContainer.");
		}else {
			this.ingredient = ingredient;
		}
	}
	
	
	
	/**
	 * Boolean to check if the amount of an AlchemicIngredient is a proper
	 * amount for the IngredientContainer.
	 * @param	ingredient
	 * 			The AlchemicIngredient from which we want to check the amount.
	 * @return	True if the ingredient is effective, it's amount is positive and the amount
	 * 			| doesn't exceed the capacity of the IngredientContainer.
	 * 			| if (ingredient != null && ingredient.getAmount() * ingredient.getUnit().convertTo(Quantity.SPOON) <= 
	 * 			|		getCapacity().getValue())
	 * 			|	then result == true
	 * 			| else result == false
	 */
	@Raw @Model
	private boolean canHaveAsAmount(AlchemicIngredient ingredient) {
		if (ingredient == null) {
			return false;
		} else {
			double amount = (ingredient.getAmount() * ingredient.getUnit().convertTo(Quantity.SPOON));
			return  amount <= getCapacity().getValue();
		}
	}
	

	
	
	/**
	* The AlchemicIngredient in the IngredientContainer.
	* @invar	An IngredientContainer must always have a proper (or none) AlchemicIngredient.
	* 			| isEmpty() || canHaveAsIngredient(getIngredient())
	*/
	private AlchemicIngredient ingredient = null;
		
		
	
	
	/**
	 * Method to check if an IngredientContainer has a proper State.
	 * @return	True if and only if the State of the Device is effective; false otherwise.
	 * 			| if (getState() != null)
	 * 			|	result == true
	 * 			| else result == false
	 */
	public boolean hasProperState() {
		return getState() != null;
	}
	
	/**
	 * Inspector to return the state of an IngredientContainer.
	 * @return	The State of the IngredientContainer.
	 */
	@Basic @Immutable
	public State getState() {
		return this.state;
	}
	
	
	/**
	 * The State of the IngredientContainer.
	 * @invar	The State of the IngredientContainer must be effective.
	 * 			| state != null
	 */
	private final State state;
		
		
		

}
