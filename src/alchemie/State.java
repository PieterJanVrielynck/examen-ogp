package alchemie;
import be.kuleuven.cs.som.annotate.*;


/**
 * Enumerator for the State's of an ingredient.
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
@Value 
public enum State {
	
	
	POWDER(), LIQUID();
	
	
	
	/**
	 * Method to return the minimum Quantity possible for the prime State.
	 * @return	The minimum Quantity possible for the prime State.
	 * 			| for each Quantity quantity in Quantity.values()
	 * 			| 	if (quantity.hasAsPossibleState(this) and quantity.getValue() <= result.getValue())
	 * 			|		then quantity == result
	 */
	@Immutable
	public Quantity getMinQuantity() {
		Quantity[] quantities = Quantity.values();
		Quantity returnQuantity = null;
		for (Quantity quantityLoop:quantities) {
			if (quantityLoop.hasAsPossibleState(this)) {
				if (returnQuantity == null || quantityLoop.convertTo(returnQuantity) < 1) {
					returnQuantity = quantityLoop;
				}
			}
		}
		return returnQuantity;
	}
	
	
	/**
	 * Method to return the maximum Quantity possible for the prime State.
	 * @return	The maximum Quantity possible for the prime State.
	 * 			| for each Quantity quantity in Quantity.values()
	 * 			| 	if (quantity.hasAsPossibleState(this) and quantity.getValue() >= result.getValue())
	 * 			|		then quantity == result
	 */
	@Immutable
	public Quantity getMaxQuantity() {
		Quantity[] quantities = Quantity.values();
		Quantity returnQuantity = null;
		for (Quantity quantityLoop:quantities) {
			if (quantityLoop.hasAsPossibleState(this)) {
				if (returnQuantity == null || quantityLoop.convertTo(returnQuantity) > 1) {
					returnQuantity = quantityLoop;
				}
			}
		}
		return returnQuantity;
	}
	
	/**
	 * Method to return the maximum capacity of an IngredientContainer to retrieve
	 * from the Laboratory, according to the prime State.
	 * @return	If this State is a POWDER, the Quantity CHEST will be given;
	 * 			if this State is a LIQUID, the Quantity BARREL will be given;
	 * 			else a null will be given.
	 * 			| if (this == POWDER) then result == Quantity.CHEST
	 * 			| else if (this == LIQUID) then result == Quantity.BARREL
	 * 			| else result == null
	 */
	@Immutable
	public Quantity getMaxQuantityToRetrieveFromLaboratory() {
		if (this == POWDER) {
			return Quantity.CHEST;
		} else if (this == LIQUID) {
			return Quantity.BARREL;
		} else {
			return null;
		}
	}
	
	

}
