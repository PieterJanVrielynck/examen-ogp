package alchemie;
import be.kuleuven.cs.som.annotate.*;
import java.util.*; 

/**
 * A class of alchemic mixtures. 
 * 
 * @invar Each alchemic mixture has either no special name or can have its special name as its name 
 *        | canHaveAsSpecialName(getSpecialName() ) || ! hasSpecialName() 
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 *
 */
public class AlchemicMixture extends AlchemicIngredient {
	
	
	/**
	 * A constructor to create a new alchemic mixture
	 * @param state
	 *        the state of the mixture
	 * @param type
	 *        the type of the mixture
	 * @param amount
	 *        the amount 
	 * @param unit
	 *        the unit of the amount 
	 * @param charVolatility
	 *        the characteristical volatility 
	 * @effect This mixture alchemic ingredient is constructed as a alchemic ingredient with given properties 
	 *         | super(state, type, amount, unit, charVolatility)
	 */
	public AlchemicMixture(State state, IngredientType type, int amount, Quantity unit, double charVolatility) {
		super(state, type, amount, unit, charVolatility);
	}

	/**********************************************************
	 * full name 
	 **********************************************************/
	
    /**
     *  An inspector to return the full name of an alchemic mixture
     *  
     *  @result if this mixture has a special name 
     *          then result equals the first part of a full name plus the special name plus between brackets the simple name plus the end part of a full name
     *          else result equals the first part of a full nale plus the simple name plus the end part of a full name 
     *          |  if ( hasSpecialName())
     *          | then result == makeFirstPartFullName() + " " +  getSpecialName() + "(" + getSimpleName() + ")"+ " " + makeEndPartFullName()
     *          | else result ==  makeFirstPartFullName() + " " + getSimpleName() + " " + makeEndPartFullName()
     */
	@Override
	public String getFullName() {
		String result = "" ; 
		result += makeFirstPartFullName() ; 
		result += " "; 
		if ( hasSpecialName()) {
			result += this.specialName ; 
			result = result + "(" + getSimpleName() + ")" ; 
		}
		else {
			result += getSimpleName() ; 
		}
		result += " " ; 
		result += makeEndPartFullName() ; 
		return result ; 
	}
	
	/**********************************************************
	 * names
	 **********************************************************/

	
	/**
	 * A basic inspector to return the special name of an miture ingredient
	 */
	@Basic
    public String getSpecialName()  { 
    	return this.specialName ; 
     }
	/**
	 * An inspector to verify if a given special name can be a special name 
	 * @return True if this alchemic mixture can have this special name as a name
	 *         false otherwise
	 *         | result == canHaveAsName(specialName)
	 */
	@Raw
	public boolean canHaveAsSpecialName(String specialName) {
		return super.canHaveAsName(specialName) ; 
	}
	
	/**
	 * An inspector to check whether a alchemic mixture has a special name
	 * @return true if the special name is effective 
	 *         false otherwise 
	 *         | result == ( cgetSpecialName()  != null ) 
	 */
	public boolean hasSpecialName() {
		return this.specialName != null ; 
	}
	
	/**
	 * A mutator to set the special name to a given special name
	 * @param specialName
	 *        the new special name
	 * @post The special name is set to the given name
	 *       |new.getSpecialName() == specialName
	 * @throws IllegalArgumentException
	 *         if the given special name is not a proper special name 
	 *         | if (! canHaveAsSpecialName(specialName)) 
	 */
	public void setSpecialName(String specialName) throws IllegalArgumentException{
		if (! canHaveAsSpecialName(specialName)) throw new IllegalArgumentException() ; 
		if (getLaboratory() != null && getLaboratory().hasIngredient(getSimpleName())) { 
			getLaboratory().removeIngredient(getSimpleName());
			this.specialName = specialName ;
			getLaboratory().addNewIngredient(this);
		}
		
	}
	
	/**
	 * A variable referencing the special name of a alchemic mixture 
	 */
	private String specialName = null;
	
	/**
	 * A inspector to query the pieces of the simple name of this mixture ingredient
	 * @return The simple name of this mixture contains every part that is returned and every part is a proper name 
	 *         | for part in result:
	 *         |      getSimpleName().contains(part) && canHaveAsName(part) 
	 */
	public ArrayList<String> getPartName() {
		ArrayList<String> result = new ArrayList<>() ;
		String name = getSimpleName().trim() ; 
		String[] list = name.split("mixed with") ; 
		result.add(list[0].trim()) ; 
		list = list[1].split("and") ; 
		for (String part: list[0].split(",") ) {
			result.add(part.trim() ) ; 
		}
		result.add(list[1].trim()) ;
		return result ; 
	}
	/**********************************************************
	 * IngredientType (total) 
	 **********************************************************/
	/**
	 * An inspector to check whether a given type is a proper type
	 * @param type
	 *        the type to check
	 *        
	 * @result true if the type is effective and a Mixture ingredient type 
	 *         false otherwise 
	 *         | result == (type instanceof MixtureIngredientType)
	 */
	@Override
	public boolean canHaveAsPossibleType(IngredientType type) {
		return  type instanceof MixtureIngredientType ; 
		
	}
	
	/**
	 * A inspector to qeury a default type
	 * @return the result is a mixture ingredient type with name "Cola mixed with Water", a liquid state, a temperature with a heat temperature of 20 
	 *         and a theoretical volatility of zero
	 *         | result == MixtureIngredientType("Cola mixed with Water", State.LIQUID,20,0 )
	 */
	@Override @Model @Immutable 
	protected IngredientType getDefaultType() {
		return new MixtureIngredientType("Cola mixed with Water", State.LIQUID,20,0 ) ; 
	}
	
	


}
