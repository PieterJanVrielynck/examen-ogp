package alchemie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import recipe.Commando;
import recipe.Recipe;
import temperature.Temperature;

/**
 * Class representing a Laboratory.
 * @invar	A Laboratory must have a proper capacity.
 * 			| canHaveAsCapacity(getCapacity())
 * @invar	A Laboratory must have a proper amount.
 * 			| canHaveAsAmount(getAmount())
 * @invar	A Laboratory must have a proper amountUnit.
 * 			| canHaveAsAmountUnit(getAmountUnit())
 * @invar	A Laboratory must have proper devices.
 * 			| hasProperDevices()
 * @invar	A Laboratory must have proper ingredients.
 * 			| hasProperIngredients()
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
public class Laboratory {
	
	
	/**
	 * Constructor for the class Laboratory.
	 * @param	capacity
	 * 			The capacity of the Laboratory, relative to the STOREROOM Quantity unit.
	 * @post	The capacity of the Laboratory has been set.
	 * 			| new.getAmount() == capacity
	 * @throws	IllegalArgumentException if the given capacity is not a proper capacity
	 * 			for a Laboratory.
	 * 			| !canHaveAsCapacity(capacity)
	 */
	public Laboratory(int capacity) throws IllegalArgumentException{
		if (!canHaveAsCapacity(capacity)) {
			throw new IllegalArgumentException("The given capacity is not a proper capacity for a Laboratory.");
		} else {
			this.CAPACITY = capacity;
		}
	}
	
	@Override
	public String toString() {
		String returnString = "";
		//OS independent 'end of line'
		String eol = System.getProperty("line.separator");
		LaboratoryIngredientIterator iterator = iterator();
		while (iterator.getNbElements() >= 0) {
			returnString += "Name ingredient: " + iterator.getCurrent().getSimpleName() + eol +
					"Amount available: " + iterator.getCurrent().getAmount() + " " + iterator.getCurrent().getUnit().toString() +
					eol + eol;
			iterator.advance();
			}
		if (returnString == "") {
			returnString = "A summary of the ingredients in this Laboratory was demanded, but "
					+ "this Laboratory has no ingredients.";
		}
		return returnString;
	}
	
	
	/**
	 * Method to check if a Laboratory has proper ingredients.
	 * @return	True if and only if the Laboratory contains all the simple names and, if the ingredients are also
	 * 			AlchemicMixture's that contain special names, special names of the ingredients.
	 * 			| if (for each AlchemicIngredient ingredient in getIngredients().values():
	 * 			|		getIngredients().containsKey(ingredient.getSimpleName()) &&
	 * 			|		( if (ingredient instanceof AlchemicMixture && ingredient.hasSpecialName():
	 * 			|			getIngredients().containsKey(ingredient.getSpecialName()))))
	 * 			|	then result == true
	 * 			| else result == false
	 */
	public boolean hasProperIngredients(){
		boolean returnValue = true;
		for (AlchemicIngredient ingredient:getIngredients().values()) {
			returnValue = returnValue && getIngredients().containsKey(ingredient.getSimpleName());
			if (ingredient instanceof AlchemicMixture && ((AlchemicMixture) ingredient).hasSpecialName()) {
				returnValue = returnValue && getIngredients().containsKey(((AlchemicMixture)ingredient).getSpecialName());
			}
		}
		return returnValue;
	}
	
	/**
	 * Method to check if an ingredient from a given IngredientContainer would be a proper ingredient
	 * for the Laboratory.
	 * @param	container
	 * 			The given IngredientContainer with the ingredient.
	 * @return	True if and only if the container is effective, is not empty, is not terminated and if adding the ingredient
	 * 			to the Laboratory wouldn't exceed the lab's maximum capacity; false otherwise.
	 * 			| container != null && !container.isEmpty() && !container.isTerminated() && canHaveAsAmount(
	 * 			|	getAmount() + container.getIngredient().getAmount() * 
	 * 			|	container.getIngredient().getUnit().convertTo(getAmountUnit()))
	 */
	public boolean canHaveAsIngredient(IngredientContainer container ) {
		return container != null && !container.isEmpty() && !container.isTerminated() && canHaveAsAmount(
				(getAmount() + container.getIngredient().getAmount() * container.getIngredient().getUnit().convertTo(getAmountUnit())));
	}
	
	
	/**
	 * Method to check if the Laboratory contains a certain ingredient with a given name.
	 * @param 	name
	 * 			The name of the ingredient to find in the Laboratory.
	 * @return	True if and only if the Laboratory contains the ingredient with the given name; false otherwise.
	 * 			| result == (getIngredient(name) != null)
	 */
	public boolean hasIngredient(String name) {
		return getIngredient(name) != null;
	}
	
	/**
	 * Method to retrieve the full amount of an ingredient with a given name.
	 * @param 	name
	 * 			The given name of the ingredient.
	 * @return	The full amount of the ingredient (with the given name) in an IngredientContainer.
	 * 			| result == retrieveIngredient(name, getIngredient(name).getAmount(), getIngredient(name).getUnit())
	 * @throws	IllegalArgumentException if no ingredient with the given name were found in the Laboratory.
	 * 			| getIngredient(name) == null
	 */
	public IngredientContainer retrieveIngredient(String name) {
		AlchemicIngredient ingredient = getIngredient(name);
		if (ingredient == null) {
			throw new IllegalArgumentException("The Laboratory doesn't contain the demanded ingredient with the given name."); 
		} else {
			return retrieveIngredient(name, ingredient.getAmount(), ingredient.getUnit());
		}
	}
	
	
	/**
	 * Method to retrieve a given amount (with a unit) of an ingredient with a given name from the Laboratory.
	 * @param	name
	 * 			The given name of the ingredient.
	 * @param	amount
	 * 			The amount of the ingredient to retrieve.
	 * @param	unit
	 * 			The unit of the given amount.
	 * @effect	The amount of the Laboratory has been decreased by the given amount.
	 * 			|	increaseAmountLaboratory(-amount, unit)
	 * @effect	The amount of the ingredient in the Laboratory has been changed.
	 * 			| changeAmountAlreadyPresentIngredient(getIngredient(name), -amount, name)
	 * @return	If the demanded amount is more than the Laboratory can possible retrieve, according to the State of the ingredient in the Laboratory,
	 * 			then this maximum amount of the ingredient will be returned; otherwise the demanded amount of the ingredient will be returned.
	 * 			| if (amount * unit.convertTo(getIngredient(name).getState().getMaximumQuantityToRetrieveFromLaboratory()) > 1)
	 * 			| 	then result == new IngredientContainer(getIngredient(name).getState().getMaxQuantityToRetrieveFromLaboratory(),
	 * 			|			new AlchemicIngredient(getIngredient(name).getState(), getIngredient(name).getType(), 1, getIngredient(name).getState().getMaxQuantityToRetrieveFromLaboratory()))
	 * 			| else   result == new IngredientContainer((getIngredient(name).getState().getMaxQuantityToRetrieveFromLaboratory(),
	 * 			|			new AlchemicIngredient(getIngredient(name).getState(), getIngredient(name).getType(), amount, unit);
	 * @throws	IllegalArgumentException
	 * 			If the demanded ingredient is not available in the given amount, or is not available at all.
	 * 			| getIngredient(name) == null || getIngredient(name).getAmount() * getIngredient(name).getUnit().convertTo(unit) < amount
	 */
	public IngredientContainer retrieveIngredient(String name, int amount, Quantity unit) throws IllegalArgumentException {
		AlchemicIngredient ingredient = getIngredient(name);
		if (ingredient == null) {
			throw new IllegalArgumentException("The Laboratory doesn't contain an ingredient with the given name.");
		} else if ( ingredient.getAmount() * ingredient.getUnit().convertTo(unit) < amount) {
			throw new IllegalArgumentException("The Laboratory doesn't contain the demanded amount of the demanded ingredient.");
		} else {
			increaseAmountLaboratory(-amount, unit);
			changeAmountAlreadyPresentIngredient(ingredient, -amount, unit);
			if (amount * unit.convertTo(ingredient.getState().getMaxQuantityToRetrieveFromLaboratory()) > 1) {
				amount = 1; unit = ingredient.getState().getMaxQuantityToRetrieveFromLaboratory();
			}
			if (ingredient instanceof AlchemicMixture) {
				AlchemicMixture returnMixture = new AlchemicMixture(ingredient.getState(), ingredient.getType(), amount, unit, ingredient.getCharacteristicalVolatility());
				returnMixture.setLaboratory(this);return new IngredientContainer(ingredient.getState().getMaxQuantityToRetrieveFromLaboratory(), returnMixture);
			} else {
				AlchemicIngredient returnIngredient = new AlchemicIngredient(ingredient.getState(), ingredient.getType(), amount, unit);
				returnIngredient.setLaboratory(this);return new IngredientContainer(ingredient.getState().getMaxQuantityToRetrieveFromLaboratory(), returnIngredient);
			}	}
	}
	
	/**
	 * Method to add an ingredient to a Laboratory.
	 * @param 	containerWithIngredient
	 * 			the given IngredientContainer.
	 * @post	If the ingredient was already present in the old Laboratory, the amount will be increased (according to the new Quantity unit).
	 * 			| if (old.hasIngredient(containerWithIngredient.getIngredient().getSimpleName()) == true)
	 * 			| 	then new.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getAmount() == old.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getAmount() *
	 * 			|		old.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit().convertTo(new.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit())
	 * 			| 		+ containerWithIngredient.getIngredient().getAmount() * 
	 * 			|		containerWithIngredient.getIngredient().getUnit().convertTo(new.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit())
	 * @post	The unit of the ingredient in the Laboratory will be set to the minimum Quantity unit of the old ingredient in the Laboratory
	 * 			and the Quantity unit of the ingredient in the given IngredientContainer.
	 * 			| if (old.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit().convertTo(
	 * 			|		containerWithIngredient.getIngredient().getUnit() < 1)
	 * 			| 	then new.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit() == old.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit()
	 * 			| else new.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getUnit() == old.containerWithIngredient.getIngredient().getUnit()
	 * @effect	The characteristic volatility of the ingredient has been changed.
	 * 			|new.getIngredient(containerWithIngredient.getIngredient().getSimpleName()).getCharacteristicalVolatility() = calculateCharacteristicalVolatility(
	 * 			|	containerWithIngredient, old.getIngredient(containerWithIngredient.getIngredient().getSimpleName()))
	 * @effect	If the ingredient wasn't already a part of the Laboratory, the ingredient will be added.
	 * 			| if (old.hasIngredient(containerWithIngredient.getIngredient().getSimpleName()) == false)
	 * 			|then addNewIngredient(old.ingredientContainer)
	 * @effect	The original IngredientContainer will be terminated.
	 * 			| new.containerWithIngredient.isTerminated() == true
	 * @effect	The ingredient in the given container will be warmed up (respectively cooled down) to it's default temperature if needed.
	 * 			| bringToStandardTemperature(old.containerWithIngredient)
	 * @effect	The amount of the Laboratory has been increased.
	 * 			| increaseAmountLaboratory(old.containerWithIngredient.getIngredient().getAmount(), old.containerWithIngredient.getIngredient().getUnit())
	 * @throws	IllegalArgumentException if the given container is not a proper IngredientContainer for this Laboratory.
	 * 			| !canHaveAsIngredient(containerWithIngredient)
	 */
	public void addIngredient(IngredientContainer containerWithIngredient) throws IllegalArgumentException{
		if (!canHaveAsIngredient(containerWithIngredient)) throw new IllegalArgumentException("The given IngredientContainer is empty or not effective.");
		IngredientContainer newContainer = bringToStandardTemperature(containerWithIngredient);
		AlchemicIngredient ingredientContainer = newContainer.getIngredient();
		AlchemicIngredient ingredient = getIngredient(ingredientContainer.getSimpleName());
		if (ingredient == null) {
			addNewIngredient(ingredientContainer);
		} else {
			Quantity newUnit = (ingredient.getUnit().convertTo(ingredientContainer.getUnit()) < 1)? ingredient.getUnit():ingredientContainer.getUnit();
			int newAmount = (int) (ingredientContainer.getAmount() * ingredientContainer.getUnit().convertTo(newUnit) +
					ingredient.getAmount() * ingredient.getUnit().convertTo(newUnit));
			double newCharVolatility = calculateCharacteristicalVolatility(newContainer, ingredient);//Preconditions always satisfied.	
			removeIngredient(ingredient.getSimpleName());
			AlchemicIngredient addIngredient = new AlchemicIngredient(ingredientContainer.getState(), ingredientContainer.getType(), newAmount, newUnit, newCharVolatility);
			addNewIngredient(addIngredient);
		}
		increaseAmountLaboratory(ingredientContainer.getAmount(), ingredientContainer.getUnit());
		containerWithIngredient.terminate();
	}
	
	/**
	 * Method to add a new ingredient to the Laboratory.
	 * @param 	ingredient
	 * 			The ingredient to add.
	 * @post	The ingredient has been added with his simple name.
	 * 			| new.hasIngredient(ingredient.getSimpleName()) == true
	 * @post	If the ingredient is an AlchemicMixture and also has a special name, the ingredient will
	 * 			also be added with it's special name.
	 * 			| if (ingredient instanceof AlchemicMixture && ((AlchemicMixture)ingredient).hasSpecialName())
	 * 			|	then new.hasIngredient(((AlchemicMixture)ingredient).getSpecialName()) == true
	 *@throws	IllegalArgumentException if the given ingredient is not effective.
	 *			| ingredient == null
	 */
	@Model
	void addNewIngredient(AlchemicIngredient ingredient) throws IllegalArgumentException{
		if (ingredient == null) {
			throw new IllegalArgumentException("The given ingredient is not effective.");
		} else {
			this.ingredients.put(ingredient.getSimpleName(), ingredient);
			if (ingredient instanceof AlchemicMixture && ((AlchemicMixture)ingredient).hasSpecialName()) {
				this.ingredients.put(((AlchemicMixture) ingredient).getSpecialName(), ingredient);
			}
			//Future proof concept: in case someone writes new methods that allow users to change the special
			//names of ingredients: this method makes it also possible to change the special name of that ingredient in the Laboratory.
			//Also makes it possible to only use ingredients in devices from the same Laboratory. Otherwise,
			//it would be possible to add ingredients to a device that are no retrieved from the same Laboratory.
			ingredient.setLaboratory(this);
		}
	}
	
	
	
	/**
	 * Method to remove the ingredient with the given name from the Laboratory.
	 * @param	name
	 * 			The simple or special name of the ingredient.
	 * @post	The ingredient is removed from the Laboratory with it's simple name.
	 * 			| if old.hasIngredient(name) == true then 
	 * 			|	new.hasIngredient(name) == false
	 * @post	If the given name is linked to an AlchemicMixture and if
	 * 			that mixture has a special name, the special name will also be removed from
	 * 			the Laboratory.
	 * 			| if (getIngredient(name) instanceof AlchemicMixture) then
	 * 			| 		new.hasIngredient(((AlchemicMixture)getIngredient(name)).getSpecialName()) == false
	 * @throws	IllegalArgumentException if the given ingredient is not found.
	 * 			| hasIngredient(name) == false
	 */
	@Model
	void removeIngredient(String name) throws IllegalArgumentException{
		AlchemicIngredient ingredient = this.ingredients.get(name);
		if (ingredient == null) {
			throw new IllegalArgumentException("No ingredients with the given name are found.");
		} else {
			this.ingredients.remove(ingredient.getSimpleName());
			if (ingredient instanceof AlchemicMixture && ((AlchemicMixture)ingredient).hasSpecialName()) {
				this.ingredients.remove(((AlchemicMixture)ingredient).getSpecialName());
			}
		}
	}
	
	
	/**
	 * Method to calculate the new characteristic volatility from a given IngredientContainer and a given AlchemicIngredient.
	 * @pre		The given container is effective and is not empty.
	 * 			| container != null && !container.isEmpty()
	 * @pre		The given ingredient is effective.
	 * 			| ingredient != null
	 * @param 	container
	 * 			The given IngredientContainer.
	 * @param 	ingredient
	 * 			The given Ingredient.
	 * @return	The new characteristical volatility, calculated as the weighted average of the characteristical volatility of the ingredient from the container
	 * 			and the volatility of the given ingredient.
	 * 			| if (container.getIngredient().getUnit().convertTo(ingredient.getUnit()) < 1)
	 * 			| 	then result == ((container.getIngredient().getAmount() * container.getIngredient().getCharacteristicalVolatility()) + (ingredient.getAmount() * 
	 * 			|		ingredient.getUnit().convertTo(container.getIngredient().getUnit()) * ingredient.getCharacteristicalVolatility())) /
	 * 			|		(container.getIngredient().getAmount() + ingredient.getAmount() * ingredient.getUnit().convertTo(container.getIngredient().getUnit()))
	 * 			| else result == ((container.getIngredient().getAmount() * container.getIngredient().getUnit().convertTo(ingredient.getUnit())
	 * 			|		 * container.getIngredient().getCharacteristicalVolatility()) + 
	 * 			|		(ingredient.getAmount() * ingredient.getCharacteristicalVolatility())) /
	 * 			|		(container.getIngredient().getAmount() * container.getIngredient().getUnit().convertTo(ingredient.getUnit() + ingredient.getAmount())
	 */
	@Model
	private double calculateCharacteristicalVolatility(IngredientContainer container, AlchemicIngredient ingredient){
		AlchemicIngredient newIngredient = container.getIngredient();
		Quantity minQuantity = (newIngredient.getUnit().convertTo(ingredient.getUnit()) < 1)? newIngredient.getUnit():ingredient.getUnit();
		double num = (newIngredient.getAmount() * newIngredient.getUnit().convertTo(minQuantity) * newIngredient.getCharacteristicalVolatility()) + 
				(ingredient.getAmount() * ingredient.getUnit().convertTo(minQuantity) * ingredient.getCharacteristicalVolatility());
		double denum = (newIngredient.getAmount() * newIngredient.getUnit().convertTo(minQuantity)) + (ingredient.getAmount() * ingredient.getUnit().convertTo(minQuantity));
		return num/denum;
	}
	
	
	/**
	 *Method to bring the ingredient of a container to it's default temperature.
	 *@param	container
	 *			The container with the ingredient.
	 *@pre		The given container is effective and not empty.
	 *			| container != null && !container.isEmpty()
	 *@effect	If the temperature of the ingredient is hotter (respectively colder) than it's default temperature,
	 *			the CoolingBox (respectively the Oven) of this Laboratory will be cleaned and it's temperature will be set
	 *			to the default temperature of the ingredient.
	 *			| if (container.getIngredient().getTemperature().isColderThan(container.getIngredient().getType().getTemperature()))
	 *			|	then getDevice(Oven.class).clean() && getDevice(Oven.class).setTemperature(
	 *			|		container.getIngredient().getType().getTemperature().getTemperatureCold(),
	 *			|		container.getIngredient().getType().getTemperature().getTemperatureHot())
	 *			| else if (container.getIngredient().getTemperature().isHotterThan(container.getIngredient().getType().getTemperature()))
	 *			|	then getDevice(CoolingBox.class).clean() && getDevice(CoolingBox.class).setTemperature(
	 *			|		container.getIngredient().getType().getTemperature().getTemperatureCold(),
	 *			|		container.getIngredient().getType().getTemperature().getTemperatureHot())
	 *@return	If the temperature of the ingredient in the given container is colder than the standard temperature of it's IngredientType,
	 * 			the ingredient will be warmed up to that temperature with a maximum offset of 5%. Else, if the temperature is hotter
	 * 			than the standard temperature of the IngredientType, the ingredient will be cooled down precisely to the last temperature with a CoolingBox.
	 * 			If none of the two above mentioned conditions apply here, the given container is returned.
	 * 			| if (container.getIngredient().getTemperature().isColderThan(container.getIngredient().getType().getTemperature()))
	 * 			|	then result.getIngredient().getTemperature().isHotterThan(container.getType().getTemperature() * 0.95)
	 * 			|		&& result.getIngredient().getTemperature().isColderThan(container.getType().getTemperature() * 1.5)
	 * 			| else result.getIngredient().getTemperature().equals(container.getIngredient().getType().getTemperature())
	 * @throws	IllegalStateException if the ingredient in the given IngredientContainer should be warmed up with an Oven or
	 * 			cooled down with a CoolingBox, but the required device is not available in the Laboratory; solve this
	 * 			exception by adding the respectively device first before using this method.
	 * 			| ((container.getIngredient().getTemperature().isColderThan(container.getIngredient().getType().getTemperature()))
	 * 			|	&& getDevice(CoolingBox.class) == null) ||
	 * 			| ((container.getIngredient().getTemperature().isHotterThan(container.getIngredient().getType().getTemperature()))
	 * 			|	&& getDevice(Oven.class) == null)
	 */
	@Model
	private IngredientContainer bringToStandardTemperature(IngredientContainer container) throws IllegalStateException{
		//First line to make the code more readable.
		AlchemicIngredient ingredientContainer = container.getIngredient();
		IngredientContainer result;
		if (ingredientContainer.getTemperature().equals(ingredientContainer.getType().getTemperature())) {
			result = container;
		} else {
			DeviceTemperatureController device =  (ingredientContainer.getTemperature().isHotterThan(ingredientContainer.getType().getTemperature()))? 
					(DeviceTemperatureController) getDevice(CoolingBox.class):(DeviceTemperatureController) getDevice(Oven.class);
			if (device == null) {
				throw new IllegalStateException("The given container contains an ingredient of which the temperature is not it's own default"
						+ " temperature, but no devices are available in this Laboratory to change it to the default temperature.");
			} else {
				device.clean();
				device.setTemperature(ingredientContainer.getType().getTemperature().getTemperatureCold(), 
						ingredientContainer.getType().getTemperature().getTemperatureHeat());
				device.addIngredientContainer(container); device.execute(); result = device.getResult(); device.clean();
			}
		}
		return result;
	}
	
	
	/**
	 * Method to change the amount of an ingredient that's already available in the Laboratory.
	 * @param	ingredient
	 * 			The ingredient to reduce.
	 * @param	amountChange
	 * 			The amount to reduce the ingredient with.
	 * @param	unit
	 * 			The unit of the amountChange parameter.
	 * @post	If the given amountChange is not a negative representation of the amount of the ingredient present
	 * 			in the Laboratory, the amount of ingredient in the Laboratory will be adapted to the given amountChange parameter.
	 * 			| new.getIngredient(ingredient.getSimpleName()).getAmount() ==
	 * 			|	old.getIngredient(ingredient.getSimpleName()).getAmount() *
	 * 			|	old.getIngredient(ingredient.getSimpleName()).getUnit().convertTo(new.getIngredient(ingredient.getSimpleName()).getUnit()) +
	 * 			|	ingredient.getAmount() * ingredient.getUnit().convertTo(new.getIngredient(ingredient.getSimpleName()).getUnit())
	 * @effect	If the given amountChange equals the amount of the ingredient present in the Laboratory, but is negative, 
	 * 			the ingredient will be removed from the Laboratory.
	 * 			| if (((getIngredient(ingredient.getSimpleName()).getUnit().convertTo(ingredient.getUnit()) < 1) &&
	 * 			|	(getIngredient(ingredient.getSimpleName()).getAmount() + amountChange * unit.convertTo(getIngredient(ingredient.getSimpleName()).getUnit())) < 0) 
	 * 			|	||
	 * 			|	(((getIngredient(ingredient.getSimpleName()).getUnit().convertTo(ingredient.getUnit()) > 1) && 
	 * 			|	(getIngredient(ingredient.getSimpleName()).getAmount() * getIngredient(ingredient.getSimpleName()).getUnit().convertTo(ingredient.getUnit()) + amountChange < 0)))
	 * 			| then removeIngredient(ingredient.getSimpleName())
	 * @throws	IllegalArgumentException if the ingredient is not present in the Laboratory.
	 * 			| hasIngredient(ingredient.getSimpleName())
	 * @throws	IllegalArgumentException if the given reducement would cause a negative amount of the ingredient in the Laboratory.
	 * 			|  ((getIngredient(ingredient.getSimpleName()).getUnit().convertTo(ingredient.getUnit()) < 1) &&
	 * 			|	(getIngredient(ingredient.getSimpleName()).getAmount() + amountChange * unit.convertTo(getIngredient(ingredient.getSimpleName()).getUnit()))) ||
	 * 			| ((getIngredient(ingredient.getSimpleName()).getUnit().convertTo(ingredient.getUnit()) > 1) &&
	 * 				(getIngredient(ingredient.getSimpleName()).getAmount() * getIngredient(ingredient.getSimpleName()).getUnit().convertTo(unit) + amountChange))
	 * 
	 */
	@Model
	private void changeAmountAlreadyPresentIngredient(AlchemicIngredient ingredient, long amountChange, Quantity unit) 
	throws IllegalArgumentException{
		AlchemicIngredient oldIngredient = getIngredient(ingredient.getSimpleName());
		if (oldIngredient == null) throw new IllegalArgumentException("Ingredient not avaible in the Laboratory.");
		Quantity newUnit = (oldIngredient.getUnit().convertTo(ingredient.getUnit()) < 1)? oldIngredient.getUnit():ingredient.getUnit();
		int newAmount = (int) (oldIngredient.getAmount() * oldIngredient.getUnit().convertTo(newUnit) + amountChange * unit.convertTo(newUnit));
		if (newAmount < 0) throw new IllegalArgumentException("The given reducement of the given ingredient in the Laboratory is not valid.");
		AlchemicIngredient newIngredient = new AlchemicIngredient(ingredient.getState(), ingredient.getType(), newAmount, newUnit);
		removeIngredient(ingredient.getSimpleName());
		if (newAmount > 0) {
			addNewIngredient(newIngredient);
		}
		
	}
	
	
	/**
	 * Method to return the AlchemicIngredient from the Laboratory mapped to the given
	 * name (Olog(n)).
	 * @param	name
	 * 			The simple or special name of the AlchemicIngredient
	 * @return	The AlchemicIngredient with the corresponding name, or null if
	 * 			the ingredient is not found.
	 * 			| if (!hasIngredient(name))
	 * 			|	then result == null
	 * 			| else result == getIngredients().get(name)
	 */
	@Model
	private AlchemicIngredient getIngredient(String name) {
		if (name == null) {
			return null;
		} else {
			return this.ingredients.get(name);
		}
		
	}
	
	

	
	
	/**
	 * Method to return a copy of the ingredient's TreeMap of the Laboratory.
	 * @return	A copy of the ingredient's TreeMap.
	 * 			| result == new TreeMap<String, AlchemicIngredient>(this.ingredients)
	 */
	@Model
	private TreeMap<String, AlchemicIngredient> getIngredients(){
		return new TreeMap<String, AlchemicIngredient>(this.ingredients);
	}
	
	

	
	
	/**
	 * TreeMap with as keys the simple (and, if they have one, special) names of the ingredients
	 * in the Laboratory and as mapped values the AlchemicIngredient's.
	 *@invar	A Laboratory always needs to contain proper ingredients.
	 *			| hasProperIngredients()
	 */
	private TreeMap<String, AlchemicIngredient> ingredients = new TreeMap<String, AlchemicIngredient>();
	
	
	
	/**
	 * Method to indicate if a given amount would be a proper amount (of the ingredients) for a Laboratory,
	 * relative to the Quantity unit of getAmountUnit().
	 * @param	amount
	 * 			The amount to check.
	 * @return	True if and only if the amount is strictly positive and doesn't exceed the capacity
	 * 			of the Laboratory; false otherwise.
	 * 			| result == amount >= 0 && amount * getAmountUnit().convertTo(Quantity.STOREROOM) <= capacity
	 */
	public boolean canHaveAsAmount(double amount) {
		return amount >= 0 && amount * getAmountUnit().convertTo(Quantity.STOREROOM) <= getCapacity();
	}
	
	
	/**
	 * Method to get the amount of ingredients in the Laboratory, relative to the Quantity unit
	 * of the method getAmountUnit().
	 * @return	The amount of the ingredients in the Laboratory.
	 */
	@Basic
	public long getAmount() {
		return this.amount;
	}
	
	
	
	/**
	 * Method to set the amount of ingredients in a Laboratory.
	 * @param	amount
	 * 			The new amount.
	 * @post	The amount of the Laboratory has been set to the given amount.
	 * 			| new.getAmount() == amount
	 * @throws	IllegalArgumentException if the given amount is not a proper amount
	 * 			for the Laboratory.
	 * 			| !canHaveAsAmount(amount)
	 */
	private void setAmount(int amount) throws IllegalArgumentException{
		if (!canHaveAsAmount(amount)) {
			throw new IllegalArgumentException("The given amount is not a proper amount for a Laboratory.");
		} else {
			this.amount = amount;
		}
	}
	
	
	/**
	 * Method to increase the amount of ingredients in the Laboratory.
	 * @param	amount
	 * 			The amount to increase the current amount with; this method
	 * 			also supports negative integers.
	 * @param	unit
	 * 			The unit of the given amount.
	 * @effect	The AmountUnit of the Laboratory has been set to the Quantity with the lowest value
	 * 			of the original AmountUnit of the Laboratory and the given Quantity unit.
	 * 			| if (unit.convertTo(getAmountUnit()) < 1)
	 * 			|	then setAmountUnit(unit)
	 * @effect	The amount of the ingredients within the Laboratory has been increased.
	 * 			| if (unit.convertTo(getAmountUnit()) < 1)
	 * 			|	then setAmount((int) amount + getAmount()*getAmountUnit().convertTo(unit))
	 * 			| else setAmount(int) amount * unit.convertTo(getAmountUnit()) + getAmount())
	 * @throws 	IllegalArgumentException if the given increasment of the amount of ingredients within the Laboratory
	 * 			is not a proper increasment.
	 * 			| amount * unit.convertTo(Quantity.STOREROOM) + getAmount() * getAmountUnit().convertTo(Quantity.STOREROOM) > getCapacity()
	 */
	private void increaseAmountLaboratory(int amount, Quantity unit) throws IllegalArgumentException{
			if (amount * unit.convertTo(Quantity.STOREROOM) + getAmount() * getAmountUnit().convertTo(Quantity.STOREROOM) > getCapacity()) {
				throw new IllegalArgumentException("The given amount is not a proper amount to increase the amount of the Laboratory with.");
			} else {
				Quantity newUnit = (unit.convertTo(getAmountUnit()) < 1)? unit:getAmountUnit();
				int newAmount = (int) (amount * unit.convertTo(newUnit) + getAmount() * getAmountUnit().convertTo(newUnit));
				setAmountUnit(newUnit); setAmount(newAmount);
			}
	}
	
	
	/**
	 * The amount of ingredients in the Laboratory, respectively to the
	 * unit of the amount.
	 * @invar	The amount of the Laboratory must be a proper amount for that Laboratory.
	 * 			| canHaveAsAmount(getAmount()
	 */
	private long amount = 0;
	
	
	/**
	 * Method to check if an Laboratory can have a given unit as the
	 * unit of it's amount.
	 * @return	True if and only if the given Quantity unit
	 * 			is effective; otherwise false.
	 * 			| if (getAmountUnit() != null)
	 * 			|	then result == true
	 * 			| else result == false
	 */
	public boolean canHaveAsAmountUnit(Quantity amountUnit) {
		return amountUnit != null;
	}
	
	
	/**
	 * Method to return the Quantity unit of the amount of the Laboratory.
	 * @return	The unit of the amount.
	 */
	@Basic
	public Quantity getAmountUnit() {
		return this.amountUnit;
	}
	
	
	/**
	 * Method to set the Quantity unit of the amount of the Laboratory.
	 * @pre		The given Quantity is a proper Quantity
	 * 			as the unit of the amount of the Laboratory.
	 * 			| canHaveAsAmountUnit(newUnit)
	 * @param	newUnit
	 * 			The new unit of the amount.
	 * @post	The unit of the amount has been set to the given Quantity.
	 * 			| new.getAmountUnit() == newUnit
	 */
	private void setAmountUnit(Quantity newUnit) {
		this.amountUnit = newUnit;
	}
	
	
	
	
	/**
	 * Unit of the amount of the Laboratory.
	 * @invar	The amountUnit must be a proper Quantity
	 * 			for the Laboratory.
	 * 			| canHaveAsAmountUnit(getAmountUnit())
	 */
	private Quantity amountUnit = Quantity.STOREROOM;
	
	
	/**
	 * Method to check if a given capacity would be a proper capacity
	 * for a Laboratory.
	 * @param	capacity
	 * 			The capacity to check, relatively to the STOREROOM quantity from the Quantity enumerator.
	 * @return	True if and only if the given capacity is strictly positive;
	 * 			false otherwise.
	 * 			| result == (capacity > 0)
	 */
	public boolean canHaveAsCapacity(int capacity) {
		return capacity > 0;
	}
	
	
	/**
	 * Method to return the capacity of a Laboratory.
	 * @return	The capacity of the Laboratory, relatively
	 * 			to the STOREROOM quantity from the Quantity enumerator.
	 */
	@Basic @Immutable
	public double getCapacity() {
		return this.CAPACITY;
	}
	
	
	/**
	 * The capacity of the Laboratory, relatively to the STOREROOM quantity
	 * from the Quantity enumerator.
	 * @invar	The capacity must be a proper capacity for the Laboratory.
	 * 			| canHaveAsCapacity(getCapacity())
	 */
	private final int CAPACITY;
	
	
	
	/**
	 * Method to check if a Laboratory contains proper devices.
	 * @return	True and only if all the devices are linked to this Laboratory; false otherwise.
	 * 			| if (for Device device in getDevices().values(): device.getLaboratory() == this)
	 * 			|	then result == true
	 * 			| else result == false
	 */
	public boolean hasProperDevices() {
		boolean returnValue = true;
		for (Device device:getDevices().values()) {
			returnValue = returnValue && device.getLaboratory() == this;
		}
		return returnValue;
	}
	
	/**
	 * Method to return the device of a certain type from the Laboratory.
	 * @param	classDevice
	 * 			The class of the device.
	 * @return	The device from the Laboratory, or null if no device from
	 * 			the given class are found.
	 * 			| result == getDevices().get(classDevice)
	 */
	public Device getDevice(Class<? extends Device> classDevice) {
		return getDevices().get(classDevice);
	}
	
	
	/**
	 * Method to return a HashMap with all the devices of the Laboratory.
	 * @return	The HashMap with the devices of the Laboratory.
	 */
	@Basic @Model
	private HashMap<Class<? extends Device>, Device> getDevices(){
		return new HashMap<Class<? extends Device>, Device>(this.devices);
	}
	
	
	/**
	 * Method to add a device to a Laboratory.
	 * @param	device
	 * 			A device to add to the Laboratory.
	 * @post	The device has been added to the Laboratory; if a device from the same
	 * 			class already existed, the old device will be removed from the Laboratory.
	 * 			Amounts of ingredient in the old device won't be part of the Laboratory any longer.
	 * 			| new.getDevice(device.getClass()) == device &&
	 * 			| if (old.getDevice(device.getClass()) != device) then
	 * 			|	old.getDevice(device.getClass()) != new.getDevice(device.getClass())
	 * @effect	The device has been linked to the Laboratory.
	 * 			|	device.setLaboratory(this)
	 * @throws	IllegalArgumentException if the given device is not effective.
	 * 			| device == null
	 */
	public void addDevice(Device device) {
		if (device == null) {
			throw new IllegalArgumentException("The given device is not effective.");
		} else {
			devices.put(device.getClass(), device);
			device.setLaboratory(this);
		}
	}
	
	/**
	 * Method to remove a device from the Laboratory.
	 * @param	device
	 * 			The device to remove from the Laboratory.
	 * @post	The device is removed from the Laboratory.
	 * 			| new.getDevice(device.getClass()) == null
	 * @effect	The Device is unlinked from the Laboratory.
	 * 			| device.removeLaboratory()
	 * @throws	IllegalArgumentException if the given device is not a valid device or if the Laboratory
	 * 			doesn't contain the given device.
	 * 			| device == null || getDevice(device.getClass()) != device
	 */
	public void removeDevice(Device device) {
		if (device == null) {
			throw new IllegalArgumentException("The given device is not effective.");
		} else {
			Device deviceLaboratory = getDevice(device.getClass());
			if (deviceLaboratory != device){
				throw new IllegalArgumentException("The Laboratory doesn't contain the given device.");
			} else {
				devices.remove(device.getClass());
				deviceLaboratory.removeLaboratory();
			}
		}
	}
	
	/**
	 * Map representing the devices in a Laboratory.
	 * @invar	A Laboratory must have proper devices.
	 * 			| hasProperDevices()
	 */
	private Map<Class<? extends Device>, Device> devices = new HashMap<Class<? extends Device>, Device>();
	
	
	/**
	 * Method returning an iterator for the ingredients of the Laboratory.
	 * @return	A new iterator.
	 * 			| result == new LaboratoryIngredientIterator()
	 */
	public LaboratoryIngredientIterator iterator() {
		return new LaboratoryIngredientIterator() {
			
			
			/**
			 * Index of the iterator.			 
			 * */
			private int index = 1;
			
			/**
			 * SortedSet containing the ingredients of the Laboratory.
			 */
			private SortedSet<AlchemicIngredient> ingredients = new TreeSet<AlchemicIngredient>(getIngredients().values());
			 
			/**
			 * Array containing the ingredients of the Laboratory, sorted on their simple name.
			 */
			private ArrayList<AlchemicIngredient> ingredientsList = new ArrayList<AlchemicIngredient>(ingredients);
			
			
			@Override
			public void reset() {
				index = 1;
			}
			
			@Override
			public int getNbElements() {
				return this.ingredients.size() - index;
			}
			
			@Override
			public AlchemicIngredient getCurrent() throws IndexOutOfBoundsException {
				return ingredientsList.get(index - 1);
				
			}
			
			@Override
			public void advance() {
				index ++;
			}
		};
	}
	
	
	////////////////////////////////////////////////
	//From here: no documentation needed
	////////////////////////////////////////////////
	
	public void execute(Recipe recipe, int factor) {
		for (Device device:getDevices().values()) {device.clean();}
		ArrayList<AlchemicIngredient> ingredients = new ArrayList<AlchemicIngredient>();
		Integer indexIngredients = 1; //Also indicates if the execution is not successful: becomes -1.
		Commando curCommando = null;
		for (int index = 1; index <= recipe.getNumberOfInstructions() && indexIngredients != -1; index ++) {
			curCommando = recipe.getInstructionAt(index);
			if (curCommando.equals(Commando.ADD)) {
				ingredients.add(executeCommandoADD(factor, curCommando, recipe.getIngredientAt(indexIngredients)));
				indexIngredients ++;
			}
			else if (curCommando.equals(Commando.COOL) && !ingredients.isEmpty()) {
				ingredients = executeCommandoTEMPERATURE(ingredients, -50);//Fail: ingredients.add(null)
			} else if (curCommando.equals(Commando.HEAT) && !ingredients.isEmpty()) {
				ingredients = executeCommandoTEMPERATURE(ingredients, 50);//Fail: ingredients.add(null)
			} else if (curCommando.equals(Commando.MIX) && !ingredients.isEmpty()) {
				ingredients = executeCommandoMIX(ingredients); //Fail: ingredients.add(null)
			} else {
				ingredients.add(null); //Couldn't recognize commando ==> failed execution
			}
			indexIngredients = (ingredients.contains(null))? -1:indexIngredients;	//-1 if the execution is not successful.
		} if (curCommando != Commando.MIX && indexIngredients != -1) ingredients = executeCommandoMIX(ingredients);
		finalizeExecutionRecipe(ingredients);
	}
	
	
	private AlchemicIngredient executeCommandoADD(int factor, Commando command, AlchemicIngredient ingredient) {
		AlchemicIngredient returnValue = null;
		try {
			returnValue = retrieveIngredient(ingredient.getSimpleName(),factor * ingredient.getAmount(), 
					ingredient.getUnit()).getIngredient();
		} catch (Exception e) {};
		return returnValue;
	}
	
	private ArrayList<AlchemicIngredient> executeCommandoTEMPERATURE (ArrayList<AlchemicIngredient> ingredients, int upOrDownTemperatureDifference){
		ArrayList<AlchemicIngredient> returnValue = ingredients;
		AlchemicIngredient lastIngredient = ingredients.get(ingredients.size() - 1);
		returnValue.remove(ingredients.size() - 1);
		try {
			Device device = (upOrDownTemperatureDifference>0)? getDevice(Oven.class):getDevice(CoolingBox.class);
			if (device == null) throw new IllegalStateException();
			Temperature newTemperature = Temperature.convertLongToTemperature(
					Temperature.convertTemperatureToLong(lastIngredient.getTemperature()) + upOrDownTemperatureDifference);
			((DeviceTemperatureController)device).setTemperature(newTemperature.getTemperatureCold(), newTemperature.getTemperatureHeat());
			device.addIngredientContainer(new IngredientContainer(lastIngredient.getState().getMaxQuantityToRetrieveFromLaboratory(),
					lastIngredient));
			device.execute();
			lastIngredient = device.getResult().getIngredient(); 
			device.clean();
		}catch (Exception  e) {returnValue.add(null);}
		returnValue.add(lastIngredient);
		return returnValue;
	}
	
	
	private ArrayList<AlchemicIngredient> executeCommandoMIX (ArrayList<AlchemicIngredient> ingredients){
		ArrayList<AlchemicIngredient> returnValue = ingredients;
		try {
			Kettle kettle = (Kettle) getDevice(Kettle.class);
			if (kettle == null) throw new IllegalStateException();
			for (AlchemicIngredient ingredient: ingredients) {
				kettle.addIngredientContainer(new IngredientContainer(
						ingredient.getState().getMaxQuantityToRetrieveFromLaboratory(), ingredient));
			} kettle.execute();
			returnValue = new ArrayList<AlchemicIngredient>();
			returnValue.add(kettle.getResult().getIngredient());
			kettle.clean();
		} catch (Exception e) {
			returnValue.add(null);
		}
		return returnValue;
	}
	
	private void finalizeExecutionRecipe(ArrayList<AlchemicIngredient> ingredients) {
		for (AlchemicIngredient ingredient:ingredients) {
			if (ingredient != null) {
				try {
					IngredientContainer tempContainer = new IngredientContainer(
							ingredient.getState().getMaxQuantityToRetrieveFromLaboratory(), ingredient);
					addIngredient(tempContainer);
				} catch (Exception e) {};
			}
		}
	}
	
}
