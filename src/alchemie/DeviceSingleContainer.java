/**
 * 
 */
package alchemie;

import be.kuleuven.cs.som.annotate.*;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Class representing a device that can only contain one IngredientContainer.
 * @invar	A DeviceSingleContainer must have a proper IngredientContainer, or none.
 * 			| hasProperIngredientContainer()
 * @invar	A Device must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version	1.0
 */
abstract class DeviceSingleContainer extends Device {
	
	/**
	 * Method to check if a device has a proper IngredientContainer.
	 * @return	True if the device has a proper IngredientContainer; false otherwise.
	 * 			| if (canHaveAsIngredientContainer(getIngredientContainer() || getIngredientContainer() == null)
	 * 			|		then result == true
	 * 			| else result == false
	 */
	public boolean hasProperIngredientContainer() {
		return canHaveAsIngredientContainer(getIngredientContainer()) || getIngredientContainer() == null;
	}
	
	/**
	 * Inspector to return the IngredientContainer
	 * @return	The IngredientContainer as ingredient of this device.
	 */
	@Basic @Model
	protected IngredientContainer getIngredientContainer() {
		return this.ingredientContainer;
	};
	
	/**
	 * @post	The IngredientContainer of the device has been set to a clone of the given IngredientContainer.
	 * 			| getIngredientContainer() == new IngredientContainer(container.getCapacity(),
	 * 			|		container.getIngredient())
	 * @effect	The IngredientContainer is terminated.
	 * 			| container.terminate()
	 * @post	Potential results are removed from the device.
	 * 			| getResult() == null
	 * @effect	The given container is added to the device as how a container would have been added in the class Device.
	 * 			| super.addIngredientContainer(container)
	 */
	@Override @Raw
	public void 
	addIngredientContainer(IngredientContainer container){
		super.addIngredientContainer(container);
		clean();
		this.ingredientContainer = new IngredientContainer(container.getCapacity(), container.getIngredient());
		container.terminate();
	}
	/**
	 * A method to make this device empty.
	 * @post	The IngredientContainer is removed from the device.
	 * 			| new.getIngredientContainer() == null
	 */
	@Model
	protected void makeEmpty() {
		this.ingredientContainer = null;
	}
	
	
	/**
	 * @return 	True if and only if the Device doesn't contain a result, doesn't contain an IngredientContainer
	 * 			 and is not executed; false otherwise. 
	 * 			| if (getResult() == null && isExecuted() == false && getIngredientContainer() == null) then 	
	 * 			| 	result == true 	
	 * 			| else result == false
	 */
	@Override
	public boolean isCleaned() {
		return super.isCleaned() && getIngredientContainer() == null; 
	}
	
	/**
	 * Protected method to remove the IngredientContainer's from a device.
	 * 
	 * @effect	This DeviceSingleContainer is cleaned as a Device.
	 * 			| super.clean()
	 * @effect  the DeviceSingleContainer is made empty
	 *          |makeEmpty()
	 */
	@Override
	public void clean() {
		super.clean();
		makeEmpty();
	}
	
	/**
	 * Method to execute a device with it's own functionality.
	 * @throws 	IllegalStateException if the device is not a part of a Laboratory or doesn't
	 * 			contain a proper IngredientContainer.
	 * 			| getLaboratory() == null || hasProperIngredientContainer() == false || (getIngredientContainer() == null)
	 */
	public void execute() throws IllegalStateException{
		if (getLaboratory() == null || !hasProperIngredientContainer() || (getIngredientContainer() == null)) {
			throw new IllegalStateException("The Device can not be executed since the Device is not a "
					+ "part of a Laboratory or doesn't contain a proper IngredientContainer.");
		}
	}
	
	/**
	 * IngredientContainer used as ingredient for the device.
	 * @invar	A DeviceSingleContainer must always have a proper (or none) IngredientContainer.
	 * 			| hasProperIngredientContainer()
	 */
	private IngredientContainer ingredientContainer = null;
	

}
