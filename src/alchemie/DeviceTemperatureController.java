package alchemie;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import temperature.*; 

/**
 * Abstract class representing a device that can adjust
 * the temperature of the ingredient of a single container.
 * @invar	A DeviceSingleContainer must have a proper IngredientContainer.
 * 			| hasProperIngredientContainer()
 * @invar	The DeviceTemperatureController must always have a proper temperature.
 * 			| isValidTemperature(getTemperature().getTemperatureCold(), getTemperature().getTemperatureHeat())
 * @invar	A Device must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version	1.0
 */
abstract class DeviceTemperatureController extends DeviceSingleContainer {
	
	
	/**
	 * Static method to check if a temperature is a proper temperature for a device.
	 * @param	cold
	 * 			The cold temperature.
	 * @param	heat
	 * 			The heat temperature.
	 * @return	True if the cold temperature and the heat temperature
	 * 			are both strictly positive and at least one of them is zero; false otherwise.
	 *          | result == ((cold == 0 && heat >= 0)||
				(cold >= 0 && heat == 0))
	 */
	public static boolean isValidTemperature(long cold, long heat) {
		return ((cold == 0 && heat >= 0)||
				(cold >= 0 && heat == 0)); 
	}
	
	
	
	/**
	 * Inspector to return the temperature of a DeviceTemperatureController.
	 * @return	The temperature of the DeviceTemperatureController (long[]).
	 */
	@Basic
	public Temperature getTemperature() {
		return this.temperature ; 
	}
	
	/**
	 * Mutator to set the temperature of a DeviceTemperatureController.
	 * @param	cold
	 * 			The given cold  temperature.
	 * @param   heat 
	 *          The given heat temperature.
	 * @post 	The temperature is set to a temperature with given cold and heat
	 *       	| new.getTemperature()[0] = new Temperature(cold, heat).getTemperatureCold() 
	 *      	| new.getTemperature()[1] = new Temperature(cold, heat).getTemperaturHeat()
	 * @throws	IllegalArgumentException if the given cold and heat can't be used to set the temperature
	 * 			of the device.
	 * 			| !isValidTemperature(cold,heat)
	 */
	@Raw
	public void setTemperature(long cold, long heat) throws IllegalArgumentException{
		if (!isValidTemperature(cold, heat)) {
			throw new IllegalArgumentException("The given temperature is not a proper temperature for this device.");
		} else {
			Temperature newTemperature = new Temperature(cold, heat);
			this.temperature = newTemperature;
		}
	}
	
	
	
	/**
	 * Temperature of the DeviceTemperatureController.
	 * @invar 	The temperature must always a proper Temperature for the Device.
	 * 			| isValidTemperature(temperature.getTemperatureCold(), temperature.getTemperatureHeat())
	 */
	private Temperature temperature;
	
}
