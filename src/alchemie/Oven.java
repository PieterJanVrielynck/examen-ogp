package alchemie;

import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;
import temperature.*; 

/**
 * Class representing a cooling box.
 * @invar	An Oven must have a proper IngredientContainer.
 * 			| hasProperIngredientContainer()
 * @invar	A Device must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @invar	The Oven must always have a proper temperature.
 * 			| isValidTemperature(getTemperature().getTemperatureCold(), getTemperature().getTemperatureHeat())
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
public class Oven extends DeviceTemperatureController {

	/**
	 * Constructor for the Oven class with a given IngredientContainer and temperature.
	 * @param	cold
	 * 			The cold temperature.
	 * @param   Heat
	 *          the heat temperature 
	 * @post	The device is marked as not executed.
	 * 			| new.isExecuted() == false
	 * @post	The device is marked as cleaned; thus not only is the result
	 * 			set to null, but this device also doesn't contain an IngredientContainer to work with.
	 * 			| new.isCleaned() == true && new.getResult() == null
	 * @post	The Laboratory of the device has been unlinked by initialization.
	 * 			| new.getLaboratory() == null
	 * @effect	The temperature of the device has been set with given cold and heat.
	 * 			| setTemperature(cold, heat);
	 */
	@Raw
	public Oven(long cold, long heat) {
		setTemperature(cold, heat);
	}
	
	/**
	 * 	A method to heat up the content of an oven. 
	 * 
	 * @post The result of this device will be set to the ingredient container store in this device
	 *       | new.getResult() = old.getIngredientContainer()
	 * @effect The container is first made empty.
	 *         | makeEmpty()
	 * @effect The device is executed as in the class DeviceSingleContainer.
	 * 			| super.execute()
	 * @effect if the temperature of the ingredient is colder than the oven, taking the error margin into account,
	 *         then  the temperature will be heated up to the temperature of the oven. 
	 *         |  if (getResult().getIngredient().isColderThan(randomRoundingErrorTemperature())
	 *         | then getIngredientContainer()
	 * 		   |		.getIngredient().heat(randomRoundingErrorTemperature().getDifference(old.getIngredientContainer.getIngredient().getTemperature())
	 */
	@Override
	public void execute(){
		super.execute();
		Temperature newTemperature = randomRoundingErrorTemperature(); 
		setResult(getIngredientContainer());
		makeEmpty(); 
		if (getResult().getIngredient().isColderThan(newTemperature)) {
			long temperatureDifference = newTemperature.getDifference(getResult().getIngredient().getTemperature()) ; 
			getResult().getIngredient().heat(temperatureDifference);
		}
	}

	
	
	
	/**
	 * Method to calculate the new temperature of the Oven with a 5% error margin.
	 * @return	if the cold temperature equals zero then the result's cold temperature will be zero as well and the heat temperature will be calculated 
	 *          else the heat temperature will be zero and the cold temperature calculated 
	 *          |result  == new Temperature(0, randomRoundingErrorTemperature(Math.round(getTemperature().getTemperatureHeat()*0.95)
	 *                                                         , Math.round(getTemperature().getTemperatureHeat()*1.05)))
	 */
	@Model
	private Temperature randomRoundingErrorTemperature() {
              if ( getTemperature().getTemperatureCold() == 0) {
            	  long low = Math.round(getTemperature().getTemperatureHeat()*0.95) ; 
            	  long high = Math.round(getTemperature().getTemperatureHeat()*1.05) ; 
            	  return new Temperature(0, randomRoundingErrorTemperature(low, high)) ; 
              }
              else {
            	  long low = Math.round(getTemperature().getTemperatureCold()*0.95) ; 
            	  long high = Math.round(getTemperature().getTemperatureCold()*1.05) ; 
            	  return new Temperature( randomRoundingErrorTemperature(low, high), 0) ;
              }
            	  
		}
	
	/**
	 * A method to calculate a random number between the two numbers
	 * @param low
	 *        the lowest number
	 * @param high
	 *        the highest number 
	 * @pre the given low is a valid cold or heat temperature
	 *      | Temperature.isPossibleTemperatureCold(low) || Temperature.isPossibleTemperatureHeat(high) 
	 * @pre the given high is a valid cold or heat temperature 
	 *      | Temperature.isPossibleTemperatureCold(low) || Temperature.isPossibleTemperatureHeat(high) 
	 * @pre Low is smaller or equal to high
	 *      | low <= high
	 * @return the result is greater than or equal to low and smaller than or equal to high 
	 *         | result >= low && result <= high
	 */
	@Model
	private long randomRoundingErrorTemperature(long low, long high) {
		return (low + Math.round(Math.random()*(high - low)) ) ; 
	}

}
