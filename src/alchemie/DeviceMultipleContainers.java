/**
 * 
 */
package alchemie;

import java.util.LinkedList;

import be.kuleuven.cs.som.annotate.Model;

/**
 * Class representing a device that can contain multiple IngredientContainer's.
 * @invar	A DeviceMultipleContainers should contain proper IngredientContainer's.
 * 			| hasProperIngredientContainers()
 * @invar	A Device must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version	1.0
 */
public abstract class DeviceMultipleContainers extends Device {
	
	/**
	 * Method to check if a device has proper IngredientContainer's.
	 * @return	True if the device has proper IngredientContainer's; false otherwise.
	 * 			| if (for each IngredientContainer container in Device: 
	 * 			|		canHaveAsIngredientContainer(container) == true)
	 * 			| 	result == true
	 * 			| else if (getNbIngredientContainers() < 0)
	 * 			|	result == false
	 * 			| else if (getNbIngredientContainers() == 0)
	 * 			|	result == true
	 * 			| else result == false
	 */
	public boolean hasProperIngredientContainers() {
		boolean returnValue = true;
		if (getNbIngredientContainers() < 0) {
			returnValue = false;
		} else {
			for (int i = 1; i <= getNbIngredientContainers() && returnValue == true; i++) {
				returnValue = returnValue && canHaveAsIngredientContainer(getIngredientContainerAt(i));
			}
		}
		return returnValue;
	}
	
	
	/**
	 * Inspector to return the amount of IngredientContainer's in a device.
	 * @return	The amount of IngredientContainer's in the device.
	 * 			| result == getIngredientContainers.size()
	 */
	public int getNbIngredientContainers() {
		return getIngredientContainers().size();
	}
	
	
	/**
	 * Inspector to return an ArrayList with all the IngredientContainer's 
	 * of the device.
	 * @return	A copy of the the ingredientcontainers LinkedList of this device.
	 * 			| result == new LinkedList && 
	 * 			| for (int index = 1 to getNbIngredientsContainers()): 
	 * 			|	result.get(index - 1) == getIngredientContainerAt(index))
	 */
	@Model
	protected LinkedList<IngredientContainer> getIngredientContainers(){
		return new LinkedList<IngredientContainer>(this.ingredientContainers);
	}
	
	/**
	 * Inspector to return the IngredientContainer of the device corresponding
	 * with the given index.
	 * @param	index
	 * 			The index, starting from 1.
	 * @return	The IngredientContainer with the corresponding index
	 * 			 as ingredient of this device.
	 * 			| result == getIngredientContainers()
	 * @throws	IndexOutOfBoundsException if the given index
	 * 			is not a proper index for the IngredientContainers of the device.
	 * 			| index < 1 || index > getNbIngredientContainers()
	 */
	protected IngredientContainer getIngredientContainerAt(int index) throws 
	IndexOutOfBoundsException{
		return getIngredientContainers().get(index - 1);
	};
	
	/**
	 * @post	The given IngredientContainer has been added (as a copy) to the device.
	 * 			| getIngredientContainerAt(getNbIngredientContainers()) == new IngredientContainer(container.getCapacity(), container.getIngredient())
	 * @effect	If the device was already executed, the device will have been cleaned.
	 * 			| if (isExecuted()) then clean()
	 * @effect	The IngredientContainer is terminated.
	 * 			| container.terminate()
	 * @effect	The given container is added to the device as how a container would have been added in the class Device.
	 * 			| super.addIngredientContainer(container)
	 * 
	 */
	@Override
	public void 
	addIngredientContainer(IngredientContainer container){
		super.addIngredientContainer(container);
		if (isExecuted()) {
			clean();
		}
		this.ingredientContainers.add(new IngredientContainer(container.getCapacity(), container.getIngredient()));
		container.terminate();
	}
	
	
	/**
	 * A mutator to add multiple IngredientContainer's to a device.
	 * @param	containers
	 * 			The IngredientContainer's
	 * @effect	The IngredientContainer's are added to the device if the device can have the container as a container.
	 * 			| for each IngredientContainer container in containers:
	 *          |    if (canHaveAsIngredientContainer(container) ) 
	 * 			|	 then addIngredientContainer(IngredientContainer)
	 */
	public void addIngredientContainers(IngredientContainer...containers ) {
		for (IngredientContainer container: containers) {
			if (canHaveAsIngredientContainer(container))  addIngredientContainer(container);
		}
	}
	
	
	/**
	 * @return 	True if and only if the Device doesn't contain a result, doesn't contain IngredientContainer's
	 * 			 and is not executed; false otherwise. 
	 * 			| if (getResult() == null && isExecuted() == false && getNbIngredientsContainers() == 0) then 	
	 * 			| 	result == true 	
	 * 			| else result == false
	 */
	@Override
	public boolean isCleaned() {
		return super.isCleaned() && getNbIngredientContainers() == 0; 
	}
	
	/**
	 * Method to execute a device with it's own functionality.
	 * @throws 	IllegalStateException if the device is not a part of a Laboratory or doesn't
	 * 			contain proper IngredientContainer's.
	 * 			| getLaboratory() == null || hasProperIngredientContainers() == false || getNbIngredientContainers() == 0
	 */
	public void execute() throws IllegalStateException{
		if (getLaboratory() == null || !hasProperIngredientContainers() || getNbIngredientContainers() == 0) {
			throw new IllegalStateException("The Device can not be executed since the Device is not a "
					+ "part of a Laboratory or doesn't contain proper IngredientContainer's.");
		}
	}
	
	/**
	 * Method to remove all IngredientContainer's from a device.
	 * @post	The IngredientContainer's are removed from the device.
	 * 			| getNbIngredientContainers() == 0
	 * @effect	The DeviceMultipleContainers is cleaned as a Device.
	 * 			| super.clean()
	 */
	@Override
	public void clean() {
		super.clean();
		makeEmpty();
	}
	
	/**
	 * Method to clean all the IngredientContainer's from a device, except it's result.
	 * @post	The IngredientContainer's are removed from the device,
	 * 			except for the result of the device.
	 * 			| getNbIngredientContainers() == 0 && new.getResult() == old.getResult()
	 */
	public void makeEmpty() {
		this.ingredientContainers = new LinkedList<IngredientContainer>() ; 
	}
	
	/**
	 * Method to convert the IngredientContainer's of a device, except for it's result,
	 * to an LinkedList<AlchemicIngredient> containing all the ingredients of these containers.
	 * @return	An new LinkedList<AlchemicIngredient> containing the ingredients from the IngredientContainer's
	 * 			of the device, except for it's result.
	 * 			| for each IngredientContainer container in getIngredientsContainers():
	 * 			|	result.contains(container.getIngredient()) == true
	 */
	protected LinkedList<AlchemicIngredient> convertFromContainerToIngredient(){
		LinkedList<AlchemicIngredient> result = new LinkedList<AlchemicIngredient>() ; 
		for (IngredientContainer container: getIngredientContainers() ) {
			result.add(container.getIngredient()) ; 
		}
		return result ; 
	}
	
	/**
	 * IngredientContainer used as ingredient for the device.
	 * @invar 	The ingredientContainer should always contain proper IngredientContainers.
	 * 			| hasProperIngredientContainers()
	 * @invar	The ingredientContainers LinkedList is a valid LinkedList.
	 * 			| ingredientContainers != null
	 */
	private LinkedList<IngredientContainer> ingredientContainers = new LinkedList<IngredientContainer>();
	

}
