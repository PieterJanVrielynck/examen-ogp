package alchemie;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import be.kuleuven.cs.som.annotate.*;
import temperature.Temperature;


/**
 * A abstract class of general ingredients 
 * 
 * @invar Each general ingredient should have a proper state 
 *        | canHaveAsState( getState())  
 * @invar Each general ingredient should have a proper temperature
 *        |     canHaveAsTemperature(getTemperature() ) 
 * @invar The class GeneralIngredient should have a proper legalCharacterString
 *        | isValidLegalCharacterString(getLegalCharacterString() ) 
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 *
 */

public abstract class GeneralIngredient extends Object {
	/**********************************************************
	 * Constructors 
	 **********************************************************/

	/**
	 * A auxiliary method for initialization of objects of subclasses
	 * @param state
	 *        the state of the general ingredient
	 * @param heat
	 *        the desired heat for the general ingredient 
	 * @effect the state is set to the given state 
	 *         | setState(state) 
	 * @post the temperature is set to a temperature with given cold and heat
	 *       | new.getTemperature()[0] = new Temperature(cold, heat)getTemperatureCold() 
	 *       | new.getTemperature()[1] = new Temperature(cold, heat)getTemperaturHeat()
	 */
	protected void auxiliaryGeneralIngredient(State state, long heat)  {
		setState(state);  
	    this.temperature = new Temperature(0, heat) ; 
		
	}
	
	/**********************************************************
	 * Name
	 **********************************************************/
	
	
	  /**
     * An inspector to check whether a given name is a proper name 
     * @param possibleName
     *        the name to check
     * @return false if the possible name is not effective 
     */
	@Raw
    protected boolean canHaveAsName(String possibleName) { 
    	 if (  !(possibleName != null && possibleName.matches(getLegallegalCharacterString()))) return false;
    	return isNameBuildOutOfProperWords(possibleName) ; 
    }
    
    /**
     * An inspector to verify the possibleName does not contain any illegal words
     * @param possibleName
     *        The name to check 
     * @result false if the possibleName does not contain any words 
     *         if the possibleName contains one word
     *             true if the possibleName is not equal to an illegal word and has more than two letters
     *                     and the first letter is a capital letter
     *             otherwise false 
     *         if the possibleName contains more than one word: 
     *            false if the possibleName contains an illegal word, a word does not start with a capital letter
     *                     a word contains less than two letters or a word contains letter, other than the first is capital 
     *             true otherwise 
     *        | if possibleName.split(" ").length == 0 then result == false
     *        | else if possibleName.split(" ").length == 1 then 
     *        |            if possibleName.length() < 3 || for any word in getIllegalWordName(): word == possibleName 
     *        |            then result == false 
     *        |            else result == true 
     *        | else 
     *        |             if for any word in  possibleName.split(" ") :( word.length() < 2 || (for any part in getIllegalWordName():
     *        |  			                                                               word == part)
     *        |                                                            || Character.isLowerCase(word.charAt(0)) 
     *        |															   || (for any i in 1...word.length():
     *        |                                                                          !Character.isLowerCase(word.charAt(i)) ) )
     *        |             then result == false 
     *        |             else result == true 
     */
    @Model
    private static boolean isNameBuildOutOfProperWords(String possibleName) {
    	possibleName = possibleName.trim(); 
        if (!possibleName.contains(" ")) {
        	if ( possibleName.length() >= 3 ) return isValidWord(possibleName); 
        	else return false ; 
        }
        else {
       	 String[] list = possibleName.split(" ") ;
       	 if( list.length == 0 ) return false ; 
       	 for( String part: list) {
       		 if (part.length() < 2) return false ; 
       		 if (! isValidWord(part)) return false ; 
       			 
       	 }
       	 return true ; 
        }
    }
    
    /**
     * An inspector to check whether a given word can be part of a name 
     * @param word
     *        the word to check 
     * @return if first letter is a capital letter the other letters are not capital letters. the word is not an illegal word and contains more than one letter
     *         false otherwise 
     *         | if  word.length() > 2 && !getIllegalWordsInName().contains(word) && ! Character.isLowerCase(word.charAt(0)) &&
     *         |     for i = 1...word.length()-1:
     *         |              Character.isLowerCase(word.charAt(i)))
     *         | then result == true 
     *         | else result == false  
     */
    @Model
    private static boolean isValidWord(String word) { 
    	if (word.length() < 2) return false ;
    	if (getIllegalWordsInName().contains(word)) return false ;
    	if (Character.isLowerCase(word.charAt(0))) return false ; 
  		 for(int i = 1 ; i < word.length() ; i++) {
   			 if (!Character.isLowerCase(word.charAt(i))) return false ; 
   		 }
  		 return true ; 
    }
    
    /**
     * A basic inspector to return the illegal words for an  name of an alchemic ingredient 
     */
    @Model @Basic @Immutable 
    private static Set<String> getIllegalWordsInName() {
    	return illegalWordsInName ; 

    }
    
    /**
     * A variable referencing the illegal words. 
     * @invar IllegalWordsInName references an effective list 
     *        | IllegalWordsInName != null 
     * @invar Each item is effective
     *        | for each word in IllegalWordsInName
     *        |            word != null
     */
    private static final  Set<String> illegalWordsInName = new HashSet<String>(Arrays.asList("Cooled", "Heated", "With", "Mixed", "Inert", "Volatile", "Danger")) ;
    /**
     * A basic inspector to return the illegal string containing the pattern of the legal letters in a  name 
     */
    @Basic @Immutable 
    public  static String getLegallegalCharacterString(){
    	return legalCharacterString ; 
    }
    /**
     * An inspector to check whether a given string is a valid legalCharacterString
     * @param legalCharacterString
     *        the string to check 
     * @return  true if legalCharacterString starts with "[", ends with "]" and contains at least "a-z", "A-Z", "(", ")", "'" and " "
     *          false otherwise 
     *          | if legalCharacterString.charAt(0) == '[' and 
     *          |    legalCharacterString.charAt( legalCharacterString.length() -1 ) == '+'
     *          |    legalCharacterString.charAt( legalCharacterString.length() -2 ) == ']'
     *          |    legalCharacterString.contains("a-z")
     *          |    legalCharacterString.contains("A-Z")
     *          |    legalCharacterString.contains("(")
     *          |    legalCharacterString.contains(")")
     *          |    legalCharacterString.contains("'")
     *          |    legalCharacterString.contains(" ")
     *          | then result == true 
     *          | else result == false 
     */
    public static boolean isValidLegalCharacterString(String legalCharacterString) {
    	if ( ! (legalCharacterString.charAt(0) == '[')) return false ; 
    	if (! (legalCharacterString.charAt( legalCharacterString.length() -1 ) == '+')) return false ; 
    	if (! (legalCharacterString.charAt( legalCharacterString.length() -2 ) == ']')) return false ; 
    	if ( !(legalCharacterString.contains("a-z"))) return false ; 
    	if ( !(legalCharacterString.contains("A-Z"))) return false ; 
    	if ( !(legalCharacterString.contains("("))) return false ; 
    	if ( !(legalCharacterString.contains(")"))) return false ;
    	if ( !(legalCharacterString.contains("'"))) return false ; 
    	if ( !(legalCharacterString.contains(" "))) return false ; 
    	return true ; 
    }
    /**
     * A string referencing pattern of the legal letters in a name 
     */
    private static final String legalCharacterString = "[a-zA-Z()' ]+" ;
	
	
	/**********************************************************
	 * State
	 **********************************************************/
	
	/**
	 * Variable referencing the state of an general ingredient
	 */
	private State state ; 
	
	/**
	 * A basic inspector to return the state of an general ingredient. 
	 */
	@Basic 
	public State getState() {
		return this.state ; 
	}
	/**
	 * Inspector to check whether a given state is a valid state
	 * 
	 * @param state
	 *        the state to check 
	 * @return false if the state is not effective 
	 *         | result == false if state == null 
	 */
	@Raw
	public boolean canHaveAsState(State state) {
		return isPossibleState(state) ; 
	}
	
	/**
	 * A mutator to change the state of an general ingredient type 
	 * @param state
	 *        the new state
	 * @post the state of this general ingredient is set to the given state 
	 * 	   	 | new.getStat() == state 
	 * @throws IllegalArgumentException
	 *         if the given state is no possible state  
	 *         | ! isPossibleState(state) 
	 */
	@Raw
	void setState(State state) throws IllegalArgumentException{
		if( !isPossibleState(state) ) throw new IllegalArgumentException("no valid state is given") ; 
		this.state = state ; 
	}
	
	/**
	 * A inspector to verify if a given state is a possibly valid state. 
	 * 
	 * @param state
	 *        the state to check 
	 * @return false if the state is not effective ; true otherwise
	 *         | result ==  (state != null ) 
	 *         
	 */
	@Raw @Model
	protected static boolean isPossibleState(State state) {
		return (state != null ) ;
	}
	
	/**********************************************************
	 * Temperature 
	 **********************************************************/

    /**
     * An inspector to return the temperature of a general ingredient 
     */
	@Basic
    public Temperature getTemperature() {
    	return this.temperature ; 
    	}
    /**
     * An inspector to check whether a given general ingredient is hotter than the given temperature
     * @param temperature
     *        the given temperature 
     * @return true if temperature is effective and is hotter than the temperature of this general ingredient
     *         false otherwise
     *         | result ==  (other != null) && (getTemperature()..isHotterThan(other))
     */
    public boolean isHotterThan(Temperature other) {
    	if (other == null ) return false ; 
    	return this.temperature.isHotterThan(other) ; 

    }
    
    
    /**
     * An inspector to check whether a given general ingredient is colder than the given temperature
     * @param temperature
     *        the given temperature
     * @return true if the temperature is effective and the temperature of this general ingredeient is colder than the given temperature 
     *         false otherwise
     *         | result ==  (other != null) && (getTemperature().isColderThan(other)) 
     */
    public boolean isColderThan(Temperature other) {
    	if (other == null ) return false ; 
    	return this.temperature.isColderThan(other) ; 
    }




    /**
     * An inspector to check whether a given temperature is a valid temperature 
     * @param temperature
     *        the temperature to check
     * @return false if the given temperature is not effective 
     *         | result == false if temperature == null 
     */
    @Raw
    public  boolean canHaveAsTemperature(Temperature temperature) {
    	if (temperature == null) return false ; 
    	return true ; 
    }
    
    /**
     * A mutator to set the temperature
     * @param newTemperature 
     *        the new temperature 
     * @post if this ingredient can have the  given temperature as its temperature the temperature is set to the given temperature 
     *       | if (canHaveAsTemperature(newTemperature))
     *       | then new.getTemperature() == newTemperature
     */
    @Model
    protected void setTemperature( Temperature newTemperature) {
    	if (canHaveAsTemperature(newTemperature)) this.temperature = newTemperature ; 
    }
    
    /**
     * A variable referencing the temperature
     */
   private Temperature temperature ; 
        

}
