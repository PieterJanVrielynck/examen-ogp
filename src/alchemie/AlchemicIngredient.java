package alchemie;
import be.kuleuven.cs.som.annotate.*;
import temperature.Temperature;

import java.util.*; 
/**
 * A class of alchemic ingredients 
 *  
 * @invar Each alchemic ingredient should have a proper characteristical volatility. 
 *        | canHaveAsCharacteristicalVolatility(getCharacteristicalVolatility() ) 
 * @invar Each alchemic ingredient should have a proper unit
 *        | canHaveAsUnit(getUnit())  
 * @invar Each alchemic ingredient should have a valid amount as its amount
 *        | isValidAmount(getAmount() ) 
 * @invar Each alchemic ingredient should be able to have its type as its type
 *        | canHaveAsType( getType() ) 
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 *
 */
public  class AlchemicIngredient extends GeneralIngredient implements Comparable<AlchemicIngredient>{
	
	/**********************************************************
	 * Constructors
	 **********************************************************/
	
	/**
	 * A constructor to initialize a new Alchemic ingredient
	 * @param state
	 *        the new state
	 * @param type
	 *        the new type 
	 * @param amount
	 *        the amount 
	 * @param unit
	 *        the unit of the amount
	 * @pre This alchemic ingredient should be able to have the amount as its amount 
	 *      | isValidAmount(amount) 
	 * @post the characteristical volatility is set to new,calculated volatility 
	 *       | new.getCharacteristicalVolatility() == makeCharacteristicalVolatility()
	 * @post if this alchemic ingredient cannot have the given type as a possible type 
	 *       then the type is set to a default type
	 *       else the type is set to the given type 
	 *       | if (! canHaveAsPossibleType(type) then new.getType() == getDefaultType() 
	 *       | else new.getType() == type 
	 * @effect This alchemic ingredient its state and temperature is set with a auxiliary method
	 *         | auxiliaryGeneralIngredient(state, type.getTemperature().getTemperatureHeat())
	 * @effect The unit of this alchemic ingredient is set to the given unit
	 *         |setUnit(unit)
	 * @effect the amount is set 
	 *         | setAmount(amount)
	 * @throws IllegalArgumentException 
	 *         if the state and the unit do not match
	 *         | if ( ! matchesUnitState(unit, state))
	 */
	public  AlchemicIngredient(State state, IngredientType type, int amount, Quantity unit) throws IllegalArgumentException {
		if ( ! matchesUnitState(unit, state)) throw new IllegalArgumentException() ; 
		if (! canHaveAsPossibleType(type) ) {
			type = getDefaultType() ;
		}
		auxiliaryGeneralIngredient(state, type.getTemperature().getTemperatureHeat());
		this.type = type ; 
		setUnit(unit);  
		setAmount(amount);
		this.characteristicalVolatility = makeCharacteristicalVolatility() ; 
	}
	
	/**
	 * A constructor to initialize a new Alchemic ingredient
	 * @param state
	 *        the new state
	 * @param type
	 *        the new type 
	 * @param amount
	 *        the amount 
	 * @param unit
	 *        the unit of the amount
	 * @param charVolatility
	 *        the new characteristical volatility 
	 * @pre This alchemic ingredient should be able to have the amount as its amount 
	 *      | isValidAmount(amount) 
	 * @post if this alchemic ingredient cannot have the given type as a possible type 
	 *       then the type is set to a default type
	 *       else the type is set to the given type 
	 *       | if (! canHaveAsPossibleType(type) then new.getType() == getDefaultType() 
	 *       | else new.getType() == type 
	 * @effect This alchemic ingredient its state and temperature is set with a auxiliary method
	 *         | auxiliaryGeneralIngredient(state, type.getTemperature().getTemperatureHeat())
	 * @effect The unit of this alchemic ingredient is set to the given unit
	 *         |setUnit(unit)
	 * @effect the amount is set 
	 *         | setAmount(amount)
	 * @throws IllegalArgumentException
	 *         if the Characteristical volatility does not match the type 
	 *         | if (! matchesTypeCharacteristicalVolatility(type, charVolatility))
	 * @throws IllegalArgumentException 
	 *         if the state and the unit do not match
	 *         | if ( ! matchesUnitState(unit, state))
	 */
	public AlchemicIngredient(State state, IngredientType type, int amount, Quantity unit, double charVolatility ) throws IllegalArgumentException{
		if (! canHaveAsPossibleType(type) ) {
			type = getDefaultType() ;
		}
		if (! matchesTypeCharacteristicalVolatility(type, charVolatility))throw new IllegalArgumentException()  ; 
		if ( ! matchesUnitState(unit, state)) throw new IllegalArgumentException() ; 
		auxiliaryGeneralIngredient(state, type.getTemperature().getTemperatureHeat());
		this.type = type;
		setUnit(unit);  
		setAmount(amount);
		this.characteristicalVolatility = charVolatility ; 
	}
	
	/**
	 *  A constructor to initialize a new Alchemic ingredient
	 * @param amount
	 *        the new amount
	 * @param unit
	 *        the new unit 
	 * @effect This alchemic ingredient is constructed as a new alchemic ingredient with a liquid state, a Water-type, the given amount and unit
	 *         | this(State.LIQUID, new IngredientType(), amount, unit)
	 */
	public AlchemicIngredient(int amount, Quantity unit) {
		this(State.LIQUID, new IngredientType(), amount, unit) ; 
	}
	
	/**********************************************************
	 * Simple Name
	 **********************************************************/

    
    /**
     * An inspector to return the simple name of a  alchemic ingredient
     * @return result is the name of the type of this alchemic ingredient 
     *         | result == getType().getName()
     */ 
	public String getSimpleName() {
		return getType().getName() ; 
	} 
	
	/**
	 * An inspector to compare two alchemic ingredients
	 * @return compare the two trimmed simple names 
	 *         | result ==  getSimpleName().trim().compareTo(otherIngredient.getSimpleName().trim());
	 */
	@Override
	public int compareTo(AlchemicIngredient otherIngredient) {
		return getSimpleName().trim().compareTo(otherIngredient.getSimpleName().trim());
	}
    
	/**********************************************************
	 * Full Name
	 **********************************************************/
    
	/**
	 * An inspector to return the full name of an alchemic ingredient
	 * 
	 * @return the result will begin with the first part of a full name of a alchemic ingredient 
	 *         the result will end  the end part of a full name 
	 *         | result.indexOf(makeFirstPartFullName() ) == 0 
	 *         | && result.indexOf(makeEndPartFullName()) = (result.length() -1) - makeEndPartFullName().length() 
	 */
	public String getFullName() {
		String result = makeFirstPartFullName() ; 
		result += getSimpleName() ;
        result += makeEndPartFullName() ; 
		return result ; 
	}
	
	/**
	 * A inspector to return the first part of a full name 
	 * 
	 * @return the result is the temperature dependent part of a full name before the volatility dependent part of a full name 
	 *         | result == makeTemperaturePartName() + makeVolatilityPartName()
	 */
	@Model
	protected String makeFirstPartFullName() {
		return  makeTemperaturePartName() + makeVolatilityPartName(); 
	
	}
	/**
	 * An inspector to return the end part of a vo
	 * @return the result will be " Danger" if the volatility at the standard temperature is greater than 1000
	 *         the result will be "" otherwise
	 *         | calculateNormalVolatility()> 1000 then result == " Danger"
	 *         | else result == "" 
	 */
	@Model
	protected String makeEndPartFullName() {
		String result = ""; 
		double standaard = calculateNormalVolatility() ; 
		if (standaard > 1000) {
			result += " Danger" ; 
		}
		return result ; 
	}
	/**
	 * A inspector to return the temperature dependent part of a full name 
	 * 
	 * @return if the ingredient is currently colder than the standard temperature of its type the result will be "Cooled "
	 *         else if the ingredient is currently hotter than the standard temperature of its type the result will be "Heated "
	 *         else the result is an empty string  
	 *         | if isColderThan(getType().getTemperature()) then result == ("Cooled ") 
	 *         | else if isHotterThan(getType().getTemperature()) then result ==  ("Heated ") 
	 *         | else result == ""
	 */
	@Model
	private String makeTemperaturePartName() {
		String result = "" ; 
		if (isColderThan(getType().getTemperature()) )  result += "Cooled " ; 
		else if ( isHotterThan(getType().getTemperature())) result += "Heated " ; 
		return result ; 
	}
	/**
	 * A inspector to return the volatility dependent part of a first part of a full name 
	 * 
	 * @return if the the ingredient has volatility greater than 1000 the result contains "Volatile "
	 *         else if volatility is smaller than 100 and is smaller than hundredth of the normal volatility then the result contains "Inert "
	 *         if none of the above conditions are met the result will be an empty string
	 *         | if getVolatility() > 1000 then result == "Volatile " 
	 *         | else if (getVolatility() < calculateNormalVolatility()/100 && getVolatility() < 100) then result == "Inert " 
	 *         | else result == ""
	 */
	@Model
    private String makeVolatilityPartName() {
    	String result = "" ; 
		if (getVolatility() > 1000) {
			result += "Volatile " ; 
		}
		else if (getVolatility() < calculateNormalVolatility()/100 && getVolatility() < 100) {
			result += "Inert " ; 
		}
		return result ; 
    }

	/**********************************************************
	 * General Name
	 **********************************************************/
    
    /**
     * An inspector to check whether a simple name of this alchemic ingredient is ordered before another
     * alchemic ingredient's name
     *  
     * @param ingredient
     *        the other alchemic ingredient 
     * @return True if the ingredient and its simple name are effective and the simple name is ordered before the simple name of the other 
     *         ingredient
     *         false otherwise 
     *         | if  (ingredient != null && (getSimpleName().compareToIgnoreCase(ingredient.getSimpleName()) < 0) && (ingredient != null)
     *         | then result == true 
     *         | else result == false 
     */
    @Model
    boolean isOrderedBefore(@Raw AlchemicIngredient ingredient) {
    	return (ingredient != null) && (ingredient.getSimpleName() != null && 
    		   (getSimpleName().compareToIgnoreCase(ingredient.getSimpleName()) < 0) ); 
    }
    /**
     * An inspector to check whether a simple name of this alchemic ingredient is ordered after another
     * alchemic ingredient's name
     *  
     * @param ingredient
     *        the other alchemic ingredient 
     * @return True if the ingredient and its simple name are effective and the simple name is ordered after the simple name of the other 
     *         ingredient
     *         false otherwise 
     *         | if  (ingredient != null && (getSimpleName().compareToIgnoreCase(ingredient.getSimpleName()) > 0) && (ingredient != null)
     *         | then result == true 
     *         | else result == false 
     */
    @Model
    boolean isOrderedAfter(@Raw AlchemicIngredient ingredient) {
    	return  (ingredient != null) && (ingredient.getSimpleName() != null) &&
    			(getSimpleName().compareToIgnoreCase(ingredient.getSimpleName()) > 0);
    }
    
    

    
    
	/**********************************************************
	 * Temperature
	 **********************************************************/
    
    /**
     * @return true if the given temperature is effective 
     *         false otherwise 
     *         | result == (temperature != null ) 
     */
    @Override @Raw
     public boolean canHaveAsTemperature(Temperature temperature) {
    	 return super.canHaveAsTemperature(temperature) ; 
     }
    

    /**
     * A mutator to heat up a given alchemic ingredient 
     * 
     * @param temperatureDifference
     *        The total amount of temperature the alchemic ingredient has to be heated up
     * @effect the temperature is set to a heated temperature 
     *         | setTemperature( getTemperature().heat(temperatureDifference))
     */
    @Model
    void heat(long temperatureDifference) {
    	setTemperature( getTemperature().heat(temperatureDifference));
    }
    
    
    /**
     * A mutator to heat up a given alchemic ingredient 
     * 
     * @param temperatureDifference
     *        The total amount of temperature the alchemic ingredient has to be cooled down
     * @effect the temperature is set to a cooled temperature 
     *         | setTemperature( getTemperature().cool(temperatureDifference)) 
     */
    @Model
    void cool(long temperatureDifference) {
    	setTemperature( getTemperature().cool(temperatureDifference));
    }
    
    

    
	/**********************************************************
	 *Amount
	 **********************************************************/
	
	/**
	 * Variable referencing the amount of this alchemic ingredient
	 */
    private  int amount ; 
    
    /**
     * A basic inspector to return the amount of the alchemic ingredient
     */
    @Basic
    public int getAmount() {
    	return amount ; 
    }
    /**
     * An inspector to check whether a given amount can be used as an amount
     * @param amount 
     *        the amount to verify 
     * @return true if the amount of this alchemic ingredient is greater than or equal to zero 
     *         false otherwise 
     *         | result == amount >=  0
     */
    @Raw
    public static boolean isValidAmount(int amount) {
    	return (amount >= 0) ; 
    }
    
    /**
     * A mutator to set the amount of this alchemic ingredient
     * @param amount
     *        the new amount
     * @pre the given amount should be a proper amount 
     *      | isValidAmount(amount) 
     * @post the amount is set to the given amount
     *       | new.getAmount() == amount 
     */
    @Raw
     void setAmount(int amount) {
    	this.amount = amount ; 
    }
    
    
	/**********************************************************
	 *Unit
	 **********************************************************/
    /**
     * A variable referencing the unit of an alchemic ingredient
     */
    private  Quantity unit ; 
    
    /**
     * A basic inspector to return the unit of the quantity of this alchemic ingredient 
     */
    @Basic
    public Quantity getUnit() {
    	return this.unit ; 
    }
    /**
     * A inspector to verify if a given unit is a possible unit for an alchemic ingredient 
     * @param unit
     *        the unit to check 
     * @return true if the given unit is effective 
     *         false otherwise
     *         | result == (unit != null)
     */
    @Raw
    public static boolean isPossibleUnit(Quantity unit) {
    	return (unit != null) ; 
    }
    /**
     * An inspector to verify if a given unit and state matches
     * @param unit
     *        the unit 
     * @param state
     *        the state
     * @return true if the both the given state and unit are a possible state and unit respectively and the unit has the state as possible state
     *         false otherwise 
     *         | if (  unit.hasAsPossibleState(state) && isPossibleUnit(unit) && isPossibleState(state) ) then result == true  
     *         | else result == false 
     */
    @Raw
    public static boolean matchesUnitState(Quantity unit, State state) {
    	if (!isPossibleUnit(unit) || ! isPossibleState(state)) return  false ; 
    	return unit.hasAsPossibleState(state)  ; 
    }
    /**
     * An inspector to check whether a given unit is a possible unit for this alchemic ingredient
     * @param unit
     *        the unit 
     * @return true if the given unit matches with the current state of this alchemic ingredient
     *         false otherwise 
     *         | result == matchesUnitState(unit, getState()) 
     */
    @Raw
    public  boolean canHaveAsUnit(Quantity unit){ 
    	return ( matchesUnitState(unit, getState()) )  ; 
  
    }
    /**
     * A mutator to set the unit to the given unit
     * @param unit
     *        the unit
     * @pre this ingredient should be able to have the given unit as its unit 
     *      | canHaveAsUnit(unit) 
     */
    @Raw
    void setUnit(Quantity unit) {
    	this.unit = unit ; 
    }
	/**********************************************************
	 * States
	 **********************************************************/  
    
     /**
      * Inspector to check whether a given state is a valid state
      * 
	  * @param state
	  *        the state to check
	  * @return true if the current unit and the given state matches 
	  *         false otherwise
	  *         | result ==  matchesUnitState(getUnit(), state)
      */
     @Override @Raw
     public boolean canHaveAsState(State state) {
    	 return matchesUnitState(getUnit(), state) ; 
     }
    /**
     * A mutator to change the state of an alchemic ingredient
     * @param newState
     *        the new state
     * @param newAmount
     *        the new amount 
     * @param newUnit
     *        the new unit 
     * @pre the newAmount should be a valid amount 
     *      | isValidAmount(newAmount) 
     * @post The amount is set to the new amount
     *       | new.getAmount() == newAmount
     * @post The state is set to the given new state 
     *       | new.getState() == newState
     * @post The unit is set to the given new unit 
     *       | new.getUnit() == newUnit
     * @throws IllegalArgumentException
     *         The given newUnit does not match the given newState
     *         |!matchesUnitState(newUnit, newState)
     */
    void changesState(State newState, int newAmount, Quantity newUnit) throws IllegalArgumentException {
    	if (!matchesUnitState(newUnit, newState)) throw new IllegalArgumentException() ; 
    	setAmount(newAmount);
    	setState(newState);
    	setUnit(newUnit);
    }
    
	/**********************************************************
	 * IngredientType (total) 
	 **********************************************************/
	
	/**
	 * Variable referencing the type of an single alchemic ingredient. 
	 */
	private IngredientType type ; 
	
	/**
	 * A basic inspector to return the type of an single alchemic ingredient 
	 */
	@Basic
	public IngredientType getType() {
		return this.type ; 
	}

	/**
	 * An inspector to check whether a given type is a proper type
	 * @param type
	 *        the type to check
	 * @return false if the type is not effective 
	 *         | if (type == null) then result == false 
	 */
	@Raw
	public  boolean canHaveAsPossibleType(IngredientType type) {
		return (type != null) && ! (type instanceof MixtureIngredientType); 
	}
	
	/**
	 * An inspector to verify if a given type matches a given characteristical volatility 
	 * @param type
	 *        the given type 
	 * @param charVolatility
	 *        the given characteristical volatility 
	 * @return  if the type is effective and charVolatility is not more than 10 % higher or lower than the theoretical volatility of the type 
	 *          then result is true 
	 *          else result is false 
	 *          | if ( type != null &&   type.getTheoreticalVolatility()*0.9  <= charVolatilit && charVolatility <= type.getTheoreticalVolatility()*1.1)
	 *          | then result == true 
	 *          | else result == false 
	 */
	public static boolean matchesTypeCharacteristicalVolatility(IngredientType type, double charVolatility) {
		if (type ==null) return false ; 
		double difference = type.getTheoreticalVolatility()*0.1 ; 
		double low = type.getTheoreticalVolatility() - difference ;
		double high = type.getTheoreticalVolatility() + difference ;
		return (low <= charVolatility && charVolatility <= high ) ; 
		
	}
	/**
	 * An inspector to verify if an alchemic ingredient can have the given type as its type
	 * @param type
	 *         The type to verify 
	 * @return if the this alchemic ingredient can have the given type as a possible type and the current characteristical volatility matches the given type
	 *         then result equals true 
	 *         else result equals false 
	 *         | result == canHaveAsPossibleType(type) && matchesTypeCharacteristicalVolatility(type, getCharacteristicalVolatility()) 
	 */
	@Raw
	public boolean canHaveAsType(IngredientType type) {
		return canHaveAsPossibleType(type) && matchesTypeCharacteristicalVolatility(type, getCharacteristicalVolatility()) ; 
	}
	/**
	 * A inspector to query a default ingredient type for this alchemic ingredient 
	 * @return the result is a proper ingredient type for this alchemic ingredient
	 *         | canHaveAsType(result) 
	 */
	@Model @Immutable 
	protected IngredientType getDefaultType() {
		return new  IngredientType() ; 
	}
	
	/**
	 * A mutator to set the type of a alchemic ingredient
	 * 
	 * @param type
	 * 		  The type for the ingredient 
	 * @post if this alchemic ingredient can have the given type as its type the type will be set to the given type
	 *       else the type will be set to the default type 
	 *       | if canHaveAsType(type) then new.getType() == type
	 *       | else new.getType == getDefaultType() 
	 */
	@Model
	private void setType(IngredientType type ) {
		if (! canHaveAsType(type)) {
			type = getDefaultType() ; 
		}
		this.type = type  ; 
	}
	

	
	/**********************************************************
	 * characteristical volatility
	 **********************************************************/
	
	/**
	 * A variable referencing the  characteristical volatility of an ingredient type 
	 */
	private final double characteristicalVolatility ; 
	
	/**
	 * A basic inspector to return the characteristical volatility of the ingredient type
	 */
	@Basic @Immutable
	public double getCharacteristicalVolatility() {
		return this.characteristicalVolatility ; 
	}
    /**
     * An inspector to verify if the given characteristical volatility is a proper characteristical volatility 
     * 
     * @param charVolatility
     *        the given characteristical volatility 
     * @return true  if charVolatility matches the type of this alchemic ingredient and the charVolatility is a possible characteristical volatility 
     *         false otherwise
     *         | result == matchesTypeCharacteristicalVolatility(getType(), charVolatility) && isPossibleCharacteristicalVolatility(charVolatility)
     */
	@Raw
	public boolean canHaveAsCharacteristicalVolatility(double charVolatility) { 
		return matchesTypeCharacteristicalVolatility(getType(), charVolatility) && isPossibleCharacteristicalVolatility(charVolatility); 

	}
	
	/**
	 * An inspector to verify if the given characteristical volatility is a possible characteristical volatility
	 * @param charVolatility
	 *         the given characteristical volatility
	 * @return false if the charVolatility is smaller than zero or charVolatility is bigger than one
	 *         true otherwise 
	 *         | result == ! ( charVolatility < 0 || charVolatility > 1)
	 */
	public static boolean isPossibleCharacteristicalVolatility(double charVolatility) {
		return ! ( charVolatility < 0 || charVolatility > 1)  ;
	}
	
	
	/**
	 * This inspector creates a valid new characteristical volatility for this alchemic ingredient
	 * 
	 * @return the result is a valid characteristical volatility 
	 *         | canHaveAsCharacteristicalVolatilit(result) 
	 */
	@Model
	private double makeCharacteristicalVolatility() {
		double difference = this.getType().getTheoreticalVolatility()*0.1 ; 
		double low = this.getType().getTheoreticalVolatility() - difference ; 
		if (low < 0) low = 0 ; 
		double high = this.getType().getTheoreticalVolatility() + difference ;
		if (high > 1) high = 1 ;
		Random r = new Random() ; 
		double result = r.nextDouble()*(high-low) + low ; 
		return result ; 
	}
	
	
	/**********************************************************
	 * Volatility
	 **********************************************************/
    
    /**
     * A  inspector to return the volatility
     * @return result == calculateVolatility()
     */
    public double getVolatility() {
    	return calculateVolatility(); 
    }


    /**
     * An inspector to query the current Volatility
     * 
     * @result result is greater than or equal to zero
     *         if the temperature of this alchemic ingredient is hotter than the temperature of its type 
     *         then result is greater than or equal to the normal volatility 
     *         else if the temperature is colder than the temperature of its type
     *         then result is smaller than or equal to the normal volatility 
     *         else result is equal to the normal volatility 
     *         | result >= 0 
     *         | if if( isHotterThan(getType().getTemperature()))
     *         | then result >= calculateNormalVolatility()
     *         | else if  (isColderThan(getType().getTemperature())) 
     *         | then result <= calculateNormalVolatility()
     *         | else result == calculateNormalVolatility()
     */
    @Model 
	private double  calculateVolatility() {
		double result ; 
		result = calculateNormalVolatility() ; 
		if( isHotterThan(getType().getTemperature())) { 
			double difference = getTemperature().getDifference(getType().getTemperature()) / getType().getTemperature().getTemperatureHeat() ;
			int count = (int) Math.floor(difference); 
			result += count*getCharacteristicalVolatility()*300 ; 
		}
		else if (isColderThan(getType().getTemperature())) {
			double difference = ( getType().getTemperature().getDifference(getTemperature()) )/getType().getTemperature().getTemperatureHeat();
			int count = (int) Math.floor(difference); 
			result = result*Math.pow(getCharacteristicalVolatility(), count) ; 
		}
		return result ; 
	}
    /**
     * An inspector to return the volatility of an alchemic ingredient under normal conditions 
     * @return the characteristical volatility multiplied by 100 multiplied by the heat temperature of the type of this ingredient divided by 10
     *         | result == (getCharacteristicalVolatility())*100*getType().getTemperatureHeat().getTemperatureHeat()/10
     */
    @Model @Immutable
    private double calculateNormalVolatility() {
    	double result = (getCharacteristicalVolatility())*100*getType().getTemperature().getTemperatureHeat()/10 ; 
    	return result ; 
    }
    
    
    

    /**********************************************************
	 * Laboratory
	 **********************************************************/
    
    /**
     * Method to return the Laboratory of an ingredient.
     * @return 
     * @return	The Laboratory of the AlchemicIngredient.
     */
    @Basic
    Laboratory getLaboratory() {
    	return this.laboratory;
    }
    
	/**
	 * Method to set the Laboratory of an AlchemicIngredient; only used for the class Laboratory.
	 * @param	laboratory
	 * 			The Laboratory to link this AlchemicIngredient to.
	 * @post	The ingredient is now linked to the given Laboratory.
	 * 			| new.getLaboratory() == laboratory.
	 */
    void setLaboratory(Laboratory laboratory) {
		this.laboratory = laboratory;
	}
    
    
    /**
     * The Laboratory this AlchemicIngredient is linked to.
     */
    private Laboratory laboratory = null;

	
	
	
}
