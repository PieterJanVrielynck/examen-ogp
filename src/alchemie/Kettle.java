package alchemie;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TreeSet;


import temperature.Temperature;

/**
 * Class representing a kettle.
 * @invar	A DeviceMultipleContainers should contain proper IngredientContainer's.
 * 			| hasProperIngredientContainers()
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
public class Kettle extends DeviceMultipleContainers {
     
	/**
	 * Constructor for the Kettle class.
	 * @post	The device is marked as not executed.
	 * 			| new.isExecuted() == false
	 * @post	The result of the device has been set to null.
	 * 			| new.getResult() == null
	 * @post	The Laboratory of the device has been unlinked by initialization.
	 * 			| new.getLaboratory() == null
	 * @post	The Kettle doesn't contain any IngredientContainer's at the time
	 * 			of initialization.
	 * 			| getNbIngredientContainers() == 0
	 */
	public Kettle() {
		
	}
	
	/**
      * An inspector to query the state of the mixture
      * @return the state of the ingredient with a standard temperature closest to {0,20} 
      *         if there multiple options Liquid is returned 
      *         | if for two ingredients in getClosestToZeroTwinty()
      *         |            ingredient1.getState() != ingredient2.getState()
      *         | then result == State.LIQUID
      *         | else result == getClosestToZeroTwinty().get(0).getState()
      */
	 private State getNewState() {
		 State newState = getClosestToZeroTwinty().get(0).getState() ; 
		 for (AlchemicIngredient closest : getClosestToZeroTwinty()) {
			 if ( newState != closest.getState()) return State.LIQUID ; 
		 }
         return newState ; 		
	 }
	 /**
	  * An inspector to query the total amount of ingredients (in spoons) 
	  * @return the total som of the amounts of the stored ingredients using the unit spoon
	  */
	 private double getAmountInSpoons() {
		 double newAmount = 0 ;  
		 for (AlchemicIngredient ingredient : convertFromContainerToIngredient()) {
                 newAmount = ingredient.getAmount()*ingredient.getUnit().convertTo(Quantity.SPOON) ;  
		 }
		 return newAmount ; 
	 }
	 
	 /**
	  * An inspector to query the unit of the new alchemic mixture 
	  * @param state
	  *        the state of the alchemic mixture
	  * @param amount
	  *        the non-rounded amount using spoons  
	  * @return if state equals liquid return the unit calculated for a liquid ingredient
	  *         else the unit for a powder 
	  *         | if (state == State.LIQUID)  result ==  calculateQuantityLiquid(amount)
	  *         | else result == calculateQuantityPowder(amount)
	  */
	 private Quantity calculateQuantity(State state, double amount) {
           if (state == State.LIQUID)  return calculateQuantityLiquid(amount); 
           else return calculateQuantityPowder(amount); 
	 }
	 /**
	  * Inspector to query the unit of an mixture 
	  * @param amount
	  *        the amount of the mixture (non-rounded) 
	  * @return Drop if the amount is smaller than 1
	  *         else spoon 
	  *         | if (amount < 1)  result ==  Quantity.DROP
	  *         | else result == Quantity.SPOON
	  */
	 private Quantity calculateQuantityLiquid(double amount) {
		 if (amount < 1)  return Quantity.DROP ; 
		 else  return Quantity.SPOON ; 
 
	 }
	 /**
	  * Inspector to query the unit of an mixture 
	  * @param amount
	  *        the amount of the mixture (non-rounded) 
	  * @return Pinch if the amount is smaller than 1
	  *         else spoon 
	  *         | if (amount < 1)  result ==  Quantity.PINCH
	  *         | else result == Quantity.SPOON
	  */
	 private Quantity calculateQuantityPowder(double amount ) {
		 if (amount < 1)  return Quantity.PINCH ; 
		 else  return Quantity.SPOON ; 		 
	 }
	 
	 /**
	  * An inspector to query the capacity of the result container
	  * @param state
	  *        the state of the result mixture 
	  * @param amount
	  *        the amount of the mixture (non-rounded) 
	  * @return if the state is liquid then the capacity for a liquid mixture 
	  *         else the capacity for a powder
	  *         |  if (state == State.LIQUID)  result ==  getCapacityContainerLiquid(amount)
	  *         | else result == getCapacityContainerPowder(amount)
	  */
	 private Quantity getCapacityContainer(State state, double amount) {
		 if (state == State.LIQUID)  return getCapacityContainerLiquid(amount); 
		 return getCapacityContainerPowder(amount); 
	 }
	 /**
	  * An inspector to query the capacity of the result container
	  * @param amount
	  *        the amount of the mixture (non-rounded) 
	  * @return if the amout is smaller than 1 then spoon
	  *         else if the amount is smaller than 5 vial 
	  *         els if the amount is smaller than 15 bottle 
	  *         else if the amount is smaller than 105 jug 
	  *         else barrel
	  *         |if (amount <= 1) result ==  Quantity.SPOON
      *         |else if (amount <= 5 ) result ==  Quantity.VIAL
      *         |else if ( amount <= 15) result == Quantity.BOTTLE 
      *         | else if (amount <= 105) result == Quantity.JUG 
      *         | else result ==  Quantity.BARREL 
	  */
	 private Quantity getCapacityContainerLiquid(double amount) {
           if (amount <= 1) return Quantity.SPOON; 
           else if (amount <= 5 ) return Quantity.VIAL ; 
           else if ( amount <= 15) return Quantity.BOTTLE ; 
           else if (amount <= 105) return Quantity.JUG ; 
           else return Quantity.BARREL ; 
	 }
	 
	 /**
	  * An inspector to query the capacity of the result container
	  * @param amount
	  *        the amount of the mixture (non-rounded) 
	  * @return if the amout is smaller than 1 then spoon
	  *         else if the amount is smaller than 7 sachet
	  *         els if the amount is smaller than 42 box
	  *         else if the amount is smaller than 126 sack 
	  *         else chest
	  *         |if (amount <= 1) result ==  Quantity.SPOON 
      *         |else if (amount <= 7 ) result ==  Quantity.SACHETL 
      *         |else if ( amount <= 42) result == Quantity.BOX 
      *         | else if (amount <= 126) result ==  Quantity.SACK  
      *         | else result ==   Quantity.CHEST  
	  */
	 private Quantity getCapacityContainerPowder(double amount) {
		 if (amount <= 1) return Quantity.SPOON; 
		 else if ( amount <= 7) return  Quantity.SACHET ; 
		 else if (amount <= 42) return Quantity.BOX ; 
		 else if (amount <= 126) return Quantity.SACK ; 
		 else return  Quantity.CHEST ; 
	 }
	 
	 /**
	  * An inspector to query the new amount of the result
	  * @param doubleAmount
	  *        the non-rounded amount using spoons
	  * @param unit
	  *        the desired unit
	  * @return the greatest integer smaller than or equal to the given doubleAmount converted to the given unit 
	  *         if this number is not greater than 1260. In that case 1260 is returned 
	  *         | if ( (int) Math.floor(doubleAmount*Quantity.SPOON.convertTo(unit)) then result == (int) Math.floor(doubleAmount*Quantity.SPOON.convertTo(unit)
	  *         | else result == 1260
	  */
	 private int getAmount(double doubleAmount, Quantity unit) {
		    int amount ; 
			doubleAmount = doubleAmount*Quantity.SPOON.convertTo(unit) ;
			amount = (int) Math.floor(doubleAmount) ; 
			if (amount > 1260) amount = 1260 ;
			return amount ; 
	 }
	 
	 /**
	  * An inspector to query the standard temperature 
	  * @return the hottest temperature in the array containing the standard temperature closest to {0,20}
	  *         | for all ingredients in getClosestToZeroTwinty():
	  *         |         result.equals(ingredient.getTemperature()) || result.isHotterThan(ingredient.getTemperature()) 
	  */
	 private long getStandardTemperatureHeat() {
		 ArrayList<AlchemicIngredient> array = getClosestToZeroTwinty() ; 
		 Temperature newTemperature = array.get(0).getType().getTemperature() ; 
		 if ( !(new Temperature()).isColderThan(newTemperature) ){
			 for (AlchemicIngredient ingredient: array) {
				 IngredientType type = ingredient.getType() ;
				 if ( type.getTemperature().isHotterThan(newTemperature)) return type.getTemperature().getTemperatureHeat() ; 
			 }
		 }
		 return newTemperature.getTemperatureHeat() ; 
	 }
	 
	 /**
	  * An inspector to return the weighted average of the temperatures of the stored ingredients using the amounts in spoons  
	  */
	 private Temperature getNewTemperature() {
		 double TemperatureWeighted = 0 ; 
		 double totalWeigth = 0 ; 
		 for (AlchemicIngredient ingredient: convertFromContainerToIngredient()) {
			 TemperatureWeighted = Temperature.convertTemperatureToLong(ingredient.getTemperature()) ;
			 TemperatureWeighted = TemperatureWeighted*ingredient.getAmount()*ingredient.getUnit().convertTo(Quantity.SPOON) ; 
			 totalWeigth += ingredient.getUnit().convertTo(Quantity.SPOON)  ; 
		 }
		 long newTemperature = Math.round(TemperatureWeighted/totalWeigth); 
		 return Temperature.convertLongToTemperature(newTemperature); 
	 }
	 /**
	  * An inspector returning a array containing the average theoretical and characteristical volatility 
	  * @return An array with length two containing a possible characteristical and theoretical volatility
	  *         | ALchemicIngredient.isPossibleCharacteristicalVolatility(result[0]) 
	  *         | IngredientType.isValidTheoriticalVolatility(result[1]) 
	  */
	 private double[] getNewCHaracteristicalAndThereticalVolatility() {
		 double averageChar = 0 ; 
		 double averageTheo = 0 ; 
		 LinkedList<AlchemicIngredient> array = convertFromContainerToIngredient() ; 
		 for (AlchemicIngredient ingredient: array) {
			 IngredientType type = ingredient.getType() ;
			 averageChar += ingredient.getCharacteristicalVolatility() ; 
			 averageTheo = type.getTheoreticalVolatility() ; 
		 }
		 averageChar = averageChar/array.size() ; 
		 averageTheo = averageTheo/array.size() ; 
		 return new double[] {averageChar, averageTheo} ; 
	 }
	 
	 /**
	  * An inspector to return all the stored ingredients closest to {0,20}
	  * @return all ingredients have a temperature with the least differnce with a temperature with a heat temperature of 20
	  *         | for all ingredients in result:
	  *         |      for all elements in convertFromContainerToIngredient(): 
	  *         |         Math.abs(ingredient.getType().getTemperature().getDifference(new Temperature() ) ) <= 
	  *         |         Math.abs(element.getType().getTemperature().getDifference(new Temperature() ) )
	  */
	 private ArrayList<AlchemicIngredient> getClosestToZeroTwinty() {
		 ArrayList<AlchemicIngredient> closest = new ArrayList<AlchemicIngredient>() ; 
		 long distance, minDistance = Long.MAX_VALUE ;
		 Temperature search = new Temperature() ; 
		 for (AlchemicIngredient ingredient: convertFromContainerToIngredient()) {
			 IngredientType type = ingredient.getType() ; 
			 distance = Math.abs(search.getDifference(type.getTemperature())) ; 
			 if ( distance < minDistance) {
				 closest = new ArrayList<AlchemicIngredient>() ; 
				 closest.add(ingredient); 
			 }
			 else if ( distance == minDistance) {
				 closest.add(ingredient);
			 }
		 }
		 return closest ; 
	 }
	 
	 /**
	  * An inspector to return parts that wlll make the new name 
	  * 
	  * @return A tree set containing all the names of the non-mixtures and all the parts of the simple name of the mixtures 
	  *         | for all ingredient in convertFromContainerToIngredient(): 
	  *         |        if ! ingredient instance of AlchemicMixture 
	  *         |        then result.contains(ingredient.getSimpleName().trim()) 
	  *         |        else: for all parts in ingredient.getPartName()
	  *         |                  result.containts(part.trim() ) 
	  */
	 private  TreeSet<String> makeNameList() {
		 TreeSet<String> result = new TreeSet<>() ; 
		 for(AlchemicIngredient ingredient: convertFromContainerToIngredient()) {
			 if ( ! (ingredient instanceof AlchemicMixture)) {
				 result.add(ingredient.getSimpleName().trim()) ; 
			 }
			 else {
				 for(String name: ((AlchemicMixture)ingredient).getPartName()) {
					 result.add(name.trim()) ; 
				 }
			 }
		 }
		 return result ; 
	 }
	 
	 /**
	  * An inspector to return a proper name for the mixture 
	  * 
	  * @return The result contains all the names in the name list 
	  *         | for all names in  makeNameList()
	  *         |    result.contains(name) 
	  */
	private String getNameResult() {
		String returnName = "";
		int index = 0 ; 
		TreeSet<String> set = makeNameList() ; 
		for (String name: set) {
			if (index == 1) {
				returnName += " mixed with " ; 
			}
			else if (index == set.size() - 1 ) {
				returnName += " and " ; 
			}
			else if (index != 0) {
				returnName += ", " ; 
			}
			returnName += name.trim(); 
			index ++ ; 
		}
		return returnName;
	}
	
	/**
	 * An inspector to return a new result container
	 * @return A container with calculated capacity containing an alchemic mixture with calculated state, characteristical volatility, unit and amount, 
	 *            temperature and type with calculated temperature, state and theoretical volatility 
	 *        | result.getIngredient().getType() 
	 *        |    == new MixtureIngredientType(getNameResult(),  getNewState(), getStandardTemperatureHeat(), getNewCHaracteristicalAndThereticalVolatility()[1])
	 *        | result.getIngredient().getState() == getNewState()
	 *        | result.getIngredient().getAmount() == getAmount(getAmountInSpoons(),  calculateQuantity( getNewState(), getAmountInSpoons()))
	 *        | result.getIngredient().getUnit() == calculateQuantity( getNewState(), getAmountInSpoons())
	 *        | result.getIngredient().getTemperature() == getNewTemperature()
	 *        | result.getCaacity() == getCapacityContainer( getNewState(), getAmountInSpoons())
	 */
	private IngredientContainer getContainer() {
		double[] volatility = getNewCHaracteristicalAndThereticalVolatility() ; 
		int amount ; 
		State newState = getNewState() ; 
		double doubleAmount = getAmountInSpoons() ; 
		Quantity capacity = getCapacityContainer(newState, doubleAmount) ; 
		Quantity unit = calculateQuantity(newState, doubleAmount) ;
        amount = getAmount(doubleAmount, unit) ; 
		MixtureIngredientType type = new MixtureIngredientType(getNameResult(), newState, getStandardTemperatureHeat(), volatility[1]) ; 
		AlchemicMixture resultIngredient = new AlchemicMixture(getNewState(), type, amount, unit, volatility[0]) ;
		long difference = type.getTemperature().getDifference(getNewTemperature()); 
		if (difference >= 0) resultIngredient.heat(difference);
		else resultIngredient.cool(difference);
		IngredientContainer resultContainer = new IngredientContainer(capacity, resultIngredient); 
		return resultContainer ; 
	}

	
	/**
	 * A mutator to execute the functionality of this kettle
	 * 
	 * @post The result of this device will be set to the calculated ingredient container
	 *       	| new.getResult() = getContainer()
	 * @effect	The device is executed as in the class DeviceMultipleContainer.
	 * 			| super.execute()
	 * @effect The container is made empty first.
	 *         | makeEmpty()
	 * @throws IllegalStateException
	 *         The kettle does not contain two different ingredients with different simple names 
	 *         | (makeNameList().size() < 2 || getIngredientContainers().size() < 2 )
	 */
	@Override
	public void execute() throws IllegalStateException {
		if (makeNameList().size() < 2 || getIngredientContainers().size() < 2 ) throw new IllegalStateException() ; 
        IngredientContainer result = getContainer() ; 
		super.execute();
		makeEmpty(); 
		setResult(result);
	}
	
	
	
	
}
