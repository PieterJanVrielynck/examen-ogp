/**
 * 
 */
package alchemie;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Class representing a device.
 * @invar	A Device must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @author 	Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version	1.0
 */
abstract class Device {
	
	
	/**
	 * Inspector to check if a Device can contain a certain IngredientContainer.
	 * @param	container
	 * 			The IngredientContainer.
	 * @return	True if the container is effective, is not terminated, the container
	 * 			is retrieved from the same Laboratory as the Laboratory of this device, 
	 * 			has a proper ingredient and has a proper capacity.
	 * 			| if (container != null && (!container.isTerminated()) && !container.isEmpty() &&
	 * 			|		getLaboratory() == container.getIngredient().getLaboratory())
	 * 			| 	then result == true
	 * 			| else result == false
	 */
	@Raw
	public boolean canHaveAsIngredientContainer(IngredientContainer container) {
		return container != null && (!container.isTerminated()) && !container.isEmpty() && getLaboratory() == container.getIngredient().getLaboratory();
		
	}
	
	
	
	
	/**
	 * Method to add an IngredientContainer to a device.
	 * @param	container
	 * 			The IngredientContainer.
	 * @throws	IllegalArgumentException if the given IngredientContainer
	 * 			is not a proper IngredientContainer for this method.
	 * 			| !canHaveAsIngredientContainer(container)
	 * @throws	IllegalStateException if the device is not a part of Laboratory.
	 * 			| getLaboratory() == null
	 */
	@Raw
	public void addIngredientContainer(IngredientContainer container) throws 
	IllegalArgumentException, IllegalStateException{
		if (!canHaveAsIngredientContainer(container)) {
			throw new IllegalArgumentException("IngredientContainer can't be "
					+ "added to the Device.");
		} else if (getLaboratory() == null) {
			throw new IllegalStateException("A Device must be part of a Laboratory to be used.");
		}
	};
	
	
	/**
	 * Method to check if a Device is cleaned.
	 * @return	True if and only if the Device doesn't contain a result and is not
	 * 			executed; false otherwise.
	 * 			| if (getResult() == null && isExecuted() == false) then
	 * 			|	result == true
	 * 			| else result == false
	 */
	public boolean isCleaned() {
		return getResult() == null && isExecuted() == false;
	}
	
	/**
	 * Method to remove the IngredientContainer's from a device, including
	 * the result.
	 * @post	The result has been reset. If not added to the Laboratory first,
	 * 			the result will be gone.
	 * 			| getResult() == null
	 * @post	The device is marked as not executed.
	 * 			| isExecuted() == false
	 */
	public void clean() {
		this.result = null;
		setExecuted(false);
	};
	
	/**
	 * Method to check if a device has been executed.
	 * @return	True if the device has been executed; false otherwise.
	 */
	@Basic
	public boolean isExecuted() {
		return this.isExecuted;
	}
	
	
	
	/**
	 * Method to execute a device with it's own functionality.
	 */
	public abstract void execute() throws IllegalStateException;
	
	

	
	
	/**
	 * Mutator to mark the device as executed.
	 * @post	The device has been marked as executed.
	 * 			| isExecuted() == true
	 */
	protected void setExecuted(boolean executed) {
		this.isExecuted = executed;
	}
	

	
	/**
	 * Boolean to indicate if a device has been executed.
	 * @invar	A Device must have a proper result if it's executed.
	 *			| !isExecuted() || getResult() != null
	 */
	private boolean isExecuted = false;
	
	
	
	
	/**
	 * Inspector to return the result of the alchemical function of a device (if executed first).
	 * @return	An effective IngredientContainer containing the result of the
	 * 			execution of the device or 'null' if the device hasn't been executed yet.
	 */
	@Basic
	public IngredientContainer getResult() {
		return this.result;
	};
	
	
	/**
	 * Mutator to set the result of the execution of a device.
	 * @param	result
	 * 			The result of the execution as an IngredientContainer.
	 * @post	The result of the execution has been set to the given IngredientContainer.
	 * 			| new.getResult() == result
	 * @post	The device is now marked as executed.
	 * 			| isExecuted() == true
	 * @effect	The device is first cleaned; all the ingredients (IngredientContainer's) are removed.
	 * 			| old.clean()
	 * @throws	IllegalArgumentException if the given IngredientContainer is not a proper
	 * 			IngredientContainer as a result.
	 * 			| result == null
	 */
	@Model
	protected void setResult(IngredientContainer result) {
		if (result == null) {
			throw new IllegalArgumentException("The given IngredientContainer is not a "
					+ "proper result (IngredientContainer).");
		} else {
			clean();
			this.result = result;
			this.isExecuted = true;
			
		}
	}
	
	
	
	/**
	 * Private IngredientContainer containing the result of the
	 * alchemic function of the device after execution.
	 * @invar	A Device must have a proper result if it's executed.
	 * 			| !isExecuted() || getResult() != null
	 */
	private IngredientContainer result = null;


	/**
	 * Method to return the linked Laboratory of the Device, or null if the Device is not
	 * linked to any Laboratory.
	 * @return	The Laboratory this Device is linked to, or null if this Device is not
	 * 			linked to any Laboratory.
	 */
	@Basic
	public Laboratory getLaboratory() {
		return this.laboratory;
	}
	

	/**
	 * Method to remove the linked Laboratory from the device.
	 * @post	The device isn't linked to any Laboratory anymore.
	 * 			| new.getLaboratory() == null
	 * @effect	The device is removed from the Laboratory.
	 * 			| if (old.getLaboratory() != null && old.getLaboratory().getDevice(this.getClass()) == this)
	 * 			|	then old.getLaboratory().removeDevice(this)
	 */
	public void removeLaboratory() {
		if (getLaboratory() != null) {
			//This way, a Laboratory can be unlinked from a Device, but a Device
			//can also be removed from a Laboratory.
			//Reversing the order of the unlinking-process and the oldLaboratory.removeDevice(this)
			//would cause a loop.
			Laboratory oldLaboratory = getLaboratory();
			this.laboratory = null;
			if (oldLaboratory.getDevice(this.getClass()) == this) {
				oldLaboratory.removeDevice(this);
			}
			
		}
	}
	
	
	/**
	 * Method to link a Device to a Laboratory.
	 * @param 	laboratory
	 * 			The Laboratory to link the Device to.
	 * @post	The device is linked to the given Laboratory.
	 * 			| new.getLaboratory() == laboratory
	 * @throws 	IllegalStateException if the device is already linked to a Laboratory.
	 * 			| getLaboratory() != null
	 * @throws 	IllegalArgumentException if the given Laboratory is not effective.
	 * 			| laboratory == null
	 */
	@Model
	void setLaboratory(Laboratory laboratory) throws IllegalStateException, IllegalArgumentException{
		if (laboratory == null) {
			throw new IllegalArgumentException("The given laboratory is not effective.");
		} else if (getLaboratory() != null) {
			throw new IllegalStateException("The Device's already linked to a Laboratory; remove the original Laboratory first.");
		} else {
			this.laboratory = laboratory;
		}
	}



	

	
	/**
	 * The Laboratory the Device is linked to.
	 */
	private Laboratory laboratory = null;




	
}
