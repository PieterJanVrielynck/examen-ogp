package alchemie;

import be.kuleuven.cs.som.annotate.Raw;

/**
 * Class representing a cooling box.
 * @invar	A CoolingBox must have a proper IngredientContainer.
 * 			| hasProperIngredientContainer()
 * @invar	A Device must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @invar	The CoolingBox must always have a proper temperature.
 * 			| isValidTemperature(getTemperature().getTemperatureCold(), getTemperature().getTemperatureHeat())
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 */
public class CoolingBox extends DeviceTemperatureController {

	/**
	 * Constructor for the CoolingBox class with a given IngredientContainer and temperature.
	 * @param	cold
	 * 			The cold temperature.
	 * @param   heat 
	 *          The heat temperature.
	 * @post	The device is marked as not executed.
	 * 			| new.isExecuted() == false
	 * @post	The device is marked as cleaned; thus not only is the result
	 * 			set to null, but this device also doesn't contain an IngredientContainer to work with.
	 * 			| new.isCleaned() == true && new.getResult() == null
	 * @post	The Laboratory of the device has been unlinked by initialization.
	 * 			| new.getLaboratory() == null
	 * @effect 	The temperature is set with given cold and heat
	 *         	| setTemperature(cold, heat)
	 */
	@Raw
	public CoolingBox(long cold, long heat) {
			setTemperature(cold, heat);
	}
	

	
	
	/**
	 * Method to cool the content of an CoolingBox.
	 * 
	 * @post The result of this device will be set to the ingredient container store in this device
	 *       	| new.getResult() = old.getIngredientContainer()
	 * @effect The temperature of the ingredient is cooled to the temperature of this cooling box if the cooling box is colder than the ingredient itself
	 *         |  if ( old.getIngredientContainer().getIngredient().isHotterThan(getTemperature())
	 *         |  then ingredient.cool(old.getIngredientContainer().getIngredient().getTemperature().getDifference(getTemperature( )) 
	 * @effect The container is made empty first.
	 *         | makeEmpty()
	 * @effect	The device is executed as in the class DeviceSingleContainer.
	 * 			| super.execute()
	 */
	@Override
	public void execute(){
		super.execute();
		setResult(getIngredientContainer());
		makeEmpty(); 
		AlchemicIngredient ingredient = getResult().getIngredient() ; 
		if (ingredient.isHotterThan(getTemperature())) {
			long temperatureDifference = ingredient.getTemperature().getDifference(getTemperature()); 
			ingredient.cool(temperatureDifference);
			}
	}
}
	
	
