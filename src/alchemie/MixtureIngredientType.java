package alchemie;
import be.kuleuven.cs.som.annotate.*;
import java.util.*; 

@Value
public class MixtureIngredientType extends IngredientType {
	
	
	/**********************************************************
	 * Constructors 
	 **********************************************************/
	/**
	 * A constructor to initialize a new mixture type
	 * @param name
	 *        the name of the type
	 * @param state
	 *        the state of the type
	 * @param heat
	 *        the heat temperature of the temperature
	 * @param theoreticalVolatility
	 *        the theoretical volatility 
	 * @effect this mixture type is constructed as an ingredient type with given properties 
	 *         | super(name, state, heat, theoreticalVolatility)
	 */
	 public MixtureIngredientType(String name, State state, long heat, double theoreticalVolatility ) {
		super(name, state, heat, theoreticalVolatility); 
	}
	
	/**********************************************************
	 * Name
	 **********************************************************/
	
	/**
	 * An inspector to check whether a given name is a proper name
	 *
     * @param possibleName
     *        the name to check
	 * @result true if the name is effective, does not contain more than one time " and ", contains exactly one time "mixed with ". 
	 *                 you can cut the name into pieces and the pieces are alphabetically ordered and are valid names for ingredient types that are no 
	 *                 mixtures types 
	 *         false  otherwise 
	 *         | if possbleName != null && (possibleName.split(" and ").length < 2) && (possibleName.split(" mixed with ").length > 2)
	 *         |      && exists cutInPieces(possibleName) && super.canHaveAsName(cutInPieces(possibleName).get(0))) && cutInPieces(possibleName).size() > 2 && 
	 *         |          for index = 1...cutInPieces(possibleName).size():
	 *         |             super.canHaveAsName(cutInPieces(possibleName).get(index)): && 
	 *         |                  (cutInPieces(possibleName).get(index-1).compareTo(cutInPieces(possibleName.get(index)) > 0)
	 *         | then result == true 
	 *         | else result = false 
	 */
	@Override 
	public boolean canHaveAsName(String possibleName) {
		if ( possibleName == null) return false ; 
		possibleName= possibleName.trim() ; 
		if (possibleName.split(" and ").length > 2) return false ; 
		if (possibleName.split(" mixed with ").length > 2) return false ; 
		ArrayList<String> pieces ; 
		try {
			pieces = cutInPieces(possibleName) ; 
		}
	    catch (IllegalArgumentException exception) {
	    	return false ; 
	    }
		if (pieces.size() < 2) return false ;  
		if (! super.canHaveAsName(pieces.get(0))) return false ;
		String last = pieces.get(0);
		for (int index = 1; index < pieces.size() ; index ++ ) {
			if (! super.canHaveAsName(pieces.get(index)) ) return false ; 
			if (last.compareTo(pieces.get(index)) > 0) return false ; 
			last = pieces.get(index) ; 
		}
		return true ; 
	}
	
	/**
	 * An inspector to query the different pieces of a name
	 * @param name
	 *        the name to cut in pieces 
	 *        
	 * @return An arraylist containing strings that are part of the name 
	 *         | for all words in result: 
	 *         |      name.contains(result) 
	 * @throws IllegalArgumentException
	 *         the name contains " mixed with ", and ends on "mixed with " 
	 *         the name contains " mixed with ", "and " and ends on "end "
	 *         | contains(" mixed with ") && name.split(" mixed with ").length == 1
	 *         | contains(" mixed with ") && contains(" and " ) && name.split(" and ").length == 1
	 *         
	 */
	@Model 
    private  ArrayList<String> cutInPieces(String name) throws IllegalArgumentException { 
	   ArrayList<String> result = new ArrayList<String>( ) ; 
	   try {
		   if (!name.contains(" mixed with ")) {
			   result.add(name) ; 
			   return result ; 
		   }
		   String[] parts = name.split(" mixed with ") ;
		   result.add(parts[0].trim()); 
		   if (!name.contains(" and " )){
			   result.add(parts[1].trim()) ; 
		   }
		   String[] parts2 = parts[1].split(" and ") ;
		   if (!name.contains(", " ) ) {
			   for (String part : parts2) {
				   result.add(part.trim()) ; 
			   }
			   return result ; 
		   }
		   String[] parts3 = parts2[0].split(", "); 
		   result.add(parts[0].trim()); 
		   for (String part: parts3) {
			   result.add(part.trim());
		   }
	       result.add(parts2[1].trim() ) ;
		   return result ; 
	   }
	   catch (Exception exception) {
		   throw new IllegalArgumentException() ; 
	   }

   }

}
