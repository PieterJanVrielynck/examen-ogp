package alchemie;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;

/**
 * Enumerator for the quantities.
 * @invar	The value of a Quantity must be strictly positive.
 * 			| canHaveAsValue(getValue())
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0	
 */
@Value
public enum Quantity {

	DROP(0.125, State.LIQUID), SPOON(1.0, State.LIQUID, State.POWDER), VIAL(5.0, State.LIQUID), 
	BOTTLE(15.0, State.LIQUID), JUG(105.0, State.LIQUID), BARREL(1260.0, State.LIQUID), 
	STOREROOM(6300.0, State.LIQUID, State.POWDER),
	PINCH(1.0/6.0, State.POWDER), SACHET(7.0, State.POWDER), BOX (42.0, State.POWDER), 
	SACK(126.0, State.POWDER), CHEST (1260.0, State.POWDER);
	
	/**
	 * Constructor for the quantity enumerator.
	 * @param	value	
	 * 			The value of the quantity unit, relative
	 * 			to the value of the SPOON quantity unit.
	 * @pre		The value of the quantity unit has to be a strictly positive double.
	 * 			| value > 0
	 * @pre		The given states are effective State's.
	 * 			| for each State state in states: state != null
	 * @post	The value of the quantity unit has been set to the given value.
	 * 			| new.getValue() == value
	 * @post	The given states are added to the ArrayList that contains
	 * 			all the possible states linked to the quantity.
	 * 			| getPOSSIBLE_STATES() == states
	 */
	private Quantity(double value, State...states) {
		this.VALUE_UNIT = value;
		this.POSSIBLE_STATES = new ArrayList<State>();
		for (State state: states) {
			POSSIBLE_STATES.add(state);
		}
	}
	
	
	
	
	
	
	/**
	 * Method to return an ArrayList containing all the possible states
	 * for a quantity.
	 * @return	An ArrayList containing all the possible states.
	 */
	@Basic @Immutable
	public ArrayList<State> getAllPOSSIBLE_STATES(){
		return new ArrayList<State>(this.POSSIBLE_STATES);
	}
	
	
	/**
	 * Method to check if a quantity has a specific (possible) state.
	 * @param	state
	 * 			The state to check.
	 * @pre		The given state is effective.
	 * 			| state != null
	 * @return	True if and only if the given state is a possible state for the
	 * 			quantity; false otherwise.
	 * 			| result == getAlPOSSIBLE_STATES().contains(state)
	 */
	public boolean hasAsPossibleState(State state) {
		return getAllPOSSIBLE_STATES().contains(state);
	}
	
	
	/**
	 * ArrayList containing all the possible states for a quantity.
	 */
	private ArrayList<State> POSSIBLE_STATES;
	
	/**
	 * An inspector to return the conversion rate of the given
	 * quantity relative to the value of the 
	 * prime quantity.
	 * @param 	otherQuantity
	 * 			The new quantity.
	 * @pre		The given quantity is effective.
	 * 			| otherQuantity != null
	 * @return	The conversion rate of the values of the two
	 * 			quantity units.
	 * 			| result == this.getValue()/otherQuantity.getValue()
	 */
	public double convertTo(Quantity otherQuantity) {
		//'this' explicitly stated to emphasize the fact that we are
		//referring to the current QuantityUnit. Makes this method more readable.
		return this.getValue()/otherQuantity.getValue();
	}
	
	
	
	/**
	 * Method to check if a Quantity can have a certain value.
	 * @param	value
	 * 			The value to check.
	 * @return	True if and only if the given value is strictly positive;
	 * 			false otherwise.
	 * 			| if (value > 0)
	 * 			|	then result == true
	 * 			| else result == false
	 */
	boolean canHaveAsValue(double value)
	{
		return value > 0;
	}
	
	
	/**
	 * A basic inspector to return the value of a certain quantity, relative
	 * to the value of the SPOON quantity.
	 * @return	The value of the quantity.
	 */
	@Basic @Immutable
	public double getValue() {
		return this.VALUE_UNIT;
	}
	
	/**
	 * Value of a unit of a quantity, relative to the value of the SPOON unit.
	 * @invar	The value of a Quantity unit must always be strictly positive.
	 * 			| canHaveAsValue(VALUE_UNIT)
	 */
	private final double VALUE_UNIT;
}
