/**
 * 
 */
package alchemie;

import be.kuleuven.cs.som.annotate.Basic;

/**
 * Class for the Transmogrifier.
 * @invar	A Transmogrifier must have a proper IngredientContainer.
 * 			| hasProperIngredientContainer()
 * @invar	A Transmogrifier must have a proper result if it's executed.
 *			| !isExecuted() || getResult() != null
 * @invar	A Transmogrifier must always have a proper State.
 *			| canHaveAsState(getState)
 * @author Yoshi Vermeire, Pieter-Jan Vrielynck
 * @version 1.0
 *
 */
public class Transmogrifier extends DeviceSingleContainer {
	
	/**
	 * Constructor for the Transmogrifier class with a given State.
	 * @param	newState
	 * 			The given state of the Transmogrifier.
	 * @post	The device is marked as not executed.
	 * 			| new.isExecuted() == false
	 * @post	The device is marked as cleaned; thus not only is the result
	 * 			set to null, but this device also doesn't contain an IngredientContainer to work with.
	 * 			| new.isCleaned() == true && new.getResult() == null
	 * @post	The Laboratory this Transmogrifier is linked to is set to null.
	 * 			| new.getLaboratory() == null
	 * @effect	The state of the Transmogrifier has been set.
	 * 			| setState(newState)
	 */
	public Transmogrifier(State newState) {
		setState(newState);
	}
	
	
	/**
	 * Method to execute a Transmogrifier.
	 * @post	The result of the execution is an IngredientContainer, which is the
	 * 			original IngredientContainer with the only difference that the State
	 * 			of the ingredient has been changed.
	 * 			| new.getResult().getIngredient().getType().getName() == old.getIngredientContainer().getIngredient().getType().getName() &&
	 * 			| new.getResult().getIngredient().getType().getState() == getState() &&
	 * 			| new.getResult().getIngredient().getType().getTemperature() == old.getIngredientContainer().getIngredient().getType().getTemperature() &&
	 * 			| new.getResult().getIngredient().getType().getTheoreticalVolatility() == old.getIngredientContainer().getIngredient().getType().getTheoreticalVolatility() &&
	 * 			| new.getResult().getIngredient().getState() == old.getIngredientContainer().getIngredient().getState() &&
	 * 			| new.getResult().getIngredient().getAmount() <= 
	 * 			|	((int) old.getIngredientContainer().getIngredient().getAmount() * old.getIngredientContainer().getIngredient().getUnit().convertTo(
	 * 			|	 new.getResult().getIngredient().getUnit())) &&
	 * 			| new.getResult().getIngredient().getAmount() >= 
	 * 			|	((int) (old.getIngredientContainer().getIngredient().getAmount() * old.getIngredientContainer().getIngredient().getUnit().convertTo(
	 * 			| new.getResult().getIngredient().getUnit())) - 1) &&
	 * 			| (if (getState().equals(State.LIQUID)) then new.getResult().getIngredient().getUnit() == Quantity.DROP
	 * 			|	else new.getResult().getIngredient().getUnit() == Quantity.PINCH) &&
	 * 			| new.getResult().getIngredient().getUnit() == old.getIngredientContainer().getIngredient().getUnit() &&
	 * 			| new.getResult().getCapacity() == new.getResult().getIngredient().getState().getMaxQuantityToRetrieveFromLaboratory()
	 * @effect	The Transmogrifier has been executed as a DeviceSingleContainer.
	 * 			| super.execute()
	 * @effect	The Transmogrifier is first made empty.
	 * 			| makeEmpty()
	 */
	public void execute(){
		super.execute();
		setResult(getIngredientContainer());
		AlchemicIngredient oldIngredient = getResult().getIngredient();
		Quantity newUnit = (getState().equals(State.LIQUID))? Quantity.DROP:Quantity.PINCH;
		double newAmount = oldIngredient.getAmount() * oldIngredient.getUnit().convertTo(newUnit);
		int newAmountInt = (int) ((newAmount - Math.round(newAmount) < 0)? newAmount-1:newAmount); 
		oldIngredient.changesState(getState(), newAmountInt, newUnit);
		makeEmpty();
	}
	
	
	/**
	 * Method to check if a given State would be a proper State for a Transmogrifier.
	 * @param	state
	 * 			The given State.
	 * @return	True if and only if the given State is effective; false otherwise.
	 * 			| state != null
	 */
	public boolean canHaveAsState(State state) {
		return state != null;
	}
	
	
	/**
	 * Inspector to return the State of a Transmogrifier.
	 * @return	The State of the Transmogrifier.
	 */
	@Basic
	public State getState() {
		return this.state;
	}
	
	
	/**
	 * Method to set the State of a Transmogrifier.
	 * @param	state
	 * 			The new State of the Transmogrifier.
	 * @post	The State of the Transmogrifier has been set.
	 * 			| new.getState() == state
	 * @throws 	IllegalArgumentException
	 * 			if the given state is not a proper State for the Transmogrifier.
	 * 			| !canHaveAsState(state)
	 */
	public void setState(State state)  throws IllegalArgumentException{
		if (!canHaveAsState(state)) {
			throw new IllegalArgumentException("The given state is not effective.");
		} else {
			this.state = state;
		}
	}
	
	
	/**
	 * State of the Transmogrifier.
	 * @invar	An Transmogrifier must have a proper State.
	 * 			| canHaveAsState(getState())
	 */
	private State state;
	
	
}
